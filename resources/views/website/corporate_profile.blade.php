@extends('website.layout.default')
@section('title')
    JMI | Corporate Profile
@endsection
@section('content')

<section class="advanced-area">
    <div class="container">
        <h1>Corporate Profile</h1>
        <div class="col-md-offset-2 col-md-8 col-sm-12 col-xs-12">
            <div class="adv-detail cor">
                {!! $mission->description !!}
            </div>
            <a href="{{ url('business') }}" class="btn btn-default btn-more">View in PDF</a>
        </div>
    </div>
</section>



<section class="range-area">
    <div class="col-md-6 col-sm-6 col-xs-12 no-padding for-mobile"></div>
    <div class="col-md-6 col-sm-6 col-xs-12 no-padding">
       <img class="range-img" src="{{ asset('public/web') }}/img/banner/banner-2.png" alt="img" width="100%">
    </div>
    <div class="upper-range">
        <div class="container">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="skill-area">
                    <h1>We have 25 years of experience</h1>
                    <div class="skills" id="skills">
                        <div class="skillbar clearfix" data-percent="80%">
                            <h3>Medical Sectors Achieve</h3>
                            <div class="skills-main">
                                <div class="skillbar-bar"></div><span class="circle"></span>
                                <div class="skill-bar-percent">80%</div>
                            </div>
                        </div>
                        <div class="skillbar clearfix" data-percent="80%">
                            <h3>LPG Plantation Growth</h3>
                            <div class="skills-main">
                                <div class="skillbar-bar"></div><span class="circle"></span>
                                <div class="skill-bar-percent">80%</div>
                            </div>
                        </div>
                        <div class="skillbar clearfix" data-percent="80%">
                            <h3>Transport Services Achieve</h3>
                            <div class="skills-main">
                                <div class="skillbar-bar"></div><span class="circle"></span>
                                <div class="skill-bar-percent">80%</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <img class="rng" src="{{ asset('public/web') }}/img/interface/rng-1.png" alt="img">
    <a href="#" class="play" data-toggle="modal" data-target="#myModal"><i class="icofont-play-alt-1"></i></a>
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <iframe width="100%" height="550" src="https://www.youtube.com/embed/WRUgqnzBo4I" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="appoinment-area services-area" style="background:#f2f3f8">
    <div class="container no-padding">
        <h4>Success Insights of JMI Group</h4>
        <h1>Awards & Winning</h1>
        <div class="row">
            <div class="jcarousel-wrapper">
            <div class="jcarousel1" >
                <ul>
                    @foreach($awards as $value)
                        <li>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="awd-box">
                                    <img src="{{ asset($value->photo) }}" alt="#">
                                </div>
                            </div>
                        </li>
                    @endforeach
                </ul>
            </div>
            <a href="#" class="jcarousel-control-prev"><i class="icofont-rounded-left"></i></a>
            <a href="#" class="jcarousel-control-next"><i class="icofont-rounded-right"></i></a>
        </div>
    </div>
</section>

@endsection