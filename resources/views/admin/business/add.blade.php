@extends('admin.layout.default')
@section('title')
  Add GMI Business
@endsection
@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <!-- Main content -->
    <section class="content">
      @isset($add)
        <div class="box box-warning">
          <div class="box-header with-border">
            <h3 class="box-title">Create GMI Business</h3>
            <div class="box-tools pull-right">
            </div>
          </div>
          <div class="box-body">
          <form action="{{ route('business.store') }}" method="post" enctype="multipart/form-data" autocomplete="off">
          @csrf
            <div class="row">
              <div class="col-md-10">

                <div class="form-group">
                    <label for="regional_branch_name">Position <span class="text-red">*</span></label>
                    <input type="text" class="form-control" id="position" name="position" placeholder="Enter Position" value="" required>
                    <span class="text-danger">{{ $errors->has('position') ? $errors->first('position') : '' }}</span>
                </div>

                <div class="form-group">
                    <label for="regional_branch_name">Title <span class="text-red">*</span></label>
                    <input type="text" class="form-control" id="title" name="title" placeholder="Enter Title" value="" required>
                    <span class="text-danger">{{ $errors->has('title') ? $errors->first('title') : '' }}</span>
                </div>

                <div class="form-group">
                    <label for="regional_branch_name">Link <span class="text-red">*</span></label>
                    <input type="text" class="form-control" id="link" name="link" placeholder="Enter Link" value="" required>
                    <span class="text-danger">{{ $errors->has('link') ? $errors->first('link') : '' }}</span>
                </div>

                <div class="form-group">
                    <label for="address">Upload Photo <span class="text-red">*</span></label>
                    <input type="file" id="userfile" class="form-control" name="file"  value="" required onchange="getPreview('userfile','img_preview','none');">
                    <img src="<?= asset('public/backend/img/unknown.png'); ?>" style="width:100px; margin-top:5px" id="img_preview" class="img-responsive img-thumbnail"/><br>
                    <code>(Max photo size: 277x215, 512kb)</code>
                    <span class="text-danger">{{ $errors->has('file') ? $errors->first('file') : '' }}</span>
                </div>

                <div class="form-group">
                    <label for=""></label>
                    <a href="" class="btn btn-info btn-sm btn-warning">Cancel</a>
                    <input type="submit" name="submit" class="btn btn-primary btn-sm pull-right" value="Save">
                </div>
              
              </div>
            </div>
          </form>         
          </div><!-- /.box-body -->
        </div><!-- /.box -->
      @endisset
      @isset($edit)
        <div class="box box-warning">
          <div class="box-header with-border">
            <h3 class="box-title">Update Notice</h3>
            <div class="box-tools pull-right">
            </div>
          </div>
          <div class="box-body">
          <form action="{{ route('business.update',$single->id) }}" method="post" enctype="multipart/form-data" autocomplete="off">
          @csrf
            <div class="row">
              <div class="col-md-10">

                <div class="form-group">
                    <label for="regional_branch_name">Position <span class="text-red">*</span></label>
                    <input type="text" class="form-control" id="position" name="position" placeholder="Enter Position" value="{{ $single->position }}" required>
                    <span class="text-danger">{{ $errors->has('position') ? $errors->first('position') : '' }}</span>
                </div>

                <div class="form-group">
                    <label for="regional_branch_name">Title <span class="text-red">*</span></label>
                    <input type="text" class="form-control" id="title" name="title" placeholder="Enter Title" value="{{ $single->title }}" required>
                    <span class="text-danger">{{ $errors->has('title') ? $errors->first('title') : '' }}</span>
                </div>

                <div class="form-group">
                    <label for="regional_branch_name">Link <span class="text-red">*</span></label>
                    <input type="text" class="form-control" id="link" name="link" placeholder="Enter Link" value="{{ $single->link }}" required>
                    <span class="text-danger">{{ $errors->has('link') ? $errors->first('link') : '' }}</span>
                </div>

                <div class="form-group">
                    <label for="address">Update Photo</label>
                    <input type="file" id="userfile" class="form-control" name="file"  value="" onchange="getPreview('userfile','img_preview','none');">
                    <img src="<?= (empty($single->file))? asset('public/backend/img/unknown.png') : asset($single->file) ?>" style="width:100px; margin-top:5px" id="img_preview" class="img-responsive img-thumbnail"/><br>
                    <code>(Max photo size: 277x215, 512kb)</code>
                </div>

                <div class="form-group">
                    <label for=""></label>
                    <a href="" class="btn btn-info btn-sm btn-warning">Cancel</a>
                    <input type="submit" name="submit" class="btn btn-primary btn-sm pull-right" value="Update">
                </div>

              </div>
            </div>
          </form>         
          </div><!-- /.box-body -->
        </div><!-- /.box -->
        <script type="text/javascript">
            document.forms['notice'].elements['section_id'].value='<?php echo $single->section_id ?>';
        </script>
      @endisset
    </section><!-- /.content -->
  </div><!-- /.content-wrapper -->

  <script src="https://cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>
  <script>
      CKEDITOR.replace('description');
  </script>

@endsection