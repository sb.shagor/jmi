<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Auth;
use App\Business;
use Illuminate\Http\Request;

class BusinessController extends Controller
{
    public function index(){
        $data['get_all'] = Business::orderBy('position', 'ASC')->get();
        return view('admin.business.view', $data);
    }

    public function create(){
        $data['add'] = TRUE;
        return view('admin.business.add', $data);
    }

    public function store(Request $request){

        $this->validate($request, [          
            'position' => 'required',               
            'title' => 'required',               
            'link' => 'required',               
            'file' => 'required'                     
        ]);

        try {
            $business = new Business;
            $business->position = $request->position;
            $business->title = $request->title;
            $business->link = $request->link;
            $business->created_by = Auth::id();
            if(!empty($request->file('file'))){
                $File = $request->file('file');
                $name = $File->getClientOriginalName();
                $ext = explode('.',$name);
                $finalName = 'business-'.time().'.'.$ext[1];
                $uploadPath = 'public/uploads/';
                $File->move($uploadPath, $finalName);
                $business->file = $uploadPath.$finalName;
            }
            $business->save();
             
            setMessage("message","success",saved_success());
            return redirect(route('business.index'));

        }catch (\Exception $e) {
            $err_message = \Lang::get($e->getMessage());
            setMessage("message","danger",exception());
            return redirect(route('business.index'));
        }
    }

    public function edit($id){
        $data['edit'] = TRUE;
        $data['single'] = Business::findOrFail($id);
        return view('admin.business.add', $data);
    }

    public function update(Request $request, $id){

        $this->validate($request, [          
            'position' => 'required',                   
            'title' => 'required'                    
        ]);

        try {
            $business = Business::findOrFail($id);
            $business->position = $request->position;
            $business->title = $request->title;
            $business->link = $request->link;
            $business->updated_by = Auth::id();
            if ($request->file != null) {

                $path = $business->file;
                if (file_exists($path) and $business->file !=null) {
                    unlink($path);
                }
                $File = $request->file('file');
                $name = $File->getClientOriginalName();
                $ext = explode('.',$name);
                $finalName = 'business-'.time().'.'.$ext[1];
                $uploadPath = 'public/uploads/';
                $File->move($uploadPath, $finalName);
                $business->file = $uploadPath.$finalName;
            }
            $business->save();

            setMessage("message","success",updated_success());
            return redirect(route('business.index'));

        }catch (\Exception $e) {
            $err_message = \Lang::get($e->getMessage());
            setMessage("message","danger",exception());
            return back();
        }
    }

    public function destroy($id){

        try {
            $business = Business::findOrFail($id);
            $path = $business->file;
            if (file_exists($path) and $business->file != null) {
                unlink($path);
            }
            $business->delete();
            setMessage("message","success",deleted_success());
            return back();
        }catch (\Exception $e) {
            $err_message = \Lang::get($e->getMessage());
            setMessage("message","danger",exception());
            return back();
        }
    }

}//BusinessController