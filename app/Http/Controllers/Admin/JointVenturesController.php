<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Auth;
use App\JointVentures;
use Illuminate\Http\Request;

class JointVenturesController extends Controller
{
    public function index(){
        $data['get_all'] = JointVentures::get();
        return view('admin.joint_ventures.view', $data);
    }

    public function edit($id){
        $data['edit'] = TRUE;
        $data['single'] = JointVentures::findOrFail($id);
        return view('admin.joint_ventures.add', $data);
    }

    public function update(Request $request, $id){

        $this->validate($request, [                             
            'title' => 'required'                    
        ]);

        try {
            $joint = JointVentures::findOrFail($id);
            $joint->title = $request->title;
            $joint->link = $request->link;
            $joint->updated_by = Auth::id();
            $joint->save();

            setMessage("message","success",updated_success());
            return redirect(route('joint.index'));

        }catch (\Exception $e) {
            $err_message = \Lang::get($e->getMessage());
            setMessage("message","danger",exception());
            return back();
        }
    }

}//JointVenturesController
