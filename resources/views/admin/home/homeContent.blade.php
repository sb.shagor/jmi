@extends('admin.layout.default')
@section('title')
    Admin Dashboard
@endsection
@section('content')

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>it all starts here</small> 
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <div class="row">
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box">
                <div class="inner">
                </div>
                <div class="icon">
                  <i class="ion ion-earth"></i>
                </div>
              </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box">
                <div class="inner">
                </div>
                <div class="icon">
                  <i class="ion ion-ribbon-b"></i>
                </div>
              </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box">
                <div class="inner">
                </div>
                <div class="icon">
                  <i class="ion ion-ios-home-outline"></i>
                </div>
              </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box">
                <div class="inner">
                </div>
                <div class="icon">
                  <i class="ion ion-android-people"></i>
                </div>
              </div>
            </div><!-- ./col -->
          </div>
    </section><!-- /.content -->
  </div><!-- /.content-wrapper -->

@endsection