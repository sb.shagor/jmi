<section class="footer-area">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="footer-box">
                    <h1>About Us</h1>
                    <div class="logo">
                        <a href="index.php">
                            <img src="{{ asset('public/web') }}/img/interface/logo-1.png" alt="img"/>
                        </a>
                        <p>The JMI Group is one of the leading and most diversified global conglomerates in Bangladesh. The company was established in April 1999, having offices in all major cities, employing more than 7,000 employees and dedicated to bringing the highest quality products and services to our customers......<a href="#entry"data-toggle="modal">Read More</a>
                            <div id="entry" class="modal fade">
                              <div class="modal-dialog">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
                                    <h4 id="myModalLabel1" style="color: #000 !important;">About GMI</h4>
                                  </div>
                                  <div class="modal-body">
                                    The JMI Group is one of the leading and most diversified global conglomerates in Bangladesh. The company was established in April 1999, having offices in all major cities, employing more than 7,000 employees and dedicated to bringing the highest quality products and services to our customers
                                  </div>
                                </div>
                              </div>
                            </div>
                        </p>
                    </div>  
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="footer-box">
                    <h1>Contact Info</h1>
                    <ul class="text-foot add">
                        <li>
                            <i class="icofont-google-map"></i>
                            {!! $contact->office_address !!}
                        </li>
                        <li>
                            <i class="icofont-phone"></i>
                            <a href="tel:880-2-55138723" target="blank">{!! $contact->phone !!}</a>
                        </li>
                        <li>
                            <i class="icofont-envelope-open"></i>
                            <a href="mailto:{{ $contact->email }}" target="blank">{!! $contact->email !!}</a>
                        </li>
                    </ul>
                    <ul class="social">
                        <li><a href="{{ $website_info->facebook }}"><i class="icofont-facebook"></i></a></li>
                        <li><a href="{{ $website_info->twitter }}"><i class="icofont-twitter"></i></a></li>
                        <li><a href="{{ $website_info->linkedin }}"><i class="icofont-linkedin"></i></a></li>
                        <li><a href="{{ $website_info->instragam }}"><i class="icofont-instagram"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-2 col-sm-6 col-xs-12">
                <div class="footer-box">
                    <h1>Usefull Links</h1>
                    <ul class="text-foot">
                        <li><a href="{{ url('/') }}">Home</a></li>
                        <li><a href="{{ url('corporate_profile') }}">About Us</a></li>
                        <li><a href="{{ url('gmi_business') }}">JMI Business</a></li>
                        <li><a href="{{ url('video') }}">Video</a></li>
                        <li><a href="{{ url('news_event') }}">News & Events</a></li>
                        <li><a href="{{ url('circuler') }}">Circular</a></li>
                        <li><a href="{{ url('contact_us') }}">Contact Us</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="footer-box another">
                    <h1>Awards</h1>
                    <ul class="text-foot tags">
                        @foreach($awards as $value)
                            <a href="#"><img src="{{ asset($value->photo) }}" alt="#"></a>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="footer-bottom">
    <div class="container">
    <p>2021 © All rights reserved by <a href="#">JMI Group.</a></p>
        <ul>
            <li><a href="https://www.hitwebcounter.com" target="_blank">
                <img src="https://hitwebcounter.com/counter/counter.php?page=7877576&style=0030&nbdigits=6&type=page&initCount=73628" title="Free Counter" Alt="web counter"   border="0" /></a>  
            </li>
        </ul>
    </div>
</div>

<div class="scroll-top-wrapper ">
    <span class="scroll-top-inner">
        <i class="icofont-arrow-up"></i>
    </span>
</div>

<script src="{{ url('public/web') }}/js/range.js"></script>
<script src="{{ url('public/web') }}/js/jquery-2.1.1.js"></script>
<script src="{{ url('public/web') }}/js/slider2.js"></script>
<script src="{{ url('public/web') }}/js/theme.js"></script>
<script src="{{ url('public/web') }}/js/bootstrap.min.js"></script>