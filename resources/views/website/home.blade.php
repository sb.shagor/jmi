@extends('website.layout.default')
@section('title')
    JMI Group
@endsection
@section('content')

<!----------Start Slider---------->
<section class="slider-area">
    <div id="myCarousel" class="carousel slide carousel-fade" data-ride="carousel">
        <div class="carousel-inner">
            <?php
                foreach($sliders as $key => $value){
                ++$key;
                if($key==1){
                    $class = 'item active';
                }else{
                    $class = 'item';
                }
            ?>
                <div class="{{ $class }}">
                    <img src="{{ asset($value->image) }}" alt="{{ $value->title }}">
                    <div class="carousel-caption caption-3">
                        <h2>{{ $value->title }}</h2>
                        <h5>{{ $value->description }}</h5>
                    </div>
                </div>
            <?php } ?>
        </div>
        <!-- Left and right controls -->
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
            <i class="icofont-swoosh-left"></i>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
            <i class="icofont-swoosh-right"></i>
            <span class="sr-only">Next</span>
        </a>
    </div>
</section>

<section class="intro-area">
    <div class="col-md-3 col-sm-6 col-xs-12 no-padding">
        <div class="intro-box first">
            <!-- <i class="icofont-wall-clock"></i> -->
            <div class="inr no-bd">
                <h2>JMI Group<br>Joint Venture</h2>
            </div>
        </div>
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12 no-padding">
        <a class="intro-box" href="https://www.nipro.co.jp/en/" target="blank">
            <div class="inr">
                <img class="main" src="{{ asset('public/web') }}/img/interface/c-1.png" alt="1">
                <h4>Nipro Corporation, Japan</h4>
            </div>
        </a>
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12 no-padding">
        <a class="intro-box" href="https://www.starsyringe.com/" target="blank">
            <div class="inr">
                <img class="main" src="{{ asset('public/web') }}/img/interface/c-2.png" alt="1">
                <h4>Star Syringe Ltd UK</h4>
            </div>
        </a>
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12 no-padding">
        <a class="intro-box" href="http://en.sankur.com.tr/" style="border-right:0" target="blank">
            <div class="inr no-bd">
                <img class="main" src="{{ asset('public/web') }}/img/interface/c-3.png" alt="1">
                <h4>SUNKUR-Turkey</h4>
            </div>
        </a>
    </div>
</section>

<section class="advanced-area">
    <div class="container">
        <h4>Welcome to JMI Group</h4>
        <h1>Founder Managing Director</h1>
        <div class="col-md-5 col-sm-4 col-xs-12 no-padding">
            <div class="adv-img">
                <img class="main" src="{{ asset('public/web') }}/img/interface/adv.png" alt="1">
                <img class="first" src="{{ asset('public/web') }}/img/interface/adv-1.png" alt="2">
                <img class="second" src="{{ asset('public/web') }}/img/interface/adv-2.png" alt="3">
            </div>
        </div>
        <div class="col-md-7 col-sm-8 col-xs-12">
            <div class="adv-detail">
                <img class="ch" src="{{ asset($md_message->file) }}" alt="1">
                <p>
                    {!! $md_message->description !!}
                </p>
            </div>
        </div>
    </div>
</section>

<section class="services-area">
    <div class="container">
        <h4>Explore Our Caring & Premium</h4>
        <h1>JMI Business</h1>
        <div class="services-box">
            @foreach($businesses as $value)
                <div class="per-box">
                    <img src="{{ asset($value->file) }}" alt="img">
                    <p class="text">{{ $value->title }}</p>
                    <div class="overlay">
                        <div class="text">{{ $value->title }}</div>
                        <a href="{{ $value->link }}" class="btn btn-detail">Details</a>
                    </div>
                </div>
            @endforeach
            <a href="{{ url('gmi_business') }}" class="btn btn-default btn-more">View More</a>
        </div>
    </div>
</section>

<section class="range-area">
    <div class="col-md-6 col-sm-6 col-xs-12 no-padding for-mobile"></div>
    <div class="col-md-6 col-sm-6 col-xs-12 no-padding">
       <img class="range-img" src="{{ asset('public/web') }}/img/banner/banner-2.png" alt="img" width="100%">
    </div>
    <div class="upper-range">
        <div class="container">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="skill-area">
                    <h1>We have 25 years of experience</h1>
                    <div class="skills" id="skills">
                        <div class="skillbar clearfix" data-percent="80%">
                            <h3>Medical Sectors Achieve</h3>
                            <div class="skills-main">
                                <div class="skillbar-bar"></div><span class="circle"></span>
                                <div class="skill-bar-percent">80%</div>
                            </div>
                        </div>
                        <div class="skillbar clearfix" data-percent="80%">
                            <h3>LPG Plantation Growth</h3>
                            <div class="skills-main">
                                <div class="skillbar-bar"></div><span class="circle"></span>
                                <div class="skill-bar-percent">80%</div>
                            </div>
                        </div>
                        <div class="skillbar clearfix" data-percent="80%">
                            <h3>Transport Services Achieve</h3>
                            <div class="skills-main">
                                <div class="skillbar-bar"></div><span class="circle"></span>
                                <div class="skill-bar-percent">80%</div>
                            </div>
                        </div>
                        
                        <div class="skillbar clearfix" data-percent="80%">
                            <h3>Pharmaceuticals</h3>
                            <div class="skills-main">
                                <div class="skillbar-bar"></div><span class="circle"></span>
                                <div class="skill-bar-percent">80%</div>
                            </div>
                        </div>
                        
                        <div class="skillbar clearfix" data-percent="80%">
                            <h3> Hospital Requisite Product</h3>
                            <div class="skills-main">
                                <div class="skillbar-bar"></div><span class="circle"></span>
                                <div class="skill-bar-percent">80%</div>
                            </div>
                        </div>
                        
                        <div class="skillbar clearfix" data-percent="80%">
                            <h3> Energy (LPG Conversion, LPG Reticulation, LPG Gas)</h3>
                            <div class="skills-main">
                                <div class="skillbar-bar"></div><span class="circle"></span>
                                <div class="skill-bar-percent">80%</div>
                            </div>
                        </div>
                        
                        <div class="skillbar clearfix" data-percent="80%">
                            <h3> Constructions</h3>
                            <div class="skills-main">
                                <div class="skillbar-bar"></div><span class="circle"></span>
                                <div class="skill-bar-percent">80%</div>
                            </div>
                        </div>
                        
                        <div class="skillbar clearfix" data-percent="80%">
                            <h3>  Education & Transportation.</h3>
                            <div class="skills-main">
                                <div class="skillbar-bar"></div><span class="circle"></span>
                                <div class="skill-bar-percent">80%</div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <img class="rng" src="{{ asset('public/web') }}/img/interface/rng-1.png" alt="img">
    <a href="#" class="play" data-toggle="modal" data-target="#myModal"><i class="icofont-play-alt-1"></i></a>
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <iframe width="100%" height="550" src="https://www.youtube.com/embed/WRUgqnzBo4I" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="appoinment-area services-area" style="background-image: url(img/banner/banner-1.png)">
    <div class="container no-padding">
        <h4>See whats going inside</h4>
        <h1>News and Events</h1>
        <div class="row">
            <div class="jcarousel-wrapper">
            <div class="jcarousel2" >
                <ul>
                    @foreach($notices as $value)
                        <li>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="awd-box">
                                    <div class="txt-bx">
                                        <h4><i class="icofont-flag"></i> {{ $value->title }}</h4>
                                        <p>{{ limit_words(strip_tags($value->description),10,"UTF-8") }}</p>
                                        <div class="row">
                                            <div class="col-md-6 dt"><i class="icofont-calendar"></i> Date : {{ $value->created_at }}</div>
                                            <div class="col-md-6 lnk">
                                                <a href="#entry{{$value->id}}"data-toggle="modal">Read More</a>
                                                <div id="entry{{$value->id}}" class="modal fade">
                                                  <div class="modal-dialog">
                                                    <div class="modal-content">
                                                      <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
                                                        <h4 id="myModalLabel1" style="color: #000 !important;">{{ $value->title }}</h4>
                                                      </div>
                                                      <div class="modal-body">
                                                        {!! $value->description !!}
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="img-box">
                                        <img src="{{ asset($value->file) }}" alt="#">
                                    </div>
                                </div>
                            </div>
                        </li>
                    @endforeach
                </ul>
            </div>
            <a href="#" class="jcarousel-control-prev"><i class="icofont-rounded-left"></i></a>
            <a href="#" class="jcarousel-control-next"><i class="icofont-rounded-right"></i></a>
        </div>
    </div>
</section>

<section class="appoinment-area services-area" style="background:#f2f3f8">
    <div class="container no-padding">
        <h4>Success Insights of JMI Group</h4>
        <h1>Awards & Winning</h1>
        <div class="row">
            <div class="jcarousel-wrapper">
            <div class="jcarousel1" >
                <ul>
                    @foreach($awards as $value)
                        <li>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="awd-box">
                                    <img src="{{ asset($value->photo) }}" alt="#">
                                </div>
                            </div>
                        </li>
                    @endforeach
                </ul>
            </div>
            <a href="#" class="jcarousel-control-prev"><i class="icofont-rounded-left"></i></a>
            <a href="#" class="jcarousel-control-next"><i class="icofont-rounded-right"></i></a>
        </div>
    </div>
</section>

@endsection