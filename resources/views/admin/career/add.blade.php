@extends('admin.layout.default')
@section('title')
  Add Circuler
@endsection
@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <!-- Main content -->
    <section class="content">
      @isset($add)
        <div class="box box-warning">
          <div class="box-header with-border">
            <h3 class="box-title">Create Circuler</h3>
            <div class="box-tools pull-right">
            </div>
          </div>
          <div class="box-body">
          <form action="{{ route('careers.store') }}" method="post" enctype="multipart/form-data" autocomplete="off">
          @csrf
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                    <label for="regional_branch_name">Title <span class="text-red">*</span></label>
                    <input type="text" class="form-control" id="title" name="title" placeholder="Enter Title" value="" required>
                    <span class="text-danger">{{ $errors->has('title') ? $errors->first('title') : '' }}</span>
                </div>

                <div class="form-group">
                    <label for="regional_branch_name">Job Nature <span class="text-red">*</span></label>
                    <input type="text" class="form-control" id="job_nature" name="job_nature" placeholder="Enter Job Nature" value="" required>
                    <span class="text-danger">{{ $errors->has('job_nature') ? $errors->first('job_nature') : '' }}</span>
                </div>

                <div class="form-group">
                    <label for="regional_branch_name">Job Location <span class="text-red">*</span></label>
                    <input type="text" class="form-control" id="job_location" name="job_location" placeholder="Enter Job Location" value="" required>
                    <span class="text-danger">{{ $errors->has('job_location') ? $errors->first('job_location') : '' }}</span>
                </div>

                <div class="form-group">
                    <label for="regional_branch_name">Salary <span class="text-red">*</span></label>
                    <input type="text" class="form-control" id="salary" name="salary" placeholder="Enter Salary" value="" required>
                    <span class="text-danger">{{ $errors->has('salary') ? $errors->first('salary') : '' }}</span>
                </div>

                <div class="form-group">
                    <label for="regional_branch_name">Vacancies <span class="text-red">*</span></label>
                    <input type="text" class="form-control" id="vacancies" name="vacancies" placeholder="Enter Vacancies" value="" required>
                    <span class="text-danger">{{ $errors->has('vacancies') ? $errors->first('vacancies') : '' }}</span>
                </div>

                <div class="form-group">
                    <label for="regional_branch_name">Deadline <span class="text-red">*</span></label>
                    <input type="text" class="form-control datepicker"  data-date-format="yyyy-mm-dd" id="deadline" name="deadline" placeholder="Enter Deadline" value="<?= date('Y-m-d'); ?>" required>
                    <span class="text-danger">{{ $errors->has('deadline') ? $errors->first('deadline') : '' }}</span>
                </div>

              </div>
              <div class="col-md-6">

                <div class="form-group">
                  <label for="regional_branch_name">Job Description <span class="text-red">*</span></label>
                  <textarea name="description" id="description" class="col-xs-12 col-sm-4" ></textarea>
                </div>
                
                <div class="form-group">
                    <label for="address">Upload File <span class="text-red">*</span></label>
                    <input type="file" class="form-control" name="file"  value="" required>
                    <code>Max File Size less than 1MB</code>
                    <span class="text-danger">{{ $errors->has('file') ? $errors->first('file') : '' }}</span>
                </div>

                <div class="form-group">
                    <label for=""></label>
                    <a href="" class="btn btn-info btn-sm btn-warning">Cancel</a>
                    <input type="submit" name="submit" class="btn btn-primary btn-sm pull-right" value="Save">
                </div>
                
              </div>
            </div>
          </form>         
          </div><!-- /.box-body -->
        </div><!-- /.box -->
      @endisset
      @isset($edit)
        <div class="box box-warning">
          <div class="box-header with-border">
            <h3 class="box-title">Update Circuler</h3>
            <div class="box-tools pull-right">
            </div>
          </div>
          <div class="box-body">
          <form action="{{ route('careers.update',$single->id) }}" method="post" enctype="multipart/form-data" autocomplete="off">
          @csrf
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                    <label for="regional_branch_name">Title <span class="text-red">*</span></label>
                    <input type="text" class="form-control" id="title" name="title" placeholder="Enter Title" value="{{ $single->title }}" required>
                    <span class="text-danger">{{ $errors->has('title') ? $errors->first('title') : '' }}</span>
                </div>

                <div class="form-group">
                    <label for="regional_branch_name">Job Nature <span class="text-red">*</span></label>
                    <input type="text" class="form-control" id="job_nature" name="job_nature" placeholder="Enter Job Nature" value="{{ $single->job_nature }}" required>
                    <span class="text-danger">{{ $errors->has('job_nature') ? $errors->first('job_nature') : '' }}</span>
                </div>

                <div class="form-group">
                    <label for="regional_branch_name">Job Location <span class="text-red">*</span></label>
                    <input type="text" class="form-control" id="job_location" name="job_location" placeholder="Enter Job Location" value="{{ $single->job_location }}" required>
                    <span class="text-danger">{{ $errors->has('job_location') ? $errors->first('job_location') : '' }}</span>
                </div>

                <div class="form-group">
                    <label for="regional_branch_name">Salary <span class="text-red">*</span></label>
                    <input type="text" class="form-control" id="salary" name="salary" placeholder="Enter Salary" value="{{ $single->salary }}" required>
                    <span class="text-danger">{{ $errors->has('salary') ? $errors->first('salary') : '' }}</span>
                </div>

                <div class="form-group">
                    <label for="regional_branch_name">Vacancies <span class="text-red">*</span></label>
                    <input type="text" class="form-control" id="vacancies" name="vacancies" placeholder="Enter Vacancies" value="{{ $single->vacancies }}" required>
                    <span class="text-danger">{{ $errors->has('vacancies') ? $errors->first('vacancies') : '' }}</span>
                </div>

                <div class="form-group">
                    <label for="regional_branch_name">Deadline <span class="text-red">*</span></label>
                    <input type="text" class="form-control datepicker"  data-date-format="yyyy-mm-dd" id="deadline" name="deadline" placeholder="Enter Deadline" value="{{ $single->deadline }}" required>
                    <span class="text-danger">{{ $errors->has('deadline') ? $errors->first('deadline') : '' }}</span>
                </div>

              </div>

              <div class="col-md-6">
                
                <div class="form-group">
                  <label for="regional_branch_name">Job Description <span class="text-red">*</span></label>
                  <textarea name="description" id="description" class="col-xs-12 col-sm-4" >{{ $single->description }}</textarea>
                </div>
                
                <div class="form-group">
                    <label for="address">Upload File <span class="text-red">*</span></label>
                    <input type="file" class="form-control" name="file"  value="">
                    <code>Max File Size less than 1MB</code>
                    <span class="text-danger">{{ $errors->has('file') ? $errors->first('file') : '' }}</span>
                </div>

                <div class="form-group">
                    <label for=""></label>
                    <a href="" class="btn btn-info btn-sm btn-warning">Cancel</a>
                    <input type="submit" name="submit" class="btn btn-primary btn-sm pull-right" value="Update">
                </div>
                
              </div>
            </div>
          </form>         
          </div><!-- /.box-body -->
        </div><!-- /.box -->
      @endisset
    </section><!-- /.content -->
  </div><!-- /.content-wrapper -->

  <script src="https://cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>
  <script>
      CKEDITOR.replace('description');
  </script>

@endsection