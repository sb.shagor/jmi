@extends('admin.layout.default')
@section('title')
  Manage GMI Business
@endsection
@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Manage JMI Business</h3>
          <div class="box-tools pull-right"></div>
        </div>
        <div class="box-body color-black">
              <table id="members_list_table" class="table table-bordered table-striped">
                <thead>
                  <tr>
                      <th>SN</th>
                      <th>Title</th>
                      <th>Position</th>
                      <th>Link</th>
                      <th>Photo</th>
                      <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php if(!empty($get_all)){ 
                  	foreach ($get_all as $key => $value) { 
                  ?>
                    <tr>
                      	<td>{{ ++$key }}</td>
                        <td>{{ $value->title }}</td>
                        <td>{{ $value->position }}</td>
                        <td>{{ $value->link }}</td>
                        <td>
                          <?php if($value->file == ""){?>
                            <img height="50" width="60" src="{{ asset('public') }}/backend/img/unknown.png" />
                          <?php }else { ?>
                            <a href="{{ asset($value->file) }}" target="_blank">
                              <img height="50" width="60" src="{{ asset($value->file) }}" />
                            </a>
                          <?php } ?>
                        </td>
                    	<td>
                          <a href="{{ route('business.edit',$value->id) }}">Edit&nbsp;<i class="fa fa-edit"></i></a> | 
                          <a href="{{ route('business.destroy',$value->id) }}" style="color: red;" title="Delete" onclick="return confirm('Are you sure to delete this ?')" >Delete<i class="fa fa-trash-o fa-lg" style="color: red;"></i></a>
                        </td>
                    </tr>
                  <?php } } ?>
                </tbody>
              </table>        
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </section><!-- /.content -->
  </div><!-- /.content-wrapper -->

@endsection