<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@yield('title')</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="{{ asset('public/admin/backend/bootstrap/css/bootstrap.min.css') }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('public/admin/backend/plugins/font-awesome/css/font-awesome.min.css') }}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{ asset('public/admin/css/ionicons.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('public/admin/backend/dist/css/AdminLTE.css') }}">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{ asset('public/admin/backend/dist/css/skins/_all-skins.min.css') }}">
    <!-- date picker -->
    <link rel="stylesheet" href="{{ asset('public/admin/css/bootstrap-datepicker.min.css') }}">
    <link href="{{ asset('public/admin/css/style.css') }}" rel="stylesheet">
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('public/admin/backend/plugins/datatables/dataTables.bootstrap.css') }}">
  </head>
  <body class="hold-transition skin-blue sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">

      <header class="main-header">
        <!-- Logo -->
        <a href="#" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"></span>{{ logged_in_user_name() }}
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"> &nbsp; <i class="fa fa-spinner fa-spin fa-lg" id="ajaxLoader" style="display:none;"></i> </span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>

          <div class="col-md-10">
            @if(Session::has('message'))
              <div class="alert alert-block alert-{{Session::get("class")}}">
                <button type="button" class="close" data-dismiss="alert">
                  <i class="ace-icon fa fa-times"></i>
                </button>
                <i class="ace-icon fa fa-check green"></i>
                {{ Session::get("message") }}
              </div>
            @endif
          </div>

          <div class="navbar-custom-menu">
            
            <ul class="nav navbar-nav">
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    @php 
	                    $userID = logged_in_user_id();
	                    $user = DB::table('users')->where('id', $userID)->first();
	                  @endphp
                    @if($user->image == NULL)
                      {{ Str::substr(logged_in_user_name(), 0,1 ) }}
                    @else
                      <img src="{{ asset($user->image) }}" class="user-image" alt="User Image">
                	@endif
                    <span class="hidden-xs">
		                {{ logged_in_user_name() }}
                  	</span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                  	@php 
	                    $userID = logged_in_user_id();
	                    $user = DB::table('users')->where('id', $userID)->first();
	                  @endphp
                    @if($user->image == NULL)
                      {{ Str::substr(logged_in_user_name(), 0,1 ) }}
                    @else
                      <img src="{{ asset($user->image) }}" class="img-circle" alt="User Image">
                	  @endif
                    <p>
                      {{ logged_in_user_name() }} - {{ $user->email }}
                    </p>

                  </li>
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-right">
                      <a href="{{ URL::to('logout') }}" class="btn btn-default btn-flat">Sign out</a>
                    </div>

                    <div class="pull-left">
                      <a href="{{ url('editProfile/'.$userID) }}" class="btn btn-default btn-flat">Change Profile</a>
                    </div>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </nav>
      </header>

      <!-- =============================================== -->

		<!-- sidebar -->
        @include('admin.layout.template_left')
        <!-- //sidebar -->
        <!-- main panel -->
        @yield('content')
        <!-- main-panel ends -->

		@include('admin.layout.template_footer')