<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Auth;
use App\Staff;

class StaffController extends Controller
{
    public function index(){

        $data['get_all'] = Staff::get();
        return view('admin.staff.view', $data);
    }

    public function create(){
        $data['add'] = TRUE;
        return view('admin.staff.add', $data);
    }

    public function store(Request $request){

        $this->validate($request, [  
            'section_id' => 'required',               
            'name' => 'required',               
            'designation' => 'required',                 
        ]);

        try {
            $staff = new Staff;
            $staff->section_id = $request->section_id;
            $staff->name = $request->name;
            $staff->designation = $request->designation;
            $staff->created_by = Auth::id();
            if(!empty($request->file('file'))){
                $File = $request->file('file');
                $name = $File->getClientOriginalName();
                $ext = explode('.',$name);
                $finalName = 'staff-'.time().'.'.$ext[1];
                $uploadPath = 'public/staff/';
                $File->move($uploadPath, $finalName);
                $staff->file = $uploadPath.$finalName;
            }
            $staff->save();
             
            setMessage("message","success",saved_success());
            return redirect(route('staff.index'));

        }catch (\Exception $e) {
            $err_message = \Lang::get($e->getMessage());
            setMessage("message","danger",exception());
            return back();
        }
    }

    public function edit($id){
        $data['edit'] = TRUE;
        $data['single'] = Staff::findOrFail($id);
        return view('admin.staff.add', $data);
    }

    public function update(Request $request, $id){

        $this->validate($request, [  
            'section_id' => 'required',               
            'name' => 'required',               
            'designation' => 'required',                    
        ]);

        try {
            $staff = Staff::findOrFail($id);
            $staff->section_id = $request->section_id;
            $staff->name = $request->name;
            $staff->designation = $request->designation;
            $staff->updated_by = Auth::id();
            if ($request->file != null) {

                $path = $staff->file;
                if (file_exists($path) and $staff->file !=null) {
                    unlink($path);
                }
                $File = $request->file('file');
                $name = $File->getClientOriginalName();
                $ext = explode('.',$name);
                $finalName = 'staff-'.time().'.'.$ext[1];
                $uploadPath = 'public/staff/';
                $File->move($uploadPath, $finalName);
                $staff->file = $uploadPath.$finalName;
            }
            $staff->save();

            setMessage("message","success",updated_success());
            return redirect(route('staff.index'));

        }catch (\Exception $e) {
            $err_message = \Lang::get($e->getMessage());
            setMessage("message","danger",exception());
            return back();
        }
    }

    public function destroy($id){

        try {
            $staff = Staff::findOrFail($id);
            $path = $staff->file;
            if (file_exists($path) and $staff->file != null) {
                unlink($path);
            }
            $staff->delete();
            setMessage("message","success",deleted_success());
            return back();
        }catch (\Exception $e) {
            $err_message = \Lang::get($e->getMessage());
            setMessage("message","danger",exception());
            return back();
        }
    }

}