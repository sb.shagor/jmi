@extends('admin.layout.default')
@section('title')
Update Welcome Message
@endsection
@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <!-- Main content -->
    <section class="content">
      @isset($edit)
        <div class="box box-warning">
          <div class="box-header with-border">
            <h3 class="box-title">Update Welcome Message</h3>
            <div class="box-tools pull-right">
            </div>
          </div>
          <div class="box-body">
          <form action="{{ route('welcome.update',$single->id) }}" method="post" enctype="multipart/form-data" name="edit" autocomplete="off">
          @csrf
            <div class="row">
              <div class="col-md-10">
                
                <div class="form-group">
                  <label for="regional_branch_name">Person <span class="text-red">*</span></label>
                  <select name="person_id" class="form-control" >
                    <option value="1">Chairman</option>
                    <option value="2">Managing Director</option>
                  </select>
                  <span class="text-danger">{{ $errors->has('person_id') ? $errors->first('person_id') : '' }}</span>
                </div>

                <div class="form-group">
                  <label for="regional_branch_name">Description <span class="text-red">*</span></label>
                  <textarea name="description" id="description" class="col-xs-12 col-sm-4" >{{ $single->description }}</textarea>
                  <span class="text-danger">{{ $errors->has('description') ? $errors->first('description') : '' }}</span>
                </div>
                
                <div class="form-group">
                    <label for="address">Upload File <span class="text-red">*</span></label>
                    <input type="file" class="form-control" name="file"  value="">
                    <code>Ratio 400x400 and Max File Size less than 1MB</code>
                </div>

                <div class="form-group">
                    <label for=""></label>
                    <a href="" class="btn btn-info btn-sm btn-warning">Cancel</a>
                    <input type="submit" name="submit" class="btn btn-primary btn-sm pull-right" value="Update">
                </div>
                
              </div>
            </div>
          </form>         
          </div><!-- /.box-body -->
        </div><!-- /.box -->
      @endisset
    </section><!-- /.content -->
  </div><!-- /.content-wrapper -->

  <script src="https://cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>
  <script type="text/javascript">
      CKEDITOR.replace('description');
      document.forms['edit'].elements['person_id'].value='<?php echo $single->person_id ?>';
  </script>

@endsection