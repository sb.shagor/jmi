@extends('admin.layout.default')
@section('title')
  Create User
@endsection
@section('content')

  <div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
      @isset($add)
        <div class="box box-warning">
          <div class="box-header with-border">
            <h3 class="box-title">Create User</h3>
            <div class="box-tools pull-right">
            </div>
          </div>
          <div class="box-body">
          <form action="{{ url('save-user') }}" method="post" enctype="multipart/form-data" autocomplete="off">
          @csrf
            <div class="row">
              <div class="col-md-6">

                <div class="form-group">
                    <label for="Name">Name <span class="text-red">*</span></label>
                    <input type="text" class="form-control" id="name" name="name" placeholder="Enter Name" value="" required>
                    <span class="text-danger">{{ $errors->has('name') ? $errors->first('name') : '' }}</span>
                </div>

                <div class="form-group">
                    <label for="Name">Role <span class="text-red">*</span></label>
                    <select class="form-control" name="role_id">
                        <option value="1">Super Admin</option>
                        <option value="2">HR</option>
                    </select>
                </div>

                <div class="form-group">
                    <label for="password">Password <span class="text-red">*</span></label>
                    <input type="text" class="form-control" id="password" name="password" placeholder="Enter Password" value="" required>
                    <span class="text-danger">{{ $errors->has('password') ? $errors->first('password') : '' }}</span>
                </div>

                <div class="form-group">
                    <label for="password">Confirm Password <span class="text-red">*</span></label>
                    <input type="text" class="form-control" id="confirm_password" name="confirm_password" placeholder="Enter Confirm Password" value="" required onkeyup="checkPass();">
                    <span class="text-danger">{{ $errors->has('confirm_password') ? $errors->first('confirm_password') : '' }}</span>
                    <span id="confirmMessage" class="confirmMessage"></span>
                </div>

              </div>

              <div class="col-md-6">

                <div class="form-group">
                    <label for="password">Email <span class="text-red">*</span></label>
                    <input type="text" class="form-control" id="email" name="email" placeholder="Enter Email" value="" required>
                    <span class="text-danger">{{ $errors->has('email') ? $errors->first('email') : '' }}</span>
                </div>

                <div class="form-group">
                    <label for="password">Phone No <span class="text-red">*</span></label>
                    <input type="text" class="form-control" id="phone" name="phone" placeholder="Enter Phone No" value="" required>
                    <span class="text-danger">{{ $errors->has('phone') ? $errors->first('phone') : '' }}</span>
                </div>

                <div class="form-group">
                    <label for="address">Upload Photograph <span class="text-red">*</span></label>
                    <input type="file" id="userfile" class="form-control" name="image" value="" required onchange="getPreview('userfile','img_preview','none');">
                    <img src="<?= asset('public/backend/img/unknown.png'); ?>" style="width:100px; margin-top:5px" id="img_preview" class="img-responsive img-thumbnail"/><br>
                    <code>(Max photo size: 400x400, 512kb)</code>
                </div>

                <div class="form-group">
                    <label for=""></label>
                    <a href="" class="btn btn-info btn-sm btn-warning">Cancel</a>
                    <input type="submit" name="submit" class="btn btn-primary btn-sm pull-right" value="Save User">
                </div>
              
              </div>
            </div>
          </form>         
          </div><!-- /.box-body -->
        </div><!-- /.box -->
      @endisset
      @isset($edit)
        <div class="box box-warning">
          <div class="box-header with-border">
            <h3 class="box-title">Update Info</h3>
            <div class="box-tools pull-right">
            </div>
          </div>
          <div class="box-body">
            <div class="row">
              <div class="col-md-6">
                <form action="{{ url('updateUser') }}" name="form" method="post" name="edit" enctype="multipart/form-data" autocomplete="off">
                @csrf
                    <div class="form-group">
                        <label for="regional_branch_name">Name <span class="text-red">*</span></label>
                        <input type="text" class="form-control" id="name" name="name" placeholder="Enter Name" value="{{ $userByID->name }}" required>
                        <span class="text-danger">{{ $errors->has('name') ? $errors->first('name') : '' }}</span>
                    </div>

                    <div class="form-group">
                        <label for="password">Email <span class="text-red">*</span></label>
                        <input type="text" class="form-control" id="email" name="email" placeholder="Enter Email" value="{{ $userByID->email }}" required>
                        <span class="text-danger">{{ $errors->has('email') ? $errors->first('email') : '' }}</span>
                    </div>

                    <div class="form-group">
                        <label for="password">Phone No <span class="text-red">*</span></label>
                        <input type="text" class="form-control" id="phone" name="phone" placeholder="Enter Phone No" value="{{ $userByID->phone }}" required>
                        <span class="text-danger">{{ $errors->has('phone') ? $errors->first('phone') : '' }}</span>
                    </div>

                    <input type="hidden" name="id" value="<?php echo $userByID->id; ?>"  />

                    <div class="form-group">
                        <label for="address">Update Photograph</label>
                        <input type="file" id="userfile" class="form-control" name="image" value="" onchange="getPreview('userfile','img_preview','none');">
                        <img src="<?= (empty($userByID->image))? asset('public/backend/img/unknown.png') : asset($userByID->image) ?>" style="width:100px; margin-top:5px" id="img_preview" class="img-responsive img-thumbnail"/><br>
                        <code>(Max photo size: 400x400, 512kb)</code>
                    </div>

                    <div class="form-group">
                        <label for=""></label>
                        <a href="" class="btn btn-info btn-sm btn-warning">Cancel</a>
                        <input type="submit" name="submit" class="btn btn-primary btn-sm pull-right" value="Update User">
                    </div>
                </form>
            </div>

            <div class="col-md-6">
                <form action="{{ url('updatePassword') }}" name="notice" method="post">
                @csrf

                    <div class="form-group">
                        <label for="password">Old Password <span class="text-red">*</span></label>
                        <input type="text" class="form-control" id="old_password" name="old_password" placeholder="Enter Old Password" value="" required>
                        <span class="text-danger">{{ $errors->has('old_password') ? $errors->first('old_password') : '' }}</span>
                    </div>

                    <div class="form-group">
                        <label for="password">Password <span class="text-red">*</span></label>
                        <input type="text" class="form-control" id="password" name="password" placeholder="Enter Password" value="" required>
                        <span class="text-danger">{{ $errors->has('password') ? $errors->first('password') : '' }}</span>
                    </div>

                    <div class="form-group">
                        <label for="password">Confirm Password <span class="text-red">*</span></label>
                        <input type="text" class="form-control" id="confirm_password" name="confirm_password" placeholder="Enter Confirm Password" value="" required>
                        <span class="text-danger">{{ $errors->has('confirm_password') ? $errors->first('confirm_password') : '' }}</span>
                    </div>

                    <input type="hidden" name="id" value="{{ $userByID->id }}" class="form-control">

                    <div class="form-group">
                        <label for=""></label>
                        <a href="" class="btn btn-info btn-sm btn-warning">Cancel</a>
                        <input type="submit" name="submit" class="btn btn-primary btn-sm pull-right" value="Update Password">
                    </div>

                </form>
              </div>
            </div>
          </div><!-- /.box-body -->
        </div><!-- /.box -->
      @endisset
    </section><!-- /.content -->
  </div><!-- /.content-wrapper -->

   <script type="text/javascript">
        function checkPass(){
            var password = document.getElementById('password');
            var confirm_password = document.getElementById('confirm_password');
            var message = document.getElementById('confirmMessage');
            var goodColor = "#66cc66";
            var badColor = "#ff6666";
            if(password.value == confirm_password.value){
                confirm_password.style.backgroundColor = goodColor;
                message.style.color = goodColor;
                message.innerHTML = "Password Match!"
            }else{
                confirm_password.style.backgroundColor = badColor;
                message.style.color = badColor;
                message.innerHTML = "Password Does Not Match!"
            }
        }
    </script>

@endsection