@extends('admin.layout.default')
@section('title')
  Manage Address
@endsection
@section('content')

  <div class="content-wrapper">
    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Manage Address</h3>
          <div class="box-tools pull-right"></div>
        </div>
        <div class="box-body color-black">
              <table id="members_list_table" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>SN</th>
                    <th>Address</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                    if(!empty($office_address)){ 
                      foreach ($office_address as $key => $value) { 
                  ?>
                    <tr>
                      <td>{{ ++$key }}</td>
                      <td>{!! $value->office_address !!}</td>
                      <td>{!! $value->email !!}</td>
                      <td>{!! $value->phone !!}</td>
                        <td>
                          <a href="{{ url('edit-address',$value->id)}}">Edit&nbsp;<i class="fa fa-edit"></i></a> | 
                        </td>
                    </tr>
                  <?php } } ?>
                </tbody>
              </table>        
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </section><!-- /.content -->
  </div><!-- /.content-wrapper -->

@endsection