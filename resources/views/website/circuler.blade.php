@extends('website.layout.default')
@section('title')
    JMI | Circuler
@endsection
@section('content')

<section class="advanced-area">
    <div class="container">
        <h4>Life at JMI?</h4>
        <h1>Job Circuler</h1>
        @if(Session::has('message'))
            <div class="alert alert-block alert-{{Session::get("class")}}">
                <button type="button" class="close" data-dismiss="alert">
                    <i class="ace-icon fa fa-times"></i>
                </button>
                <i class="ace-icon fa fa-check green"></i>
                {{ Session::get("message") }}
            </div>
        @endif
        <div class="row job-cr">
            @foreach($circuler as $value)
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body all-cr">
                            <h4>{{ $value->title }}</h4>
                            <div class="row">
                                <div class="col-6">
                                    <span><i class="icofont-ui-calendar"></i> Deadline : 
                                        <?php 
                                            date('d-m-Y', strtotime($value->created_at)).'<br/>'; 
                                            echo date('d-m-Y', strtotime($value->deadline)).'<br/>'; 
                                            echo remaining_days($value->deadline).' days to go';
                                        ?>
                                    </span>
                                </div>
                                <div class="col-6" style="text-align: right;">
                                    <span><i class="icofont-users-alt-4"></i> Vacancis : {{ $value->vacancies }}</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6"><a href="{{ url('job_details/'.$value->id) }}" class="jobbtn">Details</a></div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</section>

@endsection