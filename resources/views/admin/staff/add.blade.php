@extends('admin.layout.default')
@section('title')
  Add Member
@endsection
@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <!-- Main content -->
    <section class="content">
      @isset($add)
        <div class="box box-warning">
          <div class="box-header with-border">
            <h3 class="box-title">Create New Member</h3>
            <div class="box-tools pull-right">
            </div>
          </div>
          <div class="box-body">
          <form action="{{ route('staff.store') }}" method="post" enctype="multipart/form-data" autocomplete="off">
          @csrf
            <div class="row">
              <div class="col-md-10">

                <div class="form-group">
                    <label for="Section">Section <span class="text-red">*</span></label>
                    <select class="form-control" name="section_id" required>
                      <option value="1">Board of Directors</option>
                      <option value="2">Key Management Team</option>
                    </select>
                    <span class="text-danger">{{ $errors->has('section_id') ? $errors->first('section_id') : '' }}</span>
                </div>

                <div class="form-group">
                    <label for="Name">Name <span class="text-red">*</span></label>
                    <input type="text" class="form-control" id="name" name="name" placeholder="Enter Name" value="" required>
                    <span class="text-danger">{{ $errors->has('name') ? $errors->first('name') : '' }}</span>
                </div>

                <div class="form-group">
                    <label for="Designation">Designation <span class="text-red">*</span></label>
                    <input type="text" class="form-control" id="designation" name="designation" placeholder="Enter Designation" value="" required>
                    <span class="text-danger">{{ $errors->has('designation') ? $errors->first('designation') : '' }}</span>
                </div>

                <div class="form-group">
                    <label for="address">Upload Photograph <span class="text-red">*</span></label>
                    <input type="file" id="userfile" class="form-control" name="file"  value="" required onchange="getPreview('userfile','img_preview','none');">
                    <img src="<?= asset('public/backend/img/unknown.png'); ?>" style="width:100px; margin-top:5px" id="img_preview" class="img-responsive img-thumbnail"/><br>
                    <code>(Max photo size: 400x400, 512kb)</code>
                    <span class="text-danger">{{ $errors->has('file') ? $errors->first('file') : '' }}</span>
                </div>

                <div class="form-group">
                    <label for=""></label>
                    <a href="" class="btn btn-info btn-sm btn-warning">Cancel</a>
                    <input type="submit" name="submit" class="btn btn-primary btn-sm pull-right" value="Save">
                </div>
              
              </div>
            </div>
          </form>         
          </div><!-- /.box-body -->
        </div><!-- /.box -->
      @endisset
      @isset($edit)
        <div class="box box-warning">
          <div class="box-header with-border">
            <h3 class="box-title">Update Member Info</h3>
            <div class="box-tools pull-right">
            </div>
          </div>
          <div class="box-body">
          <form action="{{ route('staff.update',$single->id) }}" name="notice" method="post" name="edit" enctype="multipart/form-data" autocomplete="off">
          @csrf
            <div class="row">
              <div class="col-md-10">

                <div class="form-group">
                    <label for="Section">Section <span class="text-red">*</span></label>
                    <select class="form-control" name="section_id" required>
                      <option value="1">Board of Directors</option>
                      <option value="2">Key Management Team</option>
                    </select>
                    <span class="text-danger">{{ $errors->has('section_id') ? $errors->first('section_id') : '' }}</span>
                </div>

                <div class="form-group">
                    <label for="regional_branch_name">Name <span class="text-red">*</span></label>
                    <input type="text" class="form-control" id="name" name="name" placeholder="Enter Name" value="{{ $single->name }}" required>
                    <span class="text-danger">{{ $errors->has('name') ? $errors->first('name') : '' }}</span>
                </div>

                <div class="form-group">
                    <label for="regional_branch_name">Designation <span class="text-red">*</span></label>
                    <input type="text" class="form-control" id="designation" name="designation" placeholder="Enter Designation" value="{{ $single->designation }}" required>
                    <span class="text-danger">{{ $errors->has('designation') ? $errors->first('designation') : '' }}</span>
                </div>

                <div class="form-group">
                    <label for="address">Update Photograph</label>
                    <input type="file" id="userfile" class="form-control" name="file"  value="" onchange="getPreview('userfile','img_preview','none');">
                    <img src="<?= (empty($single->file))? asset('public/backend/img/unknown.png') : asset($single->file) ?>" style="width:100px; margin-top:5px" id="img_preview" class="img-responsive img-thumbnail"/><br>
                    <code>(Max photo size: 400x400, 512kb)</code>
                </div>

                <div class="form-group">
                    <label for=""></label>
                    <a href="" class="btn btn-info btn-sm btn-warning">Cancel</a>
                    <input type="submit" name="submit" class="btn btn-primary btn-sm pull-right" value="Update">
                </div>

              </div>
            </div>
          </form>         
          </div><!-- /.box-body -->
        </div><!-- /.box -->

        <script type="text/javascript">
            document.forms['edit'].elements['section_id'].value='<?php echo $single->section_id ?>';
        </script>

      @endisset
    </section><!-- /.content -->
  </div><!-- /.content-wrapper -->

@endsection