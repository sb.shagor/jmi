@extends('admin.layout.default')
@section('title')
  Update Address
@endsection
@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <!-- Main content -->
    <section class="content">
        <div class="box box-warning">
          <div class="box-header with-border">
            <h3 class="box-title">Update Address</h3>
            <div class="box-tools pull-right">
            </div>
          </div>
          <div class="box-body">
          <form action="{{ url('update-address') }}" method="post">
          @csrf
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                    <label for="regional_branch_name">Email <span class="text-red">*</span></label>
                    <textarea name="email" id="email" class="col-xs-12 col-sm-4" >{{ $single->email }}</textarea>
                </div>

                <div class="form-group">
                  <label for="regional_branch_name">Phone <span class="text-red">*</span></label>
                  <textarea name="phone" id="phone" class="col-xs-12 col-sm-4" >{{ $single->phone }}</textarea>
                </div>

                <div class="form-group">
                  <label for="regional_branch_name">Address <span class="text-red">*</span></label>
                  <textarea name="office_address" id="office_address" class="col-xs-12 col-sm-4" >{{ $single->office_address }}</textarea>
                </div>

                <div class="form-group">
                  <label for="regional_branch_name">Google Map Iframe <span class="text-red">*</span></label>
                  <textarea name="map" cols="30" rows="4" class="form-control">{!! $single->map !!}</textarea>
                </div>

                <input type="hidden" name="id" class="form-control" value="{!! $single->id !!}" />

                <div class="form-group">
                    <label for=""></label>
                    <a href="" class="btn btn-info btn-sm btn-warning">Cancel</a>
                    <input type="submit" name="submit" class="btn btn-primary btn-sm pull-right" value="Update">
                </div>

              </div>

            </div>
          </form>         
          </div><!-- /.box-body -->
        </div><!-- /.box -->
    </section><!-- /.content -->
  </div><!-- /.content-wrapper -->

  <script src="https://cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>
  <script>
      CKEDITOR.replace('email');
      CKEDITOR.replace('phone');
      CKEDITOR.replace('office_address');
  </script>

@endsection