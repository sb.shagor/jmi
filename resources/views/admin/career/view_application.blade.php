@extends('admin.layout.default')
@section('title')
  Manage Application
@endsection
@section('content')

  <div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">All Application List</h3>
          <div class="box-tools pull-right"></div>
        </div>
        <div class="box-body color-black">
          <table id="members_list_table" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>SN</th>
                <th>Name</th>
                <th>Application Date</th>
                <th>Email</th>
                <th>Mobile</th>
                <th>Post</th>
                <th>Address</th>
                <th>CV</th>
              </tr>
            </thead>
            <tbody>
              <?php if(!empty($get_all)){ foreach ($get_all as $key => $value) { ?>
                <tr>
                  <td>{{ ++$key }}</td>
                  <td>{{ $value->name }}</td>
                  <td>{{ date("d-m-Y",strtotime($value->created_at)) }}</td>
                  <td>{{ $value->email }}</td>
                  <td>{{ $value->phone }}</td>
                  <td>{{ $value->post }}</td>
                  <td>{{ $value->address }}</td>
                  <td>
                    <a href="{{ asset($value->file) }}" target="_blank">
                      View CV
                    </a>
                  </td>
                </tr>
              <?php } } ?>
            </tbody>
          </table>        
        </div><!-- /.box-body -->
        <div class="box-footer">
          <div class="form-group">
            <span>
              <p id="message" class="pull-right"></p>
            </span> 
          </div>
        </div><!-- /.box-footer-->
      </div><!-- /.box -->
    </section><!-- /.content -->
  </div><!-- /.content-wrapper -->

@endsection