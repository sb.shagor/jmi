<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<div>
			<br/><strong>Application From - {{ $name }}</strong><br/>
			<p>Name:</p><strong>{{ $name }}</strong><br/>
			<p>Email:</p><strong>{{ $fromEmail }}</strong><br/>
			<p>Phone Number:</p><strong>{{ $phone }}</strong><br/>
			<p>Post:</p><strong>{{ $post }}</strong><br/>
			<p>Address:</p><strong>{!! $address !!}</strong><br/>
		</div>
	</body>
</html>