@extends('admin.layout.default')
@section('title')
	Contact Message
@endsection
@section('content')

<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Contact Message</h3>
          <div class="box-tools pull-right"></div>
        </div>
        <div class="box-body color-black">
              <table id="members_list_table" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>SN</th>
					<th>Name</th>
					<th>Mobile </th>
					<th>Email</th>
					<th>Message</th>
                  </tr>
                </thead>
                <tbody>
                   @foreach($contact as $key => $value)
						<tr>
							<td class="center">{{ $key+1 }}</td>
							<td>{{ $value->name }}</td>
							<td>{{ $value->phone }}</td>
							<td>{{ $value->email }}</td>
							<td>{{ $value->message }}</td>
						</tr>
					@endforeach
                </tbody>
              </table>        
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </section><!-- /.content -->
  </div><!-- /.content-wrapper -->

@endsection