@extends('website.layout.default')
@section('title')
    JMI | TVC / OVC
@endsection
@section('content')

<section class="advanced-area">
    <div class="container">
    <h4>OUR TVC / OVC</h4>
        <h1>TVC / OVC</h1>
        <div class="row">
            @foreach($tvc_ovc as $value)
                <div class="col-md-4">
                     {!! $value->iframe !!}
                </div>
            @endforeach
        </div>
    </div>
</section>

<style type="text/css">
    iframe{
        height: 315px !important;
        width: 100% !important;
    }
</style>

@endsection