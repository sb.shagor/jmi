@extends('website.layout.default')
@section('title')
    JMI | Job Details
@endsection
@section('content')

<section class="advanced-area">
    <div class="container">
        <h4>Life at JMI?</h4>
        <h1>Job Circuler</h1>
        <div class="row">
            <div class="col-md-8 job-context">
                <h3><b>{{ $single->title }}</b></h3>
                <br><br>
                {!! $single->description !!}
            </div>
            <div class="col-md-4 job-sum">
                <img src="{{ asset($single->file) }}">
                <div class="summury">
                    <div class="card">
                        <div class="card-body">
                        <ul>
                            <li>Job Nature : {{ $single->job_nature }}</li>
                            <li>Job Location : {{ $single->job_location }}</li>
                            <li>Salary : {{ $single->salary }}</li>
                            <li>Vacancies : {{ $single->vacancies }}</li>
                            <li>
                                <?php 
                                    date('d-m-Y', strtotime($single->created_at)).'<br/>'; 
                                    date('d-m-Y', strtotime($single->deadline)).'<br/>'; 
                                    echo remaining_days($single->deadline).' days to go';
                                ?>
                            </li>
                        </ul>
                        </div>
                    </div>
                </div>
                <!-- Button trigger modal -->
                <button type="button" class="jobbtn" data-toggle="modal" data-target="#exampleModalCenter">
                    Apply
                </button>

                <!-- Modal -->
                <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-header"></div>
                            <div class="modal-body">
                                <form action="{{ url('sendResume') }}" method="post" enctype="multipart/form-data">
                                @csrf
                                    <div class="form-group">
                                        <label for="name">Name</label>
                                        <input type="text" name="name" value="" required class="form-control" id="name" aria-describedby="emailHelp" placeholder="type your name here">
                                    </div>
                                    <div class="form-group">
                                        <label for="contact">Contact Number</label>
                                        <input type="text" name="phone" value="" required class="form-control" id="contact" placeholder="your contact">
                                    </div>
                                    <div class="form-group">
                                        <label for="email">Email</label>
                                        <input type="email" name="fromEmail" value="" required class="form-control" id="email" placeholder="your email">
                                    </div>
                                    <div class="form-group">
                                        <label for="address">Address</label>
                                        <textarea class="form-control" name="address" required id="address" placeholder="your address"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="cv">Upload CV</label>
                                        <input type="file" name="cv" value="" class="form-control" id="cv" placeholder="upload your cv" required>
                                        <small id="emailHelp" class="form-text text-muted">fill size at most 1mb</small>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                    <br><br>
                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection