<?php

use Illuminate\Support\Facades\Route;

Route::get("/clear",function(){
    Artisan::call("config:clear");
    Artisan::call("cache:clear");
    return 'Done';
});

/*---Login Credintial---*/
Route::get('admin',                 'Auth\LoginController@login');
Route::post('checkAuthentication',  'Auth\LoginController@authenticate');
Route::get('logout',                'Auth\LoginController@logout');
Route::post('reset_password',       'Auth\LoginController@resetPassword');
Route::get('reset_confirm/{email}', 'Auth\LoginController@reset_confirm');

/*---Web---*/
Route::group(['namespace' => '\App\Http\Controllers\Web'],function(){

	Route::get('/',           		  'WebController@index');
	Route::get('message_md',          'WebController@message_md');
	Route::get('message_ch',          'WebController@message_ch');
	Route::get('corporate_profile',   'WebController@corporate_profile');
	Route::get('our_mission',         'WebController@mission');
	Route::get('board_member',        'WebController@board_member');
	Route::get('key_person',          'WebController@key_person');
	Route::get('gmi_business',        'WebController@business');
	Route::get('details/{id}',        'WebController@news_details');
	Route::get('news_event',          'WebController@news_event');
	Route::get('video',               'WebController@video');
	Route::get('tvc_ovc',             'WebController@tvc_ovc');
	Route::get('photo_gallery',       'WebController@photo_gallery');
	Route::get('under_construction',  'WebController@under_construction');
	Route::get('album-images/{id}',   'WebController@albumImages')->name('album-images');
	Route::get('circuler',            'WebController@circuler');
	Route::get('why_join_us',         'WebController@why_join_us');
	Route::get('job_details/{id}',    'WebController@job_details');
	Route::get('contact_us',          'WebController@contact');
	Route::post('sendContactMail',    'WebController@sendContactMail')->name('sendContactMail');
	Route::post('sendResume',         'WebController@sendResume');

});

/*---Admin---*/
Route::group([ 'middleware' => 'checkAuthentication'], function(){

	Route::group(['namespace' => '\App\Http\Controllers\Admin'],function(){

		Route::get('/dashboard',        'AdminController@index');
		Route::get('feedback', 		    'AdminController@contactMessage');
		Route::get('manage-address',    'AdminController@officeAddress');
		Route::get('edit-address/{id}', 'AdminController@editAddress');
		Route::post('update-address',   'AdminController@updateAddress');

		Route::get('manage-slide',      	'SliderController@index');
		Route::get('add-slide',    		    'SliderController@add');
		Route::post('save-slide', 		    'SliderController@store');
		Route::get('edit-slide/{id}', 		'SliderController@edit');
		Route::post('update-slide', 		'SliderController@update');
		Route::get('delete-slide/{id}', 	'SliderController@delete');

		Route::prefix('/notices')->name('notices.')->group(function(){
			Route::get('/', 'NoticeController@index')->name('index');
			Route::get('/create', 'NoticeController@create')->name('create');
			Route::post('/store', 'NoticeController@store')->name('store');
			Route::get('edit/{id}', 'NoticeController@edit')->name('edit');
			Route::post('update/{id}', 'NoticeController@update')->name('update');
			Route::get('destroy/{id}', 'NoticeController@destroy')->name('destroy');
		});

		Route::prefix('/careers')->name('careers.')->group(function(){
			Route::get('/', 'CareerController@index')->name('index');
			Route::get('application', 'CareerController@application')->name('application');
			Route::get('/create', 'CareerController@create')->name('create');
			Route::get('/join_us', 'CareerController@join_us')->name('join_us');
			Route::post('/store', 'CareerController@store')->name('store');
			Route::get('edit/{id}', 'CareerController@edit')->name('edit');
			Route::post('update/{id}', 'CareerController@update')->name('update');
			Route::post('update_join_us/{id}', 'CareerController@update_join_us')->name('update_join_us');
			Route::get('destroy/{id}', 'CareerController@destroy')->name('destroy');
		});

		Route::prefix('/website')->name('website.')->group(function(){
			Route::get('edit/{id}', 'WebsiteInfoController@index')->name('edit');
			Route::post('update/{id}', 'WebsiteInfoController@update')->name('update');
		});

		Route::prefix('/about')->name('about.')->group(function(){
			Route::get('/', 		  'AboutController@index')->name('index');
			Route::get('edit/{id}',   'AboutController@edit')->name('edit');
			Route::post('update/{id}','AboutController@update')->name('update');
		});

		Route::prefix('/welcome')->name('welcome.')->group(function(){
			Route::get('/', 'WelcomeMessageController@index')->name('index');
			Route::get('edit/{id}', 'WelcomeMessageController@edit')->name('edit');
			Route::post('update/{id}', 'WelcomeMessageController@update')->name('update');
		});

		Route::prefix('/mission')->name('mission.')->group(function(){
			Route::get('/', 'OurMissionController@index')->name('index');
			Route::get('edit/{id}', 'OurMissionController@edit')->name('edit');
			Route::post('update/{id}', 'OurMissionController@update')->name('update');
		});

		Route::prefix('/staff')->name('staff.')->group(function(){
			Route::get('/', 'StaffController@index')->name('index');
			Route::get('/create', 'StaffController@create')->name('create');
			Route::post('/store', 'StaffController@store')->name('store');
			Route::get('edit/{id}', 'StaffController@edit')->name('edit');
			Route::post('update/{id}', 'StaffController@update')->name('update');
			Route::get('destroy/{id}', 'StaffController@destroy')->name('destroy');
		});

		Route::prefix('/business')->name('business.')->group(function(){
			Route::get('/', 'BusinessController@index')->name('index');
			Route::get('/create', 'BusinessController@create')->name('create');
			Route::post('/store', 'BusinessController@store')->name('store');
			Route::get('edit/{id}', 'BusinessController@edit')->name('edit');
			Route::post('update/{id}', 'BusinessController@update')->name('update');
			Route::get('destroy/{id}', 'BusinessController@destroy')->name('destroy');
		});

		Route::prefix('/joint')->name('joint.')->group(function(){
			Route::get('/', 'JointVenturesController@index')->name('index');
			Route::get('edit/{id}', 'JointVenturesController@edit')->name('edit');
			Route::post('update/{id}', 'JointVenturesController@update')->name('update');
		});

		Route::prefix('/videos')->name('videos.')->group(function(){
			Route::get('/', 'VideoController@index')->name('index');
			Route::get('/create', 'VideoController@create')->name('create');
			Route::post('/store', 'VideoController@store')->name('store');
			Route::get('edit/{id}', 'VideoController@edit')->name('edit');
			Route::post('update/{id}', 'VideoController@update')->name('update');
			Route::get('destroy/{id}', 'VideoController@destroy')->name('destroy');
		});

		Route::get('manage-album', 		 'AlbumController@index');
		Route::get('add-album', 		 'AlbumController@add');
		Route::post('save-album', 		 'AlbumController@store');
		Route::get('edit-album/{id}', 	 'AlbumController@edit');
		Route::post('update-album',      'AlbumController@update');
		Route::get('delete-album/{id}',  'AlbumController@delete');

		Route::get('manage-gallery', 	  'GalleryController@index');
		Route::get('add-gallery', 		  'GalleryController@add');
		Route::post('save-gallery', 	  'GalleryController@store');
		Route::get('edit-gallery/{id}',   'GalleryController@edit');
		Route::post('update-gallery',     'GalleryController@update');
		Route::get('delete-gallery/{id}', 'GalleryController@delete');

		/*---User---*/
		Route::get('manage-user',              'UserController@manageUser');
		Route::get('add-user',                 'UserController@createUser');
		Route::post('save-user',               'UserController@storeUser');
		Route::get('doUserInactive/{user_id}', 'UserController@inactiveUser');
		Route::get('doUserActive/{user_id}',   'UserController@activeUser');
		Route::get('editProfile/{user_id}',    'UserController@editUser');
		Route::post('updateUser',              'UserController@updateUser');
		Route::get('changePassword/{user_id}', 'UserController@changeUserPassword');
		Route::post('updatePassword',          'UserController@updatePassword');
		Route::get('delete-profile/{user_id}', 'UserController@delete_profile');
		Route::get('SAtoHR/{user_id}',         'UserController@SAtoHR');
		Route::get('HRtoSA/{user_id}',         'UserController@HRtoSA');

	});


});