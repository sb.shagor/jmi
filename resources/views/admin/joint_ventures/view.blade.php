@extends('admin.layout.default')
@section('title')
  Manage Joint Ventures
@endsection
@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Manage Joint Ventures</h3>
          <div class="box-tools pull-right"></div>
        </div>
        <div class="box-body color-black">
              <table id="members_list_table" class="table table-bordered table-striped">
                <thead>
                  <tr>
                      <th>SN</th>
                      <th>Title</th>
                      <th>Link</th>
                      <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php if(!empty($get_all)){ 
                  	foreach ($get_all as $key => $value) { 
                  ?>
                    <tr>
                      	<td>{{ ++$key }}</td>
                        <td>{{ $value->title }}</td>
                        <td>{{ $value->link }}</td>
                    	<td>
                          <a href="{{ route('joint.edit',$value->id) }}">Edit&nbsp;<i class="fa fa-edit"></i></a>
                        </td>
                    </tr>
                  <?php } } ?>
                </tbody>
              </table>        
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </section><!-- /.content -->
  </div><!-- /.content-wrapper -->

@endsection