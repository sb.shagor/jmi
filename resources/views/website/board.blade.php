@extends('website.layout.default')
@section('title')
    JMI | Board of Directors
@endsection
@section('content')

<section class="advanced-area">
    <div class="container">
        <h1>Board of Directors</h1>
        @foreach($board_member as $value)
            <div class="col-md-offset-1 col-md-4 col-sm-4 col-xs-12">
                <div class="board-box">
                    <img src="{{ asset($value->file) }}"/>
                    <h3>{{ $value->name }}</h3>
                    <h5>{{ $value->designation }}</h5>
                    <h4>JMI Group</h4>
                </div>
            </div>
        @endforeach
    </div>
</section>

@endsection