<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use DB;
use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Artisan;
use App\Business;
use App\Notice;
use App\Career;
use App\WelcomeMessage;
use App\Staff;
use App\OurMission;
use App\Video;

class WebController extends Controller
{
	public function index(){
      
	    Artisan::call("config:clear");
	    Artisan::call("cache:clear");
	    Artisan::call("route:clear");
	    Artisan::call("view:clear");

		$data['sliders'] = DB::table('tbl_slider')->get();
		$data['businesses'] = Business::limit(8)->orderBy('position','ASC')->get();
		$data['notices'] = Notice::get();
		$data['md_message'] = WelcomeMessage::where('person_id', 2)->first();
		return view('website.home',$data);
	}

	public function business(){
		$data['businesses'] = Business::orderBy('position','ASC')->get();
		return view('website.business', $data);
	}

	public function under_construction(){
		return view('website.under_construction');
	}

	public function message_md(){
		$data['md_message'] = WelcomeMessage::where('person_id', 2)->first();
		return view('website.message_md', $data);
	}

	public function message_ch(){
		$data['ch_message'] = WelcomeMessage::where('person_id', 1)->first();
		return view('website.message_ch', $data);
	}

	public function corporate_profile(){
		$data['mission'] = OurMission::where('section_id', 1)->first();
		return view('website.corporate_profile', $data);
	}

	public function mission(){
		$data['mission'] = OurMission::where('section_id', 2)->first();
		return view('website.mission', $data);
	}

	public function board_member(){
		$data['board_member'] = Staff::where('section_id', 1)->get();
		return view('website.board', $data);
	}

	public function key_person(){
		$data['key_member'] = Staff::where('section_id', 2)->get();
		return view('website.key', $data);
	}

	public function photo_gallery(){
		$data['images'] = DB::table('tbl_gallery')->where('section_id', 1)->get();
		return view('website.gallery', $data);
	}

	public function video(){
		$data['videos'] = Video::where('category','video')->get();
		return view('website.video', $data);
	}

	public function tvc_ovc(){
		$data['tvc_ovc'] = Video::where('category','tvc_ovc')->get();
		return view('website.tvc_ovc', $data);
	}

	public function news_event(){
		$data['news_events'] = Notice::where('section_id','!=',1)->get();
		$data['notices'] = Notice::where('section_id',1)->get();
		return view('website.news_event', $data);
	}

	public function news_details($id){
		$data['single'] = Notice::where('id', $id)->first();
		$data['notices'] = Notice::where('section_id',1)->get();
		return view('website.news_details', $data);
	}

	public function why_join_us(){
		$data['single'] = DB::table('tbl_join_us')->first();
		return view('website.why_join_us', $data);
	}

	public function circuler(){
		$data['circuler'] = Career::get();
		return view('website.circuler', $data);
	}

	public function job_details($id){
		$data['single'] = Career::where('id', $id)->first();
		Session::put('post', $data['single']->title);
		return view('website.job_details', $data);
	}

	public function contact(){
		
		return view('website.contact');
	}

	public function sendContactMail(Request $request){

		$validate = Validator::make(Input::all(), [
			'g-recaptcha-response' => 'required|recaptchav3:register,0.5'
		]);

		$score = RecaptchaV3::verify($request->get('g-recaptcha-response'), 'register');
		if($score > 0.7) {
		    return 'good';
		} elseif($score > 0.3) {
		    return 'bad';
		} else {
		    return abort(400, 'You are most likely a bot');
		}

	    $data = array(
	        'name'        => $request->name,
	        'fromEmail'   => $request->fromEmail,
	        'subject'     => 'General Query',
	        'phone'       => $request->phone,
	        'toEmail'     => 'shagor@wanitbd.com',
	        'bodymessage' => $request->bodymessage
	    );
	        
	    $result = Mail::send('emails.contact_mail', $data, function ($message) use ($data) {
	        $message->from($data['fromEmail'], $data['name']);
	        $message->to($data['toEmail'])->subject($data['subject']);
	    });

        $insert = DB::table('tbl_contact')
	        ->insert([
	            'name'    => $request->name,
	            'email'   => $request->fromEmail,
	            'phone'   => $request->phone,
	            'message' => $request->bodymessage
	        ]);

        if($insert && $result){
            setMessage('message','success','Your message has been save and sent successfully !');
        }elseif($insert){
        	setMessage('message','success','Your message has been saved successfully !');
        }elseif($result){
        	setMessage('message','success','Your message has been send successfully !');
        }else{
        	setMessage('message','success','Failed to sent');
        }
	    return redirect('contact_us');

	}//sendContactMail

	public function sendResume(Request $request){

		$cv = $request->file('cv');
	    $data = array(
	        'name'      => $request->name,
	        'fromEmail' => $request->fromEmail,
	        'subject'   => 'Mail From Circuler',
	        'phone'     => $request->phone,
	        'post'      => Session::get('post'),
	        'toEmail'   => 'shagor@wanitbd.com',
	        'address'   => $request->address,
	        'cv'        => $cv,
	    );
	        
	    $result = Mail::send('emails.application_mail', $data, function ($message) use ($data) {
	        $message->from($data['fromEmail'], $data['name']);
	        $message->to($data['toEmail'])->subject($data['subject']);
	        $message->attach($data['cv']->getRealPath(), [
	          'as' => $data['cv']->getClientOriginalName(),
	          'mime' => $data['cv']->getMimeType()
	        ]);
	    });

	    if(!empty($request->file('cv'))){
            $File = $request->file('cv');
            $name = $File->getClientOriginalName();
            $ext = explode('.',$name);
            $finalName = 'cv-'.time().'.'.$ext[1];
            $uploadPath = 'public/uploads/';
            $File->move($uploadPath, $finalName);
            $fileURL = $uploadPath.$finalName;
        }

        $insert = DB::table('tbl_application')
	        ->insert([
	            'name'    => $request->name,
	            'email'   => $request->fromEmail,
	            'phone'   => $request->phone,
	            'post'    => Session::get('post'),
	            'address' => $request->address,
	            'file'    => $fileURL,
	        ]);

        if($insert && $result){
            setMessage('message','success','Save and sent successfully !');
        }elseif($insert){
        	setMessage('message','success','Saved successfully !');
        }elseif($result){
        	setMessage('message','success','Send successfully !');
        }else{
        	setMessage('message','success','Failed to sent');
        }
	    return redirect('circuler');

	}//sendResume

}//WebController