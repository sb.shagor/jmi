@extends('admin.layout.default')
@section('title')
  Update Join Us
@endsection
@section('content')

  <div class="content-wrapper">
    <section class="content">
        <div class="box box-warning">
          <div class="box-header with-border">
            <h3 class="box-title">Update</h3>
            <div class="box-tools pull-right">
            </div>
          </div>
          <div class="box-body">
          <form action="{{ route('careers.update_join_us',$single->id) }}" method="post" enctype="multipart/form-data" autocomplete="off">
          @csrf
            <div class="row">
              <div class="col-md-6">
                
                <div class="form-group">
                  <label for="regional_branch_name">Description <span class="text-red">*</span></label>
                  <textarea name="description" id="description" class="col-xs-12 col-sm-4" >{{ $single->description }}</textarea>
                </div>

                <div class="form-group">
                    <label for=""></label>
                    <a href="" class="btn btn-info btn-sm btn-warning">Cancel</a>
                    <input type="submit" name="submit" class="btn btn-primary btn-sm pull-right" value="Update">
                </div>
                
              </div>
            </div>
          </form>         
          </div><!-- /.box-body -->
        </div><!-- /.box -->
    </section><!-- /.content -->
  </div><!-- /.content-wrapper -->

  <script src="https://cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>
  <script>
      CKEDITOR.replace('description');
  </script>

@endsection