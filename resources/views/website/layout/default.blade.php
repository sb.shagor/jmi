<!DOCTYPE html>
<html lang="zxx">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>@yield('title')</title>
        <link rel="stylesheet" type="text/css" href="{{ url('public/web') }}/css/animate.min.css">
        <link rel="stylesheet" type="text/css" href="{{ url('public/web') }}/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="{{ url('public/web') }}/css/bootstrap-theme.min.css">
        <link rel="stylesheet" type="text/css" href="{{ url('public/web') }}/css/icofont.min.css">
        <link rel="stylesheet" type="text/css" href="{{ url('public/web') }}/css/style.css"/>
    </head>
    <body>
        <!----------Header---------->
        <section class="header-area">
            <div class="container">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="logo">
                        <a href="{{ url('/') }}"><img src="{{ asset($website_info->logo) }}" alt="Logo" class="img-responsive"/></a>
                    </div>
                </div>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <div class="contact-head">
                        <div class="box">
                            <a href="#"><i class="icofont-ui-call"></i>Call Us Now</a>
                        </div>
                        <div class="icon-bar">
                            <a href="{{ $website_info->facebook }}" target="_blank"><i class="icofont-facebook"></i></a>
                            <a href="{{ $website_info->youtube }}" target="_blank"><i class="icofont-youtube-play"></i></a> 
                            <a href="{{ $website_info->linkedin }}" target="_blank"><i class="icofont-linkedin"></i></a> 
                            <a href="{{ $website_info->twitter }}" target="_blank"><i class="icofont-twitter"></i></a> 
                            <a href="{{ $website_info->instragam }}" target="_blank"><i class="icofont-instagram"></i></a> 
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!----------Navigation---------->
        <section class="navbar-area">
            <nav class="navbar navbar-default" data-spy="affix" data-offset-top="80"> 
                <div class="container">
                    <div class="navbar-navbar">
                        <div class="navbar-header">  
                            <div class="logo logo-before">
                                <a href="{{ url('/') }}"><img src="{{ asset($website_info->logo) }}" alt="Logo" class="img-responsive"/></a>
                            </div>
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul id="myDiv" class="nav navbar-nav">
                                <li><a href="{{ url('/') }}">Home</a></li>
                                <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Discover JMI<i class="fa fa-angle-double-down"></i></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="{{ url('message_md') }}">Message from MD</a></li>
                                        <li><a href="{{ url('message_ch') }}">Message from Chairman</a></li>
                                        <li><a href="{{ url('corporate_profile') }}">Corporate Profile</a></li>
                                        <li><a href="{{ url('our_mission') }}">Mission, Vision & Objective</a></li>
                                        <li><a href="{{ url('key_person') }}">Key Management Team</a></li>
                                    </ul>
                                </li>
                                <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">JMI Business<i class="fa fa-angle-double-down"></i></a>
                                    <ul class="dropdown-menu ext">
                                        <span class="col-md-6 col-xs-12">
                                            <li><a href="https://www.jmisyringe.com/" target="blank">JMI Syringes & Medical Devices Ltd.</a></li>
                                            <li><a href="http://www.niprojmipharma.com/" target="blank">NIPRO JMI Pharma Ltd.</a></li>
                                            <li><a href="https://www.jmigroup-bd.com/nipro-jmi-co-ltd" target="blank">NIPRO JMI Company Ltd.</a></li>
                                            <li><a href="https://www.jmigroup-bd.com/NIPRO-JMI-Dialysis-Centre-Ltd" target="blank">NIPRO JMI Dialysis Centre Ltd.</a></li>
                                            <li><a href="https://jhrml-bd.com/" target="blank">JMI Hospital Requisite MFG. Ltd.</a></li>
                                            <li><a href="{{ url('under_construction') }}" target="blank">NIPRO JMI Marketing Ltd.</a></li>
                                            <li><a href="https://www.jmigroup-bd.com/JMI-Vaccine-Ltd" target="blank">JMI Vaccine Ltd.</a></li>
                                            <li><a href="{{ url('under_construction') }}" target="blank">JMI Printing & Packaging Ltd.</a></li>
                                            <li><a href="http://www.jmibuilders.com/" target="blank">JMI Builders & Construction Ltd.</a></li>
                                            <li><a href="{{ url('under_construction') }}" target="blank">JMI SONLU Appliance Ltd.</a></li>
                                            <li><a href="{{ url('under_construction') }}" target="blank">JMI Export-Import Company Ltd.</a></li>
                                            <li><a href="{{ url('under_construction') }}" target="blank">JMI Engineering Ltd.</a></li>
                                            <li><a href="{{ url('under_construction') }}" target="blank">JMI Ind. Research & Toxicology Ltd.</a></li>
                                            <li><a href="http://www.jmigas.com/" target="blank">JMI Industrial Gas Ltd.</a></li>
                                            <li><a href="{{ url('under_construction') }}" target="blank">JMI LPG & Petroleum Ltd.</a></li>
                                            <li><a href="http://www.jmicylinders.com/" target="blank">JMI Cylinders Ltd.</a></li>
                                        </span>
                                        
                                        <span class="col-md-6 col-xs-12">
                                            <li><a href="{{ url('under_construction') }}" target="blank">JMI Sankur Auto Tank Ltd.</a></li>
                                            <li><a href="{{ url('under_construction') }}" target="blank">JMI Auto Gas & Conversion</a></li>
                                            <li><a href="{{ url('under_construction') }}" target="blank">JMI Sankur Valve & Bung Ltd.</a></li>
                                            <li><a href="{{ url('under_construction') }}" target="blank">JMI Hollow Block Co. Ltd.</a></li>
                                            <li><a href="{{ url('under_construction') }}" target="blank">JMI CNG Dispensing Co. Ltd.</a></li>
                                            <li><a href="{{ url('under_construction') }}" target="blank">JMI Safe Transportation Ltd.</a></li>
                                            <li><a href="{{ url('under_construction') }}" target="blank">Advance Travel Planner Ltd.</a></li>
                                            <li><a href="http://www.bdclinical.com/" target="blank">Bangladesh Clinical Trials Ltd.</a></li>
                                            <li><a href="http://www.shopnojoyee.com/" target="blank">Shopnojoyee Fashion Wears Ltd.</a></li>
                                            <li><a href="{{ url('under_construction') }}" target="blank">Sunrise Research & Consultancy Ltd.</a></li>
                                            <li><a href="{{ url('under_construction') }}" target="blank">JMI Restora</a></li>
                                            <li><a href="{{ url('under_construction') }}" target="blank">JMI Pharmacy</a></li>
                                            <li><a href="{{ url('under_construction') }}" target="blank">Sun Myung International (Pvt.) Ltd.</a></li>
                                            <li><a href="{{ url('under_construction') }}" target="blank">M/S Razzaq Enterprise</a></li>
                                            <li><a href="http://cisdbd.net/" target="blank">Cardiff International Ltd.</a></li>
                                            <li><a href="{{ url('under_construction') }}" target="blank">JMI AGRO FARMS & FISHERIES</a></li>
                                        </span>
                                    </ul>
                                </li>
                                <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Joint Ventures<i class="fa fa-angle-double-down"></i></a>
                                    <ul class="dropdown-menu">
                                        @foreach($joint_ventures as $value)
                                            <li><a href="{{ $value->link }}" target="_blank">{{ $value->title }}</a></li>
                                        @endforeach
                                    </ul>
                                </li>
                                <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Media<i class="fa fa-angle-double-down"></i></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="{{ url('news_event') }}">News & Events</a></li>
                                        <li><a href="{{ url('video') }}">Video Gallery</a></li>
                                        <li><a href="{{ url('photo_gallery') }}">Photo Gallery</a></li>
                                        <li><a href="{{ url('tvc_ovc') }}">TVC/OVC</a></li>
                                    </ul>
                                </li> 
                                
                                <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Career<i class="fa fa-angle-double-down"></i></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="{{ url('why_join_us') }}">Why join with us</a></li>
                                        <li><a href="{{ url('circuler') }}">Our latest Circular</a></li>
                                    </ul>
                                </li>         
                                <li><a href="{{ url('contact_us') }}">Contact Us</a></li>  
                            </ul>
                        </div>
                    </div>
                </div>
            </nav>
        </section>

        <section class="social-icon-bar">
            <a href="http://jmi.com.bd/" target="_blank" class="icocart"><img src="{{ asset('public/web') }}/img/interface/shop.png" alt="#"></a>
            <a href="{{ $website_info->facebook }}" target="_blank"><i class="icofont-facebook"></i></a>
            <a href="{{ $website_info->twitter }}" target="_blank"><i class="icofont-twitter"></i></a> 
            <a href="{{ $website_info->instragam }}" target="_blank"><i class="icofont-instagram"></i></a> 
            <a href="{{ $website_info->youtube }}" target="_blank"><i class="icofont-youtube-play"></i></a> 
        </section>
        <!-- header end -->
        <!-- body content -->
        @yield('content')
        <!-- body content end-->
        <!----------Start Footer-------->
        @include('website.layout.footer')
        <!----------End Footer---------->

    </body>
</html>