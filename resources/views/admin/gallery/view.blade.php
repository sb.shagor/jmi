@extends('admin.layout.default')
@section('title')
  Manage Photo Gallery
@endsection
@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Manage Photo Gallery</h3>
          <div class="box-tools pull-right"></div>
        </div>
        <div class="box-body color-black">
              <table id="members_list_table" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>SN</th>
                    <th>Section</th>
                    <th>Image</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php if(!empty($gallery)){ 
                  	foreach ($gallery as $key => $value) { 
                  ?>
                    <tr>
                      	<td>{{ ++$key }}</td>
                        <td>{{ ($value->section_id==1)?'Gallery':'Awards & Winning' }}</td>
                        <td>
                          <?php if($value->photo == ""){?>
                            <img height="50" width="60" src="{{ asset('public') }}/backend/img/unknown.png" title="<?php echo $value->photo;?>" />
                          <?php }else { ?>
                            <a href="{{ asset($value->photo) }}" target="_blank">
                              <img height="50" width="60" src="{{ asset($value->photo) }}" />
                            </a>
                          <?php } ?>
                        </td>
                    	  <td>
                          <a href="{{ url('edit-gallery/'.$value->id)}}">Edit&nbsp;<i class="fa fa-edit"></i></a> | 
                          <a href="{{ url('delete-gallery/'.$value->id) }}" style="color: red;" title="Delete" onclick="return confirm('Are you sure to delete this ?')" >Delete&nbsp;<i class="fa fa-trash-o fa-lg" style="color: red;"></i></a>
                        </td>
                    </tr>
                  <?php } } ?>
                </tbody>
              </table>        
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </section><!-- /.content -->
  </div><!-- /.content-wrapper -->

@endsection