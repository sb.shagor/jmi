<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
    	<ul class="sidebar-menu">
        	<li>
        		<a href="{{ url('dashboard') }}"><i class="fa fa-home"></i> <span>Dashboard</span> <i class="fa fa-angle-right pull-right"></i></a>
        	</li>

        	@if(logged_in_role_id() == 1)

	        	<li class="treeview">
					<a href="javascript:">
						<i class="fa fa-slideshare"></i> <span>Slider Panel</span> <i class="fa fa-angle-left pull-right"></i>
					</a>
					<ul class="treeview-menu">
						<li><a href="{{ url('add-slide') }}"><i class="fa fa-plus"></i>Add Slider</a></li>
						<li><a href="{{ url('manage-slide') }}"><i class="fa fa-eye"></i>View Slider</a></li>
					</ul>
	            </li>

	        	<li>
	        		<a href="{{ route('website.edit',1) }}"><i class="fa fa-info-circle"></i> <span>Website Info</span> <i class="fa fa-angle-right pull-right"></i></a>
	        	</li>

	            <li class="treeview">
					<a href="javascript:">
						<i class="fa fa-building"></i> <span>Circuler Panel</span> <i class="fa fa-angle-left pull-right"></i>
					</a>
					<ul class="treeview-menu">
						<li><a href="{{ route('careers.join_us') }}"><i class="fa fa-plus"></i>Join Us</a></li>
						<li><a href="{{ route('careers.create') }}"><i class="fa fa-plus"></i>Add Circuler</a></li>
						<li><a href="{{ route('careers.index') }}"><i class="fa fa-eye"></i>View Circuler</a></li>
						<li><a href="{{ route('careers.application') }}"><i class="fa fa-eye"></i>View Application</a></li>
					</ul>
	            </li>

	            <li>
	            	<a href="{{ route('welcome.index') }}"><i class="fa fa-comments"></i> <span>Welcome Message Panel</span> <i class="fa fa-angle-right pull-right"></i></a>
	            </li>

	            <li>
	            	<a href="{{ route('mission.index') }}"><i class="fa fa-user-md"></i> <span>Profile & Mission Panel</span> <i class="fa fa-angle-right pull-right"></i></a>
	            </li>

	            <li class="treeview">
					<a href="javascript:">
						<i class="fa fa-users"></i> <span>Member Panel</span> <i class="fa fa-angle-left pull-right"></i>
					</a>
					<ul class="treeview-menu">
						<li><a href="{{ route('staff.create') }}"><i class="fa fa-plus"></i>Add Member</a></li>
						<li><a href="{{ route('staff.index') }}"><i class="fa fa-eye"></i>View Member</a></li>
					</ul>
	            </li>

	            <li>
	            	<a href="{{ route('joint.index') }}"><i class="fa fa-venus-mars"></i> <span>Joint Ventures Panel</span> <i class="fa fa-angle-right pull-right"></i></a>
	            </li>

	            <li class="treeview">
					<a href="javascript:">
						<i class="fa fa-briefcase"></i> <span>JMI Business Panel</span> <i class="fa fa-angle-left pull-right"></i>
					</a>
					<ul class="treeview-menu">
						<li><a href="{{ route('business.create') }}"><i class="fa fa-plus"></i>Add JMI Business</a></li>
						<li><a href="{{ route('business.index') }}"><i class="fa fa-eye"></i>View JMI Business</a></li>
					</ul>
	            </li>

	            <li class="treeview">
					<a href="javascript:">
						<i class="fa fa-newspaper-o"></i> <span>Notice, News & Event Panel</span> <i class="fa fa-angle-left pull-right"></i>
					</a>
					<ul class="treeview-menu">
						<li><a href="{{ route('notices.create') }}"><i class="fa fa-plus"></i>Add Notice</a></li>
						<li><a href="{{ route('notices.index') }}"><i class="fa fa-eye"></i>View Notice</a></li>
					</ul>
	            </li>

	            <li class="treeview">
					<a href="javascript:">
						<i class="fa fa-camera"></i> <span>Media Panel</span> <i class="fa fa-angle-left pull-right"></i>
					</a>
					<ul class="treeview-menu">
						<li><a href="{{ url('add-gallery') }}"><i class="fa fa-plus"></i>Add Gallery</a></li>
						<li><a href="{{ url('manage-gallery') }}"><i class="fa fa-eye"></i>View Gallery</a></li>
					</ul>
	            </li>

	            <li class="treeview">
					<a href="javascript:">
						<i class="fa fa-video-camera"></i> <span>Video Panel</span> <i class="fa fa-angle-left pull-right"></i>
					</a>
					<ul class="treeview-menu">
						<li><a href="{{ route('videos.create') }}"><i class="fa fa-plus"></i>Add Video</a></li>
						<li><a href="{{ route('videos.index') }}"><i class="fa fa-eye"></i>View Video</a></li>
					</ul>
	            </li>

	            <li>
	            	<a href="{{ url('manage-address') }}"><i class="fa fa-map"></i> <span>Contact Panel</span> <i class="fa fa-angle-right pull-right"></i></a>
	            </li>

	            <li>
	        		<a href="{{ url('feedback') }}"><i class="fa fa-info-circle"></i> <span>Feedback Panel</span> <i class="fa fa-angle-right pull-right"></i></a>
	        	</li>

	            <li class="treeview">
					<a href="javascript:">
						<i class="fa fa-users"></i> <span>User Panel</span> <i class="fa fa-angle-left pull-right"></i>
					</a>
					<ul class="treeview-menu">
						<li><a href="{{ url('add-user') }}"><i class="fa fa-plus"></i>Add User</a></li>
						<li><a href="{{ url('manage-user') }}"><i class="fa fa-eye"></i>View User</a></li>
					</ul>
	            </li>
	        @endif
	        @if(logged_in_role_id() == 2)
	        	<li class="treeview">
					<a href="javascript:">
						<i class="fa fa-building"></i> <span>Circuler Panel</span> <i class="fa fa-angle-left pull-right"></i>
					</a>
					<ul class="treeview-menu">
						<li><a href="{{ route('careers.create') }}"><i class="fa fa-plus"></i>Add Circuler</a></li>
						<li><a href="{{ route('careers.index') }}"><i class="fa fa-eye"></i>View Circuler</a></li>
						<li><a href="{{ route('careers.application') }}"><i class="fa fa-eye"></i>View Application</a></li>
					</ul>
	            </li>
	        @endif
      	</ul>
    </section>
    <!-- /.sidebar -->
</aside>