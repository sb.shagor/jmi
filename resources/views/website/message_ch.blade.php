@extends('website.layout.default')
@section('title')
    JMI | Message from Chairman
@endsection
@section('content')

<section class="advanced-area">
    <div class="container">
        <h1>Chairman of JMI Group</h1>
        <div class="col-md-5 col-sm-4 col-xs-12 no-padding">
            <div class="adv-img">
                <img class="main" src="{{ asset('public/web') }}/img/interface/adv.png" alt="1">
                <img class="first" src="{{ asset('public/web') }}/img/interface/adv-1.png" alt="2">
                <img class="second" src="{{ asset('public/web') }}/img/interface/adv-2.png" alt="3">
            </div>
        </div>
        <div class="col-md-7 col-sm-8 col-xs-12">
            <div class="adv-detail">
                <img class="ch" src="{{ asset($ch_message->file) }}" alt="1">
                {!! $ch_message->description !!}
            </div>
        </div>
    </div>
</section>

@endsection