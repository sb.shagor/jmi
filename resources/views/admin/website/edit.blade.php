@extends('admin.layout.default')
@section('title')
  Update Website Info
@endsection
@section('content')

  <div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
      @isset($edit)
        <div class="box box-warning">
          <div class="box-header with-border">
            <h3 class="box-title">Update Website Info</h3>
            <div class="box-tools pull-right">
            </div>
          </div>
          <div class="box-body">
          <form action="{{ route('website.update',$single->id) }}" method="post" enctype="multipart/form-data" name="edit" autocomplete="off">
          @csrf
            <div class="row">
              <div class="col-md-6">

                <div class="form-group">
                  <label for="regional_branch_name">Facebook </label>
                  <input type="text" name="facebook" id="facebook" class="form-control" value="{{ $single->facebook }}">
                </div>

                <div class="form-group">
                  <label for="regional_branch_name">Twitter </label>
                  <input type="text" name="twitter" id="twitter" value="{{ $single->twitter }}" class="form-control" >
                </div>

                <div class="form-group">
                  <label for="regional_branch_name">Instagram </label>
                  <input type="text" name="instragam" id="instragam" class="form-control" value="{{ $single->instragam }}">
                </div>

                <div class="form-group">
                  <label for="regional_branch_name">Linkedin </label>
                  <input type="text" name="linkedin" id="linkedin" value="{{ $single->linkedin }}" class="form-control" >
                </div>

              </div>
              
              <div class="col-md-6">

                <div class="form-group">
                  <label for="regional_branch_name">Youtube </label>
                  <input type="text" name="youtube" id="youtube" value="{{ $single->youtube }}" class="form-control" >
                </div>

                <div class="form-group">
                    <label for="address">Update Logo</label>
                    <input type="file" id="userfile" class="form-control" name="logo"  value="" onchange="getPreview('userfile','img_preview','none');">
                    <img src="<?= (empty($single->logo))? asset('public/backend/img/unknown.png') : asset($single->logo) ?>" style="width:100px; margin-top:5px" id="img_preview" class="img-responsive img-thumbnail"/><br>
                    <code>(Max photo size: 250x60, 512kb)</code><br>
                    <span class="text-danger">{{ $errors->has('logo') ? $errors->first('logo') : '' }}</span>
                </div>

                <div class="form-group">
                    <label for=""></label>
                    <a href="" class="btn btn-info btn-sm btn-warning">Cancel</a>
                    <input type="submit" name="submit" class="btn btn-primary btn-sm pull-right" value="Update">
                </div>
                
              </div>
            </div>
          </form>         
          </div><!-- /.box-body -->
        </div><!-- /.box -->
      @endisset
    </section><!-- /.content -->
  </div><!-- /.content-wrapper -->

@endsection