@extends('admin.layout.default')
@section('title')
  Add Video Gallery
@endsection
@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <!-- Main content -->
    <section class="content">
      @isset($add)
        <div class="box box-warning">
          <div class="box-header with-border">
            <h3 class="box-title">Create Video Gallery</h3>
            <div class="box-tools pull-right">
            </div>
          </div>
          <div class="box-body">
          <form action="{{ route('videos.store') }}" method="post" enctype="multipart/form-data" autocomplete="off">
          @csrf
            <div class="row">
              <div class="col-md-6">

                <div class="form-group">
                    <label for="Section">Category <span class="text-red">*</span></label>
                    <select class="form-control" name="category" required>
                      <option value="video">Video</option>
                      <option value="tvc_ovc">TVC/OVC</option>
                    </select>
                </div>

                <div class="form-group">
                    <label for="regional_branch_name">iframe <span class="text-red">*</span></label>
                    <input type="text" class="form-control" id="iframe" name="iframe" placeholder="Enter iframe" value="" required>
                    <span class="text-danger">{{ $errors->has('iframe') ? $errors->first('iframe') : '' }}</span>
                </div>

                <div class="form-group">
                    <label for=""></label>
                    <a href="" class="btn btn-info btn-sm btn-warning">Cancel</a>
                    <input type="submit" name="submit" class="btn btn-primary btn-sm pull-right" value="Save">
                </div>
              
              </div>
            </div>
          </form>         
          </div><!-- /.box-body -->
        </div><!-- /.box -->
      @endisset
      @isset($edit)
        <div class="box box-warning">
          <div class="box-header with-border">
            <h3 class="box-title">Update Video Gallery</h3>
            <div class="box-tools pull-right">
            </div>
          </div>
          <div class="box-body">
          <form action="{{ route('videos.update',$single->id) }}" method="post" enctype="multipart/form-data" autocomplete="off">
          @csrf
            <div class="row">
              <div class="col-md-6">

                <div class="form-group">
                    <label for="Section">Category <span class="text-red">*</span></label>
                    <select class="form-control" name="category" required>
                      <option value="video" <?php if($single->category=='video'){ echo 'selected'; } ?> > Video </option>
                      <option value="tvc_ovc" <?php if($single->category=='tvc_ovc'){ echo 'selected'; } ?> >TVC/OVC</option>
                    </select>
                </div>

                <div class="form-group">
                    <label for="regional_branch_name">iframe <span class="text-red">*</span></label>
                    <input type="text" class="form-control" id="iframe" name="iframe" placeholder="Enter iframe" value="{{ $single->iframe }}" required>
                    <span class="text-danger">{{ $errors->has('iframe') ? $errors->first('iframe') : '' }}</span>
                </div>

                <div class="form-group">
                    <label for=""></label>
                    <a href="" class="btn btn-info btn-sm btn-warning">Cancel</a>
                    <input type="submit" name="submit" class="btn btn-primary btn-sm pull-right" value="Update">
                </div>

              </div>
            </div>
          </form>         
          </div><!-- /.box-body -->
        </div><!-- /.box -->
      @endisset
    </section><!-- /.content -->
  </div><!-- /.content-wrapper -->

@endsection