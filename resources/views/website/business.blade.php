@extends('website.layout.default')
@section('title')
    JMI Business
@endsection
@section('content')

<section class="services-area">
    <div class="container">
        <h4>Explore Our Caring & Premium</h4>
        <h1>JMI Business</h1>
        <div class="services-box">
             @foreach($businesses as $value)
                <div class="per-box">
                    <img src="{{ asset($value->file) }}" alt="img">
                    <p class="text">{{ $value->title }}</p>
                    <div class="overlay">
                        <div class="text">{{ $value->title }}</div>
                        <a href="{{ $value->link }}" class="btn btn-detail">Details</a>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</section>

@endsection