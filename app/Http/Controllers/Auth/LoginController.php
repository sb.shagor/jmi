<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Session;
use Member;
use DB;

class LoginController extends Controller
{
    public function login(){
        if(Session::get('loggedData')){
            return redirect('/dashboard');
        }else{    
        	$this->data['headline'] = 'LOG IN';
        	return view('admin.login.login', $this->data);
        }
    }

    public function authenticate(Request $request){

    	$data =  $request->only(['email', 'password']);

        // echo '<pre>';
        // print_r($data);
        // exit();

    	if(Auth::attempt([ 'email'=>$data['email'], 'password'=>$data['password'], 'status'=>1 ])){
            
            Session::put('loggedData',Auth::user());
    		return redirect()->intended('/dashboard');

    	}else{

            return redirect()->intended('admin')->withErrors(['Invaild User email or password']);
        }
    	
    }//authenticate

    public function resetPassword(Request $request){

        $data['email'] = $request->email;
        if (existed('users', 'email', $data['email'])) {
            Mail::send('emails.reset_password', $data, function ($message) use ($data) {
                //$message->from($data['fromEmail'], $data['name']);
                $message->to($data['email'])->subject('Password Reset Link');
            });

            echo '<script type="text/javascript">alert("Password reset link send to your mail. Please check your email !")</script>';
        } else {
            echo '<script type="text/javascript">alert("Your email is invalid !")</script>';
        }

        $data['headline'] = 'LOG IN';
        return view('admin.login.login', $data);
    }

    public function reset_confirm($email) {

        $result = DB::table('users')->where('email', $email)->first();
        if (isset($result)) {
            $password_generate = rand(111111,999999); 
            $password = Hash::make($password_generate);
            $query = DB::table('users')->where('email', $email)
            ->update([
                'password' => $password
            ]);
        }

        $data['email'] = $email;
        $data['password'] = $password_generate;

        if ($query == TRUE) {

            Mail::send('emails.password_changed', $data, function ($message) use ($data) {
                $message->to($data['email'])->subject('Password succesfully reset');
            });

            echo '<script type="text/javascript">alert("Password successfully reset. Please check your email !")</script>';
        } else {
            echo '<script type="text/javascript">alert("Password reset fail!")</script>';
        }
        $data['headline'] = 'LOG IN';
        return view('admin.login.login', $data);
    }

    // Backup database
    public function backup_db() {
        $this->load->dbutil(); // Load the DB utility class
        $backup = $this->dbutil->backup(); // Backup your entire database and assign it to a variable
        write_file(FCPATH . '/db/BackupDb.sql', $backup); // write the file to your server
        //force_download('BackupDb_'.date('YmdHi').'.sql', $backup); // backup the file to your desktop
        // Backup send to mail 
        $this->email->from('info@wanitltd.com', 'WAN IT Ltd.');
        $this->email->to('shagor@wanitbd.com');
        $this->email->subject('Manual DB Backup of ERP');
        $this->email->attach(FCPATH . "/db/BackupDb.sql");
        $this->email->message("Dear Admin, <br /> Please save the attached database file.");
        $this->email->send();
        $this->session->set_tempdata("msg", "<span class='success'> Database backup has been send to your email. Please check your email. </span>", 5);
        redirect('dashboard');
    }


    public function logout(){
        Auth::logout();
        Session::forget('loggedData');
        return redirect('/');
    }


}//LoginController