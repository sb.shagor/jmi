@extends('admin.layout.default')
@section('title')
  Manage Slider
@endsection
@section('content')

  <div class="content-wrapper">
    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Manage Slider</h3>
          <div class="box-tools pull-right"></div>
        </div>
        <div class="box-body color-black">
              <table id="members_list_table" class="table table-bordered table-striped">
                <thead>
                  <tr>
                      <th>SN</th>
                      <th>Title</th>
                      <th>Description</th>
                      <th>Image</th>
                      <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php if(!empty($slider)){ 
                  	foreach ($slider as $key => $value) { 
                  ?>
                    <tr>
                      	<td>{{ ++$key }}</td>
                        <td>{{ $value->title }}</td>
                        <td>{{ $value->description }}</td>
                        <td>
                          <?php if($value->image == ""){?>
                            <img height="50" width="60" src="{{ asset('public') }}/backend/img/unknown.png" title="<?php echo $value->image;?>" />
                          <?php }else { ?>
                            <a href="{{ asset($value->image) }}" target="_blank">
                              <img height="50" width="60" src="{{ asset($value->image) }}"
                               title="<?php echo $value->title;?>" />
                            </a>
                          <?php } ?>
                        </td>
                    	<td>
                          <a href="{{ url('edit-slide/'.$value->id)}}">Edit&nbsp;<i class="fa fa-edit"></i></a> | 
                          <a href="{{ url('delete-slide/'.$value->id) }}" style="color: red;" title="Delete" onclick="return confirm('Are you sure to delete this ?')" >Delete&nbsp;<i class="fa fa-trash-o fa-lg" style="color: red;"></i></a>
                        </td>
                    </tr>
                  <?php } } ?>
                </tbody>
              </table>        
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </section><!-- /.content -->
  </div><!-- /.content-wrapper -->

@endsection