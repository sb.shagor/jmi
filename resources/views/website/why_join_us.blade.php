@extends('website.layout.default')
@section('title')
    JMI | Why Join Us
@endsection
@section('content')

<section class="advanced-area">
    <div class="container">
        <h1>Why Join Us</h1>
        <div class="row">
            <div class="col-md-12 job-context">
                {!! $single->description !!}
            </div>
        </div>
    </div>
</section>

@endsection