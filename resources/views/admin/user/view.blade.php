@extends('admin.layout.default')
@section('title')
  Manage User
@endsection
@section('content')

  <div class="content-wrapper">
    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Manage User</h3>
          <div class="box-tools pull-right"></div>
        </div>
        <div class="box-body color-black">
              <table id="members_list_table" class="table table-bordered table-striped">
                <thead>
                  <tr>
                      <th>SN</th>
                      <th>Name</th>
                      <th>Role</th>
                      <th>User Email</th>
                      <th>Phone</th>
                      <th>Image</th>
                      <th>Status</th>
                      <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php if(!empty($allUsers)){ 
                  	foreach ($allUsers as $key => $user) { 
                  ?>
                    <tr>
                      	<td>{{ ++$key }}</td>
                        <td>{{ $user->name }}</td>
                        <td>{{ ($user->role_id==1)?'Super Admin':'HR' }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->phone }}</td>
                        <td>
                          <?php if($user->image == ""){?>
                            <img height="50" width="60" src="{{ asset('public') }}/backend/img/unknown.png" />
                          <?php }else { ?>
                            <a href="{{ asset($user->image) }}" target="_blank">
                              <img height="50" width="60" src="{{ asset($user->image) }}" />
                            </a>
                          <?php } ?>
                        </td>
                        <td>{{ $user->status==1 ? 'Active' : 'Inactive' }}</td>
                      	<td>
                          <?php
                            if($user->status==1){ ?>
                                <a onclick="return confirm('Are You Sure?')" href="{{ url('doUserInactive/'.$user->id) }}" >Inactive <i class="fa fa-times-circle"></i></a> | 

                            <?php }else{ ?>

                                <a onclick="return confirm('Are You Sure?')" href="{{ url('doUserActive/'.$user->id) }}" >Active <i class="fa fa-check-circle"></i></a> | 

                            <?php } ?>

                            <?php
                              if($user->role_id==1){ ?>
                                  <a onclick="return confirm('Are You Sure?')" href="{{ url('SAtoHR/'.$user->id) }}" style="color: green;">Change Role</a> | 

                              <?php }else{ ?>

                                  <a onclick="return confirm('Are You Sure?')" href="{{ url('HRtoSA/'.$user->id) }}" style="color: green;">Change Role</a> | 

                              <?php } ?>

                            {{-- <a href="{{ url('editProfile/'.$user->id)}}"><i class="fa fa-edit"></i></a> |  --}}
                            <a href="{{ url('delete-profile/'.$user->id)}}" style="color: red;" title="Delete" onclick="return confirm('Are you sure to delete this ?')" >Delete <i class="fa fa-trash-o fa-lg" style="color: red;"></i></a>
                        </td>
                    </tr>
                  <?php } } ?>
                </tbody>
              </table>        
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </section><!-- /.content -->
  </div><!-- /.content-wrapper -->

@endsection