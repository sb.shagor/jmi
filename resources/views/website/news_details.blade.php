@extends('website.layout.default')
@section('title')
    JMI | News and Event Details
@endsection
@section('content')

    <section class="advanced-area">
        <div class="container">
        	<div class="row">
        	    <aside class="col-md-4">
                    <div class="card" style="padding: 20px 0px;">
                        <div class="card-body notice scroller">
                    		<h4><i class="icofont-newspaper"></i> Notice Board</h4>
                            <marquee width="100%" direction="up" height="600px">
                                <div class="row">
                            		<div class="col-md-12 offset-md-3">
                            			<ul class="timeline">
                            				@foreach($notices as $value)
                                                <li>
                                                    <div class="row ntc">
                                                        <div class="col-md-4 ntc-img">
                                                            <img src="{{ asset($value->file) }}">
                                                        </div>
                                                        <div class="col-md-8">
                                                            <a target="_blank" href="#">{{ $value->title }}</a>
                                                            <a href="#" class="float-right">{{ $value->created_at }}</a>
                                                            <p>{{ limit_words(strip_tags($value->description),10,"UTF-8") }}</p>
                                                            <a href="#entry{{$value->id}}" role="button" class="btn btn-warning btn-xs" data-toggle="modal">Read More</a>
                                                            <div id="entry{{$value->id}}" class="modal fade">
                                                              <div class="modal-dialog">
                                                                <div class="modal-content">
                                                                  <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
                                                                    <h4 id="myModalLabel1" style="color: #000 !important;">{{ $value->title }}</h4>
                                                                  </div>
                                                                  <div class="modal-body">
                                                                    {!! $value->description !!}
                                                                  </div>
                                                                </div>
                                                              </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <hr>
                                            @endforeach
                            			</ul>
                            		</div>
                            	</div>
                            </marquee>
                        </div>
                    </div>
        	    </aside>

        	   <main class="col-md-8">
                    <header class="border-bottom mb-4 pb-3">
                        <div class="row" style="margin-bottom: 20px;">
                            <div class="col-md-12" style="text-align: left;">
                                <h4 style="color: #146d85; margin-top: 30px"><i class="icofont-flag"></i> {{ $single->title }}</h4>
                            </div>
                        </div>

                        <div class="nws-con">
                            <img src="{{ asset($single->file) }}" alt="#" style="width: 100%;">
                            {!! $single->description !!}
                            <div class="row nws-dt">
                                <div class="col-md-12">
                                    <div class="dt">
                                    <i class="icofont-calendar"></i> Date : {{ $single->created_at }}
                                    </div>
                                </div>
                            </div>
                        </div>

            			<div class="btn-group">
            				<a href="#" class="btn btn-outline-secondary" data-toggle="tooltip" title="" data-original-title="List view"> 
            					<i class="fa fa-bars"></i></a>
            				<a href="#" class="btn  btn-outline-secondary active" data-toggle="tooltip" title="" data-original-title="Grid view"> 
            					<i class="fa fa-th"></i></a>
            			</div>
                    </header><!-- sect-heading -->
        	   </main>
        	</div>
        </div>
    </section>

@endsection