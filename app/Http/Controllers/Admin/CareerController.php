<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use DB;
use Auth;
use App\Career;
use Illuminate\Http\Request;

class CareerController extends Controller
{
    public function index(){
        $data['get_all'] = Career::get();
        return view('admin.career.view', $data);
    }

    public function application(){
        $data['get_all'] = DB::table('tbl_application')->get();
        return view('admin.career.view_application', $data);
    }

    public function create(){
        $data['add'] = TRUE;
        return view('admin.career.add', $data);
    }

    public function store(Request $request){

        $this->validate($request, [          
            'title' => 'required',               
            'job_nature' => 'required',               
            'job_location' => 'required',               
            'salary' => 'required',               
            'vacancies' => 'required',               
            'deadline' => 'required',               
            'description' => 'required',               
            'file' => 'required'                     
        ]);

        try {
            $career = new Career;
            $career->title = $request->title;
            $career->job_nature = $request->job_nature;
            $career->job_location = $request->job_location;
            $career->salary = $request->salary;
            $career->vacancies = $request->vacancies;
            $career->deadline = $request->deadline;
            $career->description = $request->description;
            $career->created_by = Auth::id();
            if(!empty($request->file('file'))){
                $File = $request->file('file');
                $name = $File->getClientOriginalName();
                $ext = explode('.',$name);
                $finalName = 'career-'.time().'.'.$ext[1];
                $uploadPath = 'public/career/';
                $File->move($uploadPath, $finalName);
                $career->file = $uploadPath.$finalName;
            }
            $career->save();
             
            setMessage("message","success","Successful");
            return redirect(route('careers.index'));

        }catch (\Exception $e) {
            $err_message = \Lang::get($e->getMessage());
            setMessage("message","danger","Failed");
            return redirect(route('careers.index'));
            //return back();
        }
    }

    public function edit($id){
        $data['edit'] = TRUE;
        $data['single'] = Career::findOrFail($id);
        return view('admin.career.add', $data);
    }

    public function join_us(){
        $data['single'] = DB::table('tbl_join_us')->first();
        return view('admin.career.join_us', $data);
    }

    public function update_join_us(Request $request, $id){

        $this->validate($request, [                         
            'description' => 'required',                    
        ]);

        try {

           DB::table('tbl_join_us')->where('id', $id)->update(['description' => $request->description]);
            setMessage("message","success","Successful");
            return redirect(route('careers.join_us'));

        }catch (\Exception $e) {
            $err_message = \Lang::get($e->getMessage());
            setMessage("message","danger","Failed");
            return back();
        }
    }

    public function update(Request $request, $id){

        $this->validate($request, [          
            'title' => 'required',               
            'job_nature' => 'required',               
            'job_location' => 'required',               
            'salary' => 'required',               
            'vacancies' => 'required',               
            'deadline' => 'required',               
            'description' => 'required',                    
        ]);

        try {
            $career = Career::findOrFail($id);
            $career->title = $request->title;
            $career->job_nature = $request->job_nature;
            $career->job_location = $request->job_location;
            $career->salary = $request->salary;
            $career->vacancies = $request->vacancies;
            $career->deadline = $request->deadline;
            $career->description = $request->description;
            $career->updated_by = Auth::id();
            if ($request->file != null) {

                $path = $career->file;
                if (file_exists($path) and $career->file !=null) {
                    unlink($path);
                }
                $File = $request->file('file');
                $name = $File->getClientOriginalName();
                $ext = explode('.',$name);
                $finalName = 'career-'.time().'.'.$ext[1];
                $uploadPath = 'public/career/';
                $File->move($uploadPath, $finalName);
                $career->file = $uploadPath.$finalName;
            }
            $career->save();

            setMessage("message","success","Successful");
            return redirect(route('careers.index'));

        }catch (\Exception $e) {
            $err_message = \Lang::get($e->getMessage());
            setMessage("message","danger","Failed");
            return back();
        }
    }

    public function destroy($id){

        try {
            $career = Career::findOrFail($id);
            $path = $career->file;
            if (file_exists($path) and $career->file != null) {
                unlink($path);
            }
            $career->delete();
            setMessage("message","success","Successful");
            return back();
        }catch (\Exception $e) {
            $err_message = \Lang::get($e->getMessage());
            setMessage("message","danger","Failed");
            return back();
        }
    }

}//CareerController