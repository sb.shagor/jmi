@extends('admin.layout.default')
@section('title')
  Manage Video
@endsection
@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Manage Video</h3>
          <div class="box-tools pull-right"></div>
        </div>
        <div class="box-body color-black">
              <table id="members_list_table" class="table table-bordered table-striped">
                <thead>
                  <tr>
                     <th>SN</th>
					           <th>Category</th>
                     <th>Iframe</th>
					           <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php if(!empty($get_all)){ 
                  	foreach ($get_all as $key => $value) { 
                  ?>
                    <tr>
                      	<td>{{ ++$key }}</td>
                      	<td>{{ $value->category }}</td>
                        <td>{!! $value->iframe !!}</td>
                    	<td>
                          <a href="{{ route('videos.edit',$value->id) }}">Edit&nbsp;<i class="fa fa-edit"></i></a> | 
                          <a href="{{ route('videos.destroy',$value->id) }}" style="color: red;" title="Delete" onclick="return confirm('Are you sure to delete this ?')" >Delete&nbsp;<i class="fa fa-trash-o fa-lg" style="color: red;"></i></a>
                        </td>
                    </tr>
                  <?php } } ?>
                </tbody>
              </table>        
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </section><!-- /.content -->
  </div><!-- /.content-wrapper -->

  <style type="text/css">
    iframe{
      width: 100% !important;
      height: 250px !important;
    }
  </style>

@endsection