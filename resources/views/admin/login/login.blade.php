<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>JMI | Login</title>
    <!-- Bootstrap -->
    <link href="{{ asset('public/admin/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('public/admin/css/style.css') }}" rel="stylesheet">
  </head>
  <body>
    <div class="container-fluid">
        <div class="row">
          <div class="banner">
            <div class="banner_overlay hidden-xs"><span>JMI</span></div>
            <div class="banner_overlay visible-xs"><span>JMI</span></div>
          </div>
        </div>
        
        <div class="row login_form_area">
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-10 col-lg-offset-3 col-md-offset-3 col-sm-offset-3 col-xs-offset-1">
              <div class="row login_bg">
                @if(Session::has('message'))
                    <div class="alert alert-block alert-danger">
                        <button type="button" class="close" data-dismiss="alert">
                            <i class="ace-icon fa fa-times"></i>
                        </button>
                        <i class="ace-icon fa fa-check green"></i>
                        {{ Session::get("message") }}
                        {{ Session::forget('message') }}
                    </div>
                @endif

                <div class="col-lg-6 col-md-6">
                    <img style="position: relative; top: 50px;" src="{{ asset('public/admin/img/login_img.png') }}" class="img-responsive hidden-xs center-block">
                </div>
                
                @if($errors->any())
								  @foreach($errors->all() as $error)
								    <span class="invalid-feedback" role="alert">
								      <strong>{{ $error }}</strong>
								    </span>
								  @endforeach
								@endif

                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="margin-top:35px">
                  <form name="login_panel" id="login_panel" action="{{ url('checkAuthentication') }}" method="post" onsubmit="return(validate());">
										@csrf
                      <div class="form-group">
                        <label for="username" class="col-sm-3 hidden-md control-label">Email</label>
                        <div class="col-sm-9">
                          <input type="email" class="form-control" id="email" name="email" placeholder="Email" value="">
                        </div>
                        @if ($errors->has('email'))
										      <span class="invalid-feedback" role="alert">
										        <strong>{{ $errors->first('email') }}</strong>
										      </span>
										    @endif
                      </div><br><br>
                      <div class="form-group">
                        <label for="password" class="col-sm-3 hidden-md control-label">Password</label>
                        <div class="col-sm-9">
                          <input type="password" class="form-control" id="password" name="password" placeholder="******">
                        </div>
                        <a href="javascript:void(0)" id="password_text"><i class="fa fa-eye"></i></a>
                      </div><br><br>
                      @if ($errors->has('password'))
									      <span class="invalid-feedback" role="alert">
									        <strong>{{ $errors->first('password') }}</strong>
									      </span>
									    @endif
                      <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-8">
                          <button type="submit" class="btn btn-default" name="login">Sign in</button>
                        </div>
                      </div>
                      
                    </form>
                </div>                
              </div>
          </div>
        </div>
    </div>

    <!-- footer -->
    <footer class="footer">
      <div class="container-fluid">
        <span class="text-muted">
          <strong>Copyright &copy; <?= date('Y'); ?> <a href="javascript:">JMI</a>.</strong> All rights reserved.</a>
        </span>
        <span class="pull-right"><b>Powered by</b> <a href="http://wanitbd.com/">WAN IT</a></span>
      </div>
    </footer>

  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="{{ asset('public/admin/js/bootstrap.min.js') }}"></script>
  <script type="text/javascript">
      $(document).ready(function(){
            
            $("#password_text").on("click",function(){
                var x = document.getElementById("password");
                if (x.type === "password") {
                    x.type = "text";
                } else {
                    x.type = "password";
                }
            });
        });
		</script>
  </body>
</html>