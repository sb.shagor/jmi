@extends('website.layout.default')
@section('title')
    JMI | Contact
@endsection
@section('content')

<style type="text/css">
    .grecaptcha-badge { visibility: hidden !important; }

</style>

<section class="advanced-area">
    <div class="container">
    <h4>have any query?</h4>
        <h1>Contact us</h1>
        @if(Session::has('message'))
            <div class="alert alert-block alert-{{Session::get("class")}}">
                <button type="button" class="close" data-dismiss="alert">
                    <i class="ace-icon fa fa-times"></i>
                </button>
                <i class="ace-icon fa fa-check green"></i>
                {{ Session::get("message") }}
            </div>
        @endif
        <div class="row">
            <div class="col-md-6">
            {!! $contact->map !!}
            </div>
            <div class="col-md-6">
                <div class="contact-form">
                    <form method="post" action="{{ route('sendContactMail') }}">
                    @csrf
                       <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="text" name="name" class="form-control" placeholder="Your Name *" value="" />
                                </div>
                                <div class="form-group">
                                    <input type="text" name="fromEmail" class="form-control" placeholder="Your Email *" value="" />
                                </div>
                                <div class="form-group">
                                    <input type="text" name="phone" class="form-control" placeholder="Your Phone Number *" value="" />
                                </div>
                                <div class="form-group">
                                    <textarea name="bodymessage" class="form-control" placeholder="Your Message *" style="width: 100%; height: 150px;"></textarea>
                                </div>

                                {!! RecaptchaV3::initJs() !!}

                                {!! RecaptchaV3::field('register') !!}

                                <div class="form-group">
                                    <input type="submit" name="btnSubmit" class="btnContactSubmit" value="Send Message" />
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection