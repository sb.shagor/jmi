<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use DB;
use Auth;
use App\WelcomeMessage;
use Illuminate\Http\Request;

class WelcomeMessageController extends Controller
{
    public function index(){

        $data['get_all'] = WelcomeMessage::get();
        return view('admin.welcome_message.view', $data);
    }

    public function edit($id){
        $data['edit'] = TRUE;
        $data['single'] = WelcomeMessage::findOrFail($id);
        return view('admin.welcome_message.edit', $data);
    }

    public function update(Request $request, $id){

        $this->validate($request, [                      
            'person_id' => 'required',                     
            'description' => 'required',                     
        ]);

        try {
            $welcome = WelcomeMessage::findOrFail($id);
            $welcome->person_id = $request->person_id;
            $welcome->description = $request->description;
            if ($request->file != null) {

                $path = $welcome->file;
                if (file_exists($path) and $welcome->file != null) {
                    unlink($path);
                }
                $File = $request->file('file');
                $name = $File->getClientOriginalName();
                $ext = explode('.',$name);
                $finalName = 'message-'.time().'.'.$ext[1];
                $uploadPath = 'public/uploads/';
                $File->move($uploadPath, $finalName);
                $welcome->file = $uploadPath.$finalName;
            }
            $welcome->save();

            setMessage("message","success",updated_success());
            return redirect(route('welcome.index'));

        }catch (\Exception $e) {
            $err_message = \Lang::get($e->getMessage());
            setMessage("message","danger",exception());
            return back();
        }
    }

}