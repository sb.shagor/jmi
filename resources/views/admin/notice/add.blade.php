@extends('admin.layout.default')
@section('title')
  Add Notice
@endsection
@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <!-- Main content -->
    <section class="content">
      @isset($add)
        <div class="box box-warning">
          <div class="box-header with-border">
            <h3 class="box-title">Create Notice</h3>
            <div class="box-tools pull-right">
            </div>
          </div>
          <div class="box-body">
          <form action="{{ route('notices.store') }}" method="post" enctype="multipart/form-data" autocomplete="off">
          @csrf
            <div class="row">
              <div class="col-md-10">

                <div class="form-group">
                    <label for="regional_branch_name">Section <span class="text-red">*</span></label>
                    <select class="form-control" name="section_id" required>
                      <option value="1">Notice</option>
                      <option value="2">News</option>
                      <option value="3">Event</option>
                    </select>
                    <span class="text-danger">{{ $errors->has('section_id') ? $errors->first('section_id') : '' }}</span>
                </div>

                <div class="form-group">
                    <label for="regional_branch_name">Title <span class="text-red">*</span></label>
                    <input type="text" class="form-control" id="title" name="title" placeholder="Enter Title" value="" required>
                    <span class="text-danger">{{ $errors->has('title') ? $errors->first('title') : '' }}</span>
                </div>

                <div class="form-group">
                  <label for="regional_branch_name">Description <span class="text-red">*</span></label>
                  <textarea name="description" id="description" class="col-xs-12 col-sm-4" required></textarea>
                </div>

                <div class="form-group">
                    <label for="address">Upload File <span class="text-red">*</span></label>
                    <input type="file" class="form-control" name="file"  value="" required>
                    <code>Ration 277x215 and Max File Size less than 1MB</code>
                    <span class="text-danger">{{ $errors->has('file') ? $errors->first('file') : '' }}</span>
                </div>

                <div class="form-group">
                    <label for=""></label>
                    <a href="" class="btn btn-info btn-sm btn-warning">Cancel</a>
                    <input type="submit" name="submit" class="btn btn-primary btn-sm pull-right" value="Save">
                </div>
              
              </div>
            </div>
          </form>         
          </div><!-- /.box-body -->
        </div><!-- /.box -->
      @endisset
      @isset($edit)
        <div class="box box-warning">
          <div class="box-header with-border">
            <h3 class="box-title">Update Notice</h3>
            <div class="box-tools pull-right">
            </div>
          </div>
          <div class="box-body">
          <form action="{{ route('notices.update',$single->id) }}" name="notice" method="post" enctype="multipart/form-data" autocomplete="off">
          @csrf
            <div class="row">
              <div class="col-md-10">

                <div class="form-group">
                    <label for="regional_branch_name">Section <span class="text-red">*</span></label>
                    <select class="form-control" name="section_id" required>
                      <option value="1">Notice</option>
                      <option value="2">News</option>
                      <option value="3">Event</option>
                    </select>
                    <span class="text-danger">{{ $errors->has('section_id') ? $errors->first('section_id') : '' }}</span>
                </div>

                <div class="form-group">
                    <label for="regional_branch_name">Title <span class="text-red">*</span></label>
                    <input type="text" class="form-control" id="title" name="title" placeholder="Enter Title" value="{{ $single->title }}" required>
                    <span class="text-danger">{{ $errors->has('title') ? $errors->first('title') : '' }}</span>
                </div>

                <div class="form-group">
                  <label for="regional_branch_name">Description <span class="text-red">*</span></label>
                  <textarea name="description" id="description" class="col-xs-12 col-sm-4" required>{{ $single->description }}</textarea>
                </div>

                <div class="form-group">
                    <label for="address">Upload File</label>
                    <input type="file" class="form-control" name="file"  value="" >
                    <code>Ration 277x215 and Max File Size less than 1MB</code>
                    <span class="text-danger">{{ $errors->has('file') ? $errors->first('file') : '' }}</span>
                </div>

                <div class="form-group">
                    <label for=""></label>
                    <a href="" class="btn btn-info btn-sm btn-warning">Cancel</a>
                    <input type="submit" name="submit" class="btn btn-primary btn-sm pull-right" value="Update">
                </div>

              </div>
            </div>
          </form>         
          </div><!-- /.box-body -->
        </div><!-- /.box -->
        <script type="text/javascript">
            document.forms['notice'].elements['section_id'].value='<?php echo $single->section_id ?>';
        </script>
      @endisset
    </section><!-- /.content -->
  </div><!-- /.content-wrapper -->

  <script src="https://cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>
  <script>
      CKEDITOR.replace('description');
  </script>

@endsection