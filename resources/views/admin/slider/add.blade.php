@extends('admin.layout.default')
@section('title')
  Add Slider
@endsection
@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <!-- Main content -->
    <section class="content">
      @isset($add)
        <div class="box box-warning">
          <div class="box-header with-border">
            <h3 class="box-title">Create New Slider</h3>
            <div class="box-tools pull-right">
            </div>
          </div>
          <div class="box-body">
          <form action="{{ url('save-slide') }}" method="post" enctype="multipart/form-data" autocomplete="off">
          @csrf
            <div class="row">
              <div class="col-md-10">

                <div class="form-group">
                    <label for="Name">Title <span class="text-red">*</span></label>
                    <input type="text" class="form-control" id="title" name="title" placeholder="Enter Title" value="" required>
                    <span class="text-danger">{{ $errors->has('title') ? $errors->first('title') : '' }}</span>
                </div>

                <div class="form-group">
                    <label for="description">Description <span class="text-red">*</span></label>
                    <input type="text" class="form-control" id="description" name="description" placeholder="Enter Description" value="" required>
                    <span class="text-danger">{{ $errors->has('description') ? $errors->first('description') : '' }}</span>
                </div>

                <div class="form-group">
                    <label for="address">Upload Slider Image <span class="text-red">*</span></label>
                    <input type="file" id="userfile" class="form-control" name="image" value="" required onchange="getPreview('userfile','img_preview','none');">
                    <img src="<?= asset('public/backend/img/unknown.png'); ?>" style="width:100px; margin-top:5px" id="img_preview" class="img-responsive img-thumbnail"/><br>
                    <code>(Max photo size: 1920x680, 1024kb)</code>
                    <span class="text-danger">{{ $errors->has('image') ? $errors->first('image') : '' }}</span>
                </div>

                <div class="form-group">
                    <label for=""></label>
                    <a href="" class="btn btn-info btn-sm btn-warning">Cancel</a>
                    <input type="submit" name="submit" class="btn btn-primary btn-sm pull-right" value="Save">
                </div>
              
              </div>
            </div>
          </form>         
          </div><!-- /.box-body -->
        </div><!-- /.box -->
      @endisset
      @isset($edit)
        <div class="box box-warning">
          <div class="box-header with-border">
            <h3 class="box-title">Update Slider Info</h3>
            <div class="box-tools pull-right">
            </div>
          </div>
          <div class="box-body">
          <form action="{{ url('update-slide') }}" name="notice" method="post" name="edit" enctype="multipart/form-data" autocomplete="off">
          @csrf
            <div class="row">
              <div class="col-md-10">

                <div class="form-group">
                    <label for="regional_branch_name">Title <span class="text-red">*</span></label>
                    <input type="text" class="form-control" id="title" name="title" placeholder="Enter Title" value="{{ $single->title }}" required>
                    <span class="text-danger">{{ $errors->has('title') ? $errors->first('title') : '' }}</span>
                </div>

                <div class="form-group">
                    <label for="description">Description <span class="text-red">*</span></label>
                    <input type="text" class="form-control" id="description" name="description" placeholder="Enter Description" value="{{ $single->description }}" required>
                    <span class="text-danger">{{ $errors->has('description') ? $errors->first('description') : '' }}</span>
                </div>

                <input type="hidden" name="id" class="col-xs-12 col-sm-4" value="{{ $single->id }}" />

                <div class="form-group">
                    <label for="address">Update Photograph</label>
                    <input type="file" id="userfile" class="form-control" name="image"  value="" onchange="getPreview('userfile','img_preview','none');">
                    <img src="<?= (empty($single->image))? asset('public/backend/img/unknown.png') : asset($single->image) ?>" style="width:100px; margin-top:5px" id="img_preview" class="img-responsive img-thumbnail"/><br>
                    <code>(Max photo size: 1920x680, 1024kb)</code>
                </div>

                <div class="form-group">
                    <label for=""></label>
                    <a href="" class="btn btn-info btn-sm btn-warning">Cancel</a>
                    <input type="submit" name="submit" class="btn btn-primary btn-sm pull-right" value="Update">
                </div>

              </div>
            </div>
          </form>         
          </div><!-- /.box-body -->
        </div><!-- /.box -->

      @endisset
    </section><!-- /.content -->
  </div><!-- /.content-wrapper -->

@endsection