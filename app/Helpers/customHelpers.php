<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Request;


// Check exist data
function existed($tableName = NULL, $fieldName = NULL, $value = NULL) {

    $query = DB::table($tableName)
            ->select($fieldName)
            ->where($fieldName,$value)
            ->first();
    if ($query)
        return true;
    else
        return false;
}

if (!function_exists('logged_in_user_id')) {

    function logged_in_user_id(){

        $logged_in_id = '';
        $logged_in_id = Auth::user()->id;
        return $logged_in_id;
    }
}

if (!function_exists('logged_in_user_name')) {

    function logged_in_user_name(){

        $logged_in_name = '';
        $logged_in_name = Auth::user()->name;
        return $logged_in_name;
    }
}

if (!function_exists('logged_in_role_id')) {

    function logged_in_role_id(){

        $logged_in_role_id = 0;
        if (Auth::user()->role_id) :
            $logged_in_role_id = Auth::user()->role_id;
        endif;
        return $logged_in_role_id;
    }
}

// Create a slug
function slug($str=NULL)
{
    $url = url_title($str,'dash',TRUE);
    if($url=='')
        return str_replace(' ', '-' , $str);
    else 
        return $url;
}

// Compare date month year  
$timezone = "Asia/Dhaka";
if(function_exists('date_default_timezone_set')) {

    date_default_timezone_set($timezone);

    // Difference between day
    function diff_day($sDay, $eDay)
    {
        $sDay = new DateTime(date($sDay));
        $eDay = new DateTime(date($eDay));
        return  $eDay->diff($sDay)->d;
        // Call this function diff_day($v1, $v2);
    }
    
    // Remaining Days
    function remaining_days($date=NULL)
    {
        if(date('Y-m-d')<$date) {
            $date = new DateTime($date);
            $now = new DateTime();
            $diff = $date->diff($now);
            return '<span class="green">Remaining Days: '.$diff->days.'</span>';
        }
        else {
            return '<span class="red"> Date Expired </span>';
        }
        // Cal this fuction as remaining_days('Y-m-d');
    }
}

if (!function_exists('setMessage')) {

    function setMessage($key, $class, $message){

        session()->flash($key, $message);
        session()->flash("class", $class);
        // session()->flash($key,'<div class="alert alert-'.$class.'">'.$message.'</div>');
        return true;
    }
}

if (!function_exists('debug_r')) {

    function debug_r($value){

        echo "<pre>";
        print_r($value);
        echo "</pre>";
        die();
    }
}

if (!function_exists('debug_v')) {

    function debug_v($value){

        echo "<pre>";
        var_dump($value);
        echo "</pre>";
        die();
    }
}

function limit_words($string, $wordsreturned) {
    
    $string = strip_tags($string); // Remove html tag
    $retval = $string; //   Just in case of a problem
    $array = explode(" ", $string);
    /*  Already short enough, return the whole thing */
    if (count($array) <= $wordsreturned) {
        $retval = $string;
    }
    /*  Need to chop of some words */ 
    else {
        array_splice($array, $wordsreturned);
        $retval = implode(" ", $array) . " ......";
    }
    return $retval;
    //echo read_more('word' , 'number');
}

if (!function_exists('title_name')) {

    function title_name($value){
        switch($value){
            case 1:
                return "Academic Staff at Primary Section";
            case 2:
                return "Early Childhood";
            case 3:
                return "Foundation";
            case 4:
                return "Pre Primary";
            case 5:
                return "Year 1";
            case 6:
                return "Year 2";
            case 7:
                return "Year 3";
            case 8:
                return "Year 4";
            case 9:
                return "Year 5";
            case 10:
                return "Specialist Teachers";
            case 11:
                return "Academic Staff at Middle and Senior Section";

        }
    }

}

if (!function_exists('staff_title_name')) {

    function staff_title_name($value){
        switch($value){
            case 1:
                return "Head of School";
            case 2:
                return "Primary Section";
            case 3:
                return "Middle Section";
            case 4:
                return "Senior Section";
            case 5:
                return "Office Inquiry";
            case 6:
                return "Finance Department";

        }
    }

}

if (!function_exists('age_title')) {

    function age_title($value){
        switch($value){
            case 1:
                return "Early Childhood to Year 5 / PYP";
            case 2:
                return "Year 6 to Year 10 / MYP";
            case 3:
                return "Year 11 to Year 12 / IBDP / WACE";

        }
    }

}