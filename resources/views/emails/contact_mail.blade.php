<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<div>
			<br/><strong>General Inquery</strong><br/>
			<p>Name:</p><strong>{{ $name }}</strong><br/>
			<p>Email:</p><strong>{{ $fromEmail }}</strong><br/>
			<p>Phone Number:</p><strong>{{ $phone }}</strong><br/>
			<p>Message:</p><strong>{!! $bodymessage !!}</strong><br/>
		</div>
	</body>
</html>