<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Session;
use Image;
use DB;

class GalleryController extends Controller
{
    public function index(){

    	$data['gallery'] = DB::table('tbl_gallery')
            ->join('tbl_album','tbl_album.id','=','tbl_gallery.album_id', 'LEFT')
            ->select('tbl_gallery.*','tbl_album.title as album_name')
            ->get();
    	return view('admin.gallery.view',$data);
    }

    public function add(){
        $data['add'] = TRUE;
        $data['album_list'] = DB::table('tbl_album')->get();
        return view('admin.gallery.add',$data);
    }

    public function store(Request $request){

        $this->validate($request,[
            'section_id' => 'required',
            'photo' => 'image|required|mimes:jpeg,png,jpg,gif,svg',
        ]);

        $imageUrl='';
        if(!empty($request->file('photo'))){

            $Image = $request->file('photo');
            $name = $Image->getClientOriginalName();
            $ext = explode('.',$name);
            $finalName = 'gallery-'.time().'.'.$ext[1];
            $uploadPath = 'public/galleryImage/';
            $imageUrl = $uploadPath.$finalName;
            Image::make(file_get_contents($Image))->resize(600, 400)->save($imageUrl);

        }

        $result=DB::table('tbl_gallery')
                ->insert([
                    'section_id'=>$request->section_id,
                    'album_id'=>$request->album_id,
                    'title'=>$request->title,
                    'photo'=>($imageUrl) ? $imageUrl : '',
                ]);

        if($result){

            setMessage('message','success',saved_success());
            return redirect('add-gallery');
        }else{

            setMessage('message','danger',exception());
            return redirect('add-gallery');
        }

    }//store

    public function edit($id){
        $data['edit'] = TRUE;
    	$data['single'] = DB::table('tbl_gallery')->find($id);
        $data['album_list'] = DB::table('tbl_album')->get();
    	return view('admin.gallery.add', $data);
    }

    public function update(Request $request){

        $gallery = DB::table('tbl_gallery')->find($request->id);
        $Image = $request->file('photo');
    
        if($Image){

            $preImg = $gallery->photo;
            if($preImg){
                unlink($preImg);
            }
            
            $name = $Image->getClientOriginalName();
            $ext = explode('.',$name);
            $finalName = 'gallery-'.time().'.'.$ext[1];
            $uploadPath = 'public/galleryImage/';
            $imageUrl = $uploadPath.$finalName;
            Image::make(file_get_contents($Image))->resize(600, 400)->save($imageUrl);

            $result = DB::table('tbl_gallery')
                ->where('id',$request->id)
                ->update([
                    'section_id'=>$request->section_id,
                    'album_id'=>$request->album_id,
                    'title'=>$request->title,
                    'photo'=>$imageUrl,
                ]);

            if($result){
                setMessage('message','success',updated_success());
                return redirect('edit-gallery/'.$request->id);
            }else{
                setMessage('message','danger',exception());
                return redirect('edit-gallery/'.$request->id);
            }
    
        }else{

            $result = DB::table('tbl_gallery')
                ->where('id',$request->id)
                ->update([
                    'section_id'=>$request->section_id,
                    'album_id'=>$request->album_id,
                    'title'=>$request->title
                ]);

            if($result){
                setMessage('message','success',updated_success());
                return redirect('edit-gallery/'.$request->id);
            }else{
                setMessage('message','danger',exception());
                return redirect('edit-gallery/'.$request->id);
            }
        }
        
    }//update

    public function delete($id){

        $gallery = DB::table('tbl_gallery')->find($id);
        $preImg = $gallery->photo;
        if($preImg){
            unlink($preImg);
        }
        $result = DB::table('tbl_gallery')->where('id', '=', $id)->delete();
        if($result){
            setMessage('message','success',deleted_success());
            return redirect('manage-gallery');
        }else{
            setMessage('message','danger',exception());
            return redirect('manage-gallery');
        }
    }


}//