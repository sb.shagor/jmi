@extends('website.layout.default')
@section('title')
    JMI | Videos
@endsection
@section('content')

<section class="advanced-area">
    <div class="container">
    <h4>our videos</h4>
        <h1>Video Gallery</h1>
        <div class="row">
            @foreach($videos as $value)
                <div class="col-md-4">
                     {!! $value->iframe !!}
                </div>
            @endforeach
        </div>
    </div>
</section>

<style type="text/css">
    iframe{
        height: 315px !important;
        width: 100% !important;
    }
</style>

@endsection