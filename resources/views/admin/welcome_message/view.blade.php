@extends('admin.layout.default')
@section('title')
  Welcome Message
@endsection
@section('content')

  <div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Welcome Message</h3>
          <div class="box-tools pull-right"></div>
        </div>
        <div class="box-body color-black">
              <table id="members_list_table" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>SN</th>
                    <th>Person</th>
                    <th>Description</th>
                    <th>Photograph</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php if(!empty($get_all)){ 
                    foreach ($get_all as $key => $value) { 
                  ?>
                    <tr>
                      <td>{{ ++$key }}</td>
                      <td>{{ ($value->person_id==1)?'Chairman':'MD' }}</td>
                      <td>{{ limit_words(strip_tags($value->description),55,"UTF-8") }}
                        <a href="#entry{{$value->id}}" role="button" class="btn btn-warning btn-xs" data-toggle="modal">Read More</a>
                        <div id="entry{{$value->id}}" class="modal fade">
                          <div class="modal-dialog">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
                                <h4 id="myModalLabel1">Welcome Message</h4>
                              </div>
                              <div class="modal-body">
                                {!! $value->description !!}
                              </div>
                            </div>
                          </div>
                        </div>
                      </td>
                      <td>
                        <a href="{{ asset($value->file) }}" target="_blank">
                          View
                        </a>
                      </td>
                        <td>
                          <a href="{{ route('welcome.edit',$value->id)}}">Edit&nbsp;<i class="fa fa-edit"></i></a>
                        </td>
                    </tr>
                  <?php } } ?>
                </tbody>
              </table>        
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </section><!-- /.content -->
  </div><!-- /.content-wrapper -->

@endsection