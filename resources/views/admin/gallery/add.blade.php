@extends('admin.layout.default')
@section('title')
  Add Gallery Images
@endsection
@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <!-- Main content -->
    <section class="content">
      @isset($add)
        <div class="box box-warning">
          <div class="box-header with-border">
            <h3 class="box-title">Create Gallery Images</h3>
            <div class="box-tools pull-right">
            </div>
          </div>
          <div class="box-body">
          <form action="{{ url('save-gallery') }}" method="post" enctype="multipart/form-data">
          @csrf
            <div class="row">
              <div class="col-md-6">

                <div class="form-group">
                    <label for="Section">Section <span class="text-red">*</span></label>
                    <select class="form-control" name="section_id" required>
                      <option value="1">Gallery</option>
                      <option value="2">Awards & Winning</option>
                    </select>
                    <span class="text-danger">{{ $errors->has('section_id') ? $errors->first('section_id') : '' }}</span>
                </div>

                <div class="form-group">
                    <label for="address">Upload Photo <span class="text-red">*</span></label>
                    <input type="file" id="userfile" class="form-control" name="photo" value="" required onchange="getPreview('userfile','img_preview','none');">
                    <img src="<?= asset('public/backend/img/unknown.png'); ?>" style="width:100px; margin-top:5px" id="img_preview" class="img-responsive img-thumbnail"/><br>
                    <code>(Max photo size: 400x400, 512kb)</code>
                    <span class="text-danger">{{ $errors->has('photo') ? $errors->first('photo') : '' }}</span>
                </div>

                <div class="form-group">
                    <label for=""></label>
                    <a href="" class="btn btn-info btn-sm btn-warning">Cancel</a>
                    <input type="submit" name="submit" class="btn btn-primary btn-sm pull-right" value="Save">
                </div>
              
              </div>
            </div>
          </form>         
          </div><!-- /.box-body -->
        </div><!-- /.box -->
      @endisset
      @isset($edit)
        <div class="box box-warning">
          <div class="box-header with-border">
            <h3 class="box-title">Update Gallery</h3>
            <div class="box-tools pull-right">
            </div>
          </div>
          <div class="box-body">
          <form action="{{ url('update-gallery') }}" name="edit" method="post" enctype="multipart/form-data">
          @csrf
            <div class="row">
              <div class="col-md-6">

                <div class="form-group">
                    <label for="Section">Section <span class="text-red">*</span></label>
                    <select class="form-control" name="section_id" required>
                      <option value="1">Gallery</option>
                      <option value="2">Awards & Winning</option>
                    </select>
                    <span class="text-danger">{{ $errors->has('section_id') ? $errors->first('section_id') : '' }}</span>
                </div>

                <div class="form-group">
                    <label for="address">Update Photo</label>
                    <input type="file" id="userfile" class="form-control" name="photo" value="" onchange="getPreview('userfile','img_preview','none');">
                    <img src="<?= (empty($single->photo))? asset('public/backend/img/unknown.png') : asset($single->photo) ?>" style="width:100px; margin-top:5px" id="img_preview" class="img-responsive img-thumbnail"/><br>
                    <code>(Max photo size: 400x400, 512kb)</code>
                </div>

                <input type="hidden" name="id" class="col-xs-12 col-sm-4" value="{{ $single->id }}" />

                <div class="form-group">
                    <label for=""></label>
                    <a href="" class="btn btn-info btn-sm btn-warning">Cancel</a>
                    <input type="submit" name="submit" class="btn btn-primary btn-sm pull-right" value="Update">
                </div>

              </div>
            </div>
          </form>         
          </div><!-- /.box-body -->
        </div><!-- /.box -->
        <script type="text/javascript">
            document.forms['edit'].elements['section_id'].value='<?php echo $single->section_id ?>';
        </script>
      @endisset
    </section><!-- /.content -->
  </div><!-- /.content-wrapper -->

@endsection