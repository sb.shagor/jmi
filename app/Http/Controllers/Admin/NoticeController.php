<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use DB;
use Auth;
use App\Notice;
use Illuminate\Http\Request;

class NoticeController extends Controller
{
    public function index(){

        $data['get_all'] = Notice::get();
        return view('admin.notice.view', $data);
    }

    public function create(){
        $data['add'] = TRUE;
        return view('admin.notice.add', $data);
    }

    public function store(Request $request){

        $this->validate($request, [          
            'section_id' => 'required',               
            'title' => 'required',               
            'description' => 'required',               
            'file' => 'required'                     
        ]);

        try {
            $notice = new Notice;
            $notice->section_id = $request->section_id;
            $notice->title = $request->title;
            $notice->description = $request->description;
            $notice->created_by = Auth::id();
            if(!empty($request->file('file'))){
                $File = $request->file('file');
                $name = $File->getClientOriginalName();
                $ext = explode('.',$name);
                $finalName = 'notice-'.time().'.'.$ext[1];
                $uploadPath = 'public/notice/';
                $File->move($uploadPath, $finalName);
                $notice->file = $uploadPath.$finalName;
            }
            $notice->save();
             
            setMessage("message","success","Successful");
            return redirect(route('notices.index'));

        }catch (\Exception $e) {
            $err_message = \Lang::get($e->getMessage());
            setMessage("message","danger","Failed");
            return redirect(route('notices.index'));
            //return back();
        }
    }

    public function edit($id){
        $data['edit'] = TRUE;
        $data['single'] = Notice::findOrFail($id);
        return view('admin.notice.add', $data);
    }

    public function update(Request $request, $id){

        $this->validate($request, [          
            'section_id' => 'required',                     
            'title' => 'required',                     
            'description' => 'required'                     
        ]);

        try {
            $notice = Notice::findOrFail($id);
            $notice->section_id = $request->section_id;
            $notice->title = $request->title;
            $notice->description = $request->description;
            $notice->updated_by = Auth::id();
            if ($request->file != null) {

                $path = $notice->file;
                if (file_exists($path) and $notice->file !=null) {
                    unlink($path);
                }
                $File = $request->file('file');
                $name = $File->getClientOriginalName();
                $ext = explode('.',$name);
                $finalName = 'notice-'.time().'.'.$ext[1];
                $uploadPath = 'public/notice/';
                $File->move($uploadPath, $finalName);
                $notice->file = $uploadPath.$finalName;
            }
            $notice->save();

            setMessage("message","success","Successful");
            return redirect(route('notices.index'));

        }catch (\Exception $e) {
            $err_message = \Lang::get($e->getMessage());
            setMessage("message","danger","Failed");
            return back();
        }
    }

    public function destroy($id){

        try {
            $notice = Notice::findOrFail($id);
            $path = $notice->file;
            if (file_exists($path) and $notice->file != null) {
                unlink($path);
            }
            $notice->delete();
            setMessage("message","success","Successful");
            return back();
        }catch (\Exception $e) {
            $err_message = \Lang::get($e->getMessage());
            setMessage("message","danger","Failed");
            return back();
        }
    }

}//NoticeController