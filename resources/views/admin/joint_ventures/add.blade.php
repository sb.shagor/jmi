@extends('admin.layout.default')
@section('title')
  Joint Ventures
@endsection
@section('content')

  <div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
      @isset($edit)
        <div class="box box-warning">
          <div class="box-header with-border">
            <h3 class="box-title">Update Joint Ventures</h3>
            <div class="box-tools pull-right">
            </div>
          </div>
          <div class="box-body">
          <form action="{{ route('joint.update',$single->id) }}" method="post" enctype="multipart/form-data">
          @csrf
            <div class="row">
              <div class="col-md-10">

                <div class="form-group">
                    <label for="regional_branch_name">Title <span class="text-red">*</span></label>
                    <input type="text" class="form-control" id="title" name="title" placeholder="Enter Title" value="{{ $single->title }}" required>
                    <span class="text-danger">{{ $errors->has('title') ? $errors->first('title') : '' }}</span>
                </div>

                <div class="form-group">
                    <label for="regional_branch_name">Link <span class="text-red">*</span></label>
                    <input type="text" class="form-control" id="link" name="link" placeholder="Enter Link" value="{{ $single->link }}">
                    <span class="text-danger">{{ $errors->has('link') ? $errors->first('link') : '' }}</span>
                </div>

                <div class="form-group">
                    <label for=""></label>
                    <a href="" class="btn btn-info btn-sm btn-warning">Cancel</a>
                    <input type="submit" name="submit" class="btn btn-primary btn-sm pull-right" value="Update">
                </div>

              </div>
            </div>
          </form>         
          </div><!-- /.box-body -->
        </div><!-- /.box -->
      @endisset
    </section><!-- /.content -->
  </div><!-- /.content-wrapper -->

@endsection