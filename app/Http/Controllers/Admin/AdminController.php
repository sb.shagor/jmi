<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use DB;
use Auth;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function index(){

        $this->data['user_info'] = DB::table('users')->where('id',logged_in_user_id())->first();
    	return view('admin.home.homeContent', $this->data);
    }

    public function contactMessage(){

        $data['contact'] = DB::table('tbl_contact')->get();
        return view('admin.contact.contact', $data);
    }

    public function officeAddress(){
        $data['office_address'] = DB::table('tbl_office_address')->get();
        return view('admin.contact.view', $data);
    }

    public function editAddress($id){
        $data['single'] = DB::table('tbl_office_address')->find($id);
        return view('admin.contact.edit', $data);
    }

    public function updateAddress(Request $request){

        $result = DB::table('tbl_office_address')
            ->where('id',$request->id)
            ->update([
                'email'=>$request->email,
                'phone'=>$request->phone,
                'office_address'=>$request->office_address,
                'map'=>$request->map,
            ]);

        if($result){
            setMessage("message","success","Updated Successful");
            return redirect('edit-address/'.$request->id);
        }else{
            setMessage("message","danger","Updated Failed");
            return redirect('edit-address/'.$request->id);
        }
        
    }//update

}//AdminController