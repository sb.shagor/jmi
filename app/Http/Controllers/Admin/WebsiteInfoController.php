<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\WebsiteInfo;
use Auth;
use DB;

class WebsiteInfoController extends Controller
{

    public function index($id){
        $data['edit'] = TRUE;
        $data['single'] = WebsiteInfo::findOrFail($id);
        return view('admin.website.edit', $data);
    }

    public function update(Request $request, $id){

        $this->validate($request, [                      
            'logo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:512|dimensions:max_width=270,max_height=100',                    
        ]);

        try {
            $website = WebsiteInfo::findOrFail($id);
            $website->facebook = $request->facebook;
            $website->twitter = $request->twitter;
            $website->instragam = $request->instragam;
            $website->linkedin = $request->linkedin;
            $website->youtube = $request->youtube;
            if ($request->logo != null) {

                $path = $website->logo;
                if (file_exists($path) and $website->logo != null) {
                    unlink($path);
                }
                $File = $request->file('logo');
                $name = $File->getClientOriginalName();
                $ext = explode('.',$name);
                $finalName = 'logo-'.time().'.'.$ext[1];
                $uploadPath = 'public/uploads/';
                $File->move($uploadPath, $finalName);
                $website->logo = $uploadPath.$finalName;
            }
            $website->save();

            setMessage("message","success",updated_success());
            return redirect(route('website.index'));

        }catch (\Exception $e) {
            $err_message = \Lang::get($e->getMessage());
            setMessage("message","success",updated_success());
            return back();
        }
    }

}//WebsiteInfoController