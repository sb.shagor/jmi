<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Auth;

class SliderController extends Controller
{
    public function index(){

        $data['slider'] = DB::table('tbl_slider')->get();
        return view('admin.slider.view', $data);
    }

    public function add(){
        $data['add'] = TRUE;
        return view('admin.slider.add', $data);
    }

    public function store(Request $request){

        $imageUrl='';
        if(!empty($request->file('image'))){
            $Image = $request->file('image');
            $name = 'slider-'.$Image->getClientOriginalName();
            $ext = explode('.',$name);
            $finalName = 'slider-'.time().'.'.$ext[1];
            $uploadPath = 'public/uploads/';
            $Image->move($uploadPath, $finalName);
            $imageUrl = $uploadPath.$finalName;
        }

        $result=DB::table('tbl_slider')
            ->insert([
                'title'=>$request->title,
                'description'=>$request->description,
                'image'=>$imageUrl
            ]);

        if($result){
            setMessage("message","success",saved_success());
            return redirect('add-slide');
        }else{
            setMessage("message","danger",exception());
            return redirect('add-slide');
        }

    }//store

    public function edit($id){
        $data['edit'] = TRUE;
        $data['single'] = DB::table('tbl_slider')->find($id);
        return view('admin.slider.add', $data);
    }

    public function update(Request $request){

        $oldImg = DB::table('tbl_slider')->find($request->id);
        $Image = $request->file('image');
    
        if($Image){
            $preImg = $oldImg->image;
            if($preImg){
                unlink($preImg); 
            }
            $name = 'slider-'.$Image->getClientOriginalName();
            $ext = explode('.',$name);
            $finalName = 'slider-'.time().'.'.$ext[1];
            $uploadPath = 'public/uploads/';
            $Image->move($uploadPath, $finalName);
            $imageUrl = $uploadPath.$finalName;

            $result = DB::table('tbl_slider')
                ->where('id',$request->id)
                ->update([
                    'title'=>$request->title,
                    'description'=>$request->description,
                    'image'=>$imageUrl,
                ]);

            if($result){
                setMessage("message","success",updated_success());
                return redirect('edit-slide/'.$request->id);
            }else{
                setMessage("message","danger",exception());
                return redirect('edit-slide/'.$request->id);
            }
    
        }else{

            $result = DB::table('tbl_slider')
                ->where('id',$request->id)
                ->update([
                    'title'=>$request->title,
                    'description'=>$request->description,
                ]);

            if($result){
                setMessage("message","success",updated_success());
                return redirect('edit-slide/'.$request->id);
            }else{
                setMessage("message","danger",exception());
                return redirect('edit-slide/'.$request->id);
            }
        }
        
    }//update

    public function delete($id){

        $Old = DB::table('tbl_slider')->find($id);
        $preImg = $Old->image;
         if($preImg){
            unlink($preImg); 
        }
        $result = DB::table('tbl_slider')->where('id', '=', $id)->delete();
        if($result){
            setMessage("message","success","Successful");
            return redirect('manage-slide');
        }else{
            setMessage("message","danger",exception());
            return redirect('manage-slide');
        }
    }

}
