<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use DB;
use Auth;
use App\OurMission;
use Illuminate\Http\Request;

class OurMissionController extends Controller
{
    public function index(){

        $data['get_all'] = OurMission::get();
        return view('admin.mission.view', $data);
    }

    public function edit($id){
        $data['edit'] = TRUE;
        $data['single'] = OurMission::findOrFail($id);
        return view('admin.mission.edit', $data);
    }

    public function update(Request $request, $id){

        $this->validate($request, [                      
            'section_id' => 'required',                     
            'description' => 'required',                     
        ]);

        try {
            $mission = OurMission::findOrFail($id);
            $mission->section_id = $request->section_id;
            $mission->description = $request->description;
            $mission->save();

            setMessage("message","success",updated_success());
            return redirect(route('mission.index'));

        }catch (\Exception $e) {
            $err_message = \Lang::get($e->getMessage());
            setMessage("message","danger",exception());
            return back();
        }
    }

}