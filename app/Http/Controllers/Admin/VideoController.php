<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use DB;
use Auth;
use App\Video;
use Illuminate\Http\Request;

class VideoController extends Controller
{
    public function index(){

        $data['get_all'] = Video::get();
        return view('admin.video.view', $data);
    }

    public function create(){
        $data['add'] = TRUE;
        return view('admin.video.add', $data);
    }

    public function store(Request $request){

        $this->validate($request, [                     
            'iframe' => 'required',                   
        ]);

        try {
            $video = new Video;
            $video->category = $request->category;
            $video->iframe = $request->iframe;
            $video->created_by = Auth::id();
            $video->save();
             
            setMessage("message","success",saved_success());
            return redirect(route('videos.index'));

        }catch (\Exception $e) {
            $err_message = \Lang::get($e->getMessage());
            setMessage("message","danger",exception());
            return back();
        }
    }

    public function edit($id){
        $data['edit'] = TRUE;
        $data['single'] = Video::findOrFail($id);
        return view('admin.video.add', $data);
    }

    public function update(Request $request, $id){

        $this->validate($request, [                     
            'iframe' => 'required',                    
        ]);

        try {
            $video = Video::findOrFail($id);
            $video->category = $request->category;
            $video->iframe = $request->iframe;
            $video->updated_by = Auth::id();
            $video->save();

            setMessage("message","success",updated_success());
            return redirect(route('videos.index'));

        }catch (\Exception $e) {
            $err_message = \Lang::get($e->getMessage());
            setMessage("message","danger",exception());
            return back();
        }
    }

    public function destroy($id){

        try {
            $video = Video::findOrFail($id);
            $video->delete();
            setMessage("message","success",deleted_success());
            return back();
        }catch (\Exception $e) {
            $err_message = \Lang::get($e->getMessage());
            setMessage("message","danger",exception());
            return back();
        }
    }

}