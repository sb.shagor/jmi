@extends('website.layout.default')
@section('title')
    JMI | Under Construction
@endsection
@section('content')

<section class="advanced-area">
    <div class="container">
        <h1>Page is under construction</h1>
        <div class="col-md-2"></div>
        <div class="col-md-10">
            <img class="img-responsive" src="{{ asset('public/web/img/interface/under_con.png') }}">
        </div>
    </div>
</section>

@endsection