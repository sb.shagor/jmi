<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use App\JointVentures;
use App\WebsiteInfo;
use DB;
use View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        
        $contact = DB::table('tbl_office_address')->first();
        View::share('contact', $contact);

        $joint_ventures = JointVentures::get();
        View::share('joint_ventures', $joint_ventures);

        $website_info = WebsiteInfo::first();
        View::share('website_info', $website_info);

        $awards = DB::table('tbl_gallery')->where('section_id', 2)->limit(15)->get();
        View::share('awards', $awards);
    }
}
