-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 09, 2021 at 09:53 AM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 7.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `jmi`
--

-- --------------------------------------------------------

--
-- Table structure for table `businesses`
--

CREATE TABLE `businesses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `position` int(3) NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `businesses`
--

INSERT INTO `businesses` (`id`, `position`, `title`, `file`, `link`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 3, 'JMI Vaccine Ltd.', 'public/uploads/business-1634462470.png', 'https://www.jmigroup-bd.com/JMI-Vaccine-Ltd', 1, 1, '2021-10-17 03:21:10', '2021-10-19 03:03:21'),
(2, 2, 'Nipro JMI Pharma Ltd.', 'public/uploads/business-1634462502.png', 'Nipro JMI Pharma Ltd.', 1, 1, '2021-10-17 03:21:42', '2021-10-17 03:22:49'),
(3, 1, 'JMI Syringes & Medical Devices Ltd.', 'public/uploads/business-1634634240.png', 'https://www.jmisyringe.com/', 1, NULL, '2021-10-19 03:04:00', '2021-10-19 03:04:00'),
(4, 4, 'JMI Hospital Requisite MFG. Ltd.', 'public/uploads/business-1634634301.png', 'https://jhrml-bd.com/', 1, NULL, '2021-10-19 03:05:01', '2021-10-19 03:05:01'),
(5, 5, 'Nipro JMI Co. Ltd.', 'public/uploads/business-1634634343.png', 'https://www.jmigroup-bd.com/nipro-jmi-co-ltd', 1, NULL, '2021-10-19 03:05:43', '2021-10-19 03:05:43'),
(6, 6, 'JMI Export Import Co. Ltd.', 'public/uploads/business-1634634400.png', '#', 1, NULL, '2021-10-19 03:06:40', '2021-10-19 03:06:40'),
(7, 7, 'JMI Builders & Construction Ltd.', 'public/uploads/business-1634634440.png', 'http://www.jmibuilders.com/', 1, NULL, '2021-10-19 03:07:20', '2021-10-19 03:07:20'),
(8, 8, 'JMI CNG Dispensing Co. Ltd.', 'public/uploads/business-1634634480.png', '#', 1, NULL, '2021-10-19 03:08:00', '2021-10-19 03:08:00'),
(9, 9, 'JMI Printing & Packaging Ltd', 'public/uploads/business-1634634555.png', '#', 1, NULL, '2021-10-19 03:09:15', '2021-10-19 03:09:15'),
(10, 10, 'JMI LPG Plantation', 'public/uploads/business-1634634590.png', '#', 1, NULL, '2021-10-19 03:09:50', '2021-10-19 03:09:50'),
(11, 11, 'JMI Cylinders Ltd.', 'public/uploads/business-1634634622.png', 'http://www.jmicylinders.com/', 1, NULL, '2021-10-19 03:10:22', '2021-10-19 03:10:22'),
(12, 12, 'JMI Safe Transportation Ltd.', 'public/uploads/business-1634634654.png', '#', 1, NULL, '2021-10-19 03:10:54', '2021-10-19 03:10:54');

-- --------------------------------------------------------

--
-- Table structure for table `careers`
--

CREATE TABLE `careers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `job_nature` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `job_location` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `salary` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vacancies` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `file` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `deadline` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `careers`
--

INSERT INTO `careers` (`id`, `title`, `job_nature`, `job_location`, `salary`, `vacancies`, `description`, `file`, `status`, `deadline`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(2, 'Junior Engineer (CSE)', 'Fulltime', 'Agargaon', 'Negociatable', '3', '<p>Job Context</p>\r\n\r\n<ul>\r\n	<li>The Junior Engineer (CSE) will be based in Dhaka. The job involves programming, designing, testing, implementing, and maintaining computer Software, allied Apps, and embedded systems.</li>\r\n	<li>This job is going to be a permanent one, having bright chances for upward mobility.</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Job Responsibilities</p>\r\n\r\n<ul>\r\n	<li>The Junior Engineer (CSE)&#39;s responsibilities include developing expertise basically in the existing software management of the Electronic Seal &amp; Lock, Electromechanical Lock, for providing Wireless Solutions for tracking and securing high value &amp; dangerous cargo and assets. Follow the practices, and procedures of ESL gadgets so as to serve our customers with state-of-the-art technology.</li>\r\n	<li>In particular, working with the Assistant Engineers, down to the field level workforces, focusing on equipment, technology, and the infrastructure of micro and macro-level operating systems, and help running the system in a smooth manner.</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Educational Requirements</p>\r\n\r\n<ul>\r\n	<li>Bachelor in Engineering (BEngg) in CSE, Master of Information &amp; Communication Technology (MICT) in MICT</li>\r\n	<li>The candidate should have a good command of the English language. Masters in Information and Communication Technology (MICT) will have advantages. However, education, concentration/major may be relaxed for experienced candidates.</li>\r\n	<li>Skills Required: Data management, Electrical Electronics, Embedded System, Graphics Design, programiming language</li>\r\n</ul>\r\n\r\n<p><br />\r\n&nbsp;</p>\r\n\r\n<p>Experience Requirements</p>\r\n\r\n<ul>\r\n	<li>2 to 3 year(s)</li>\r\n	<li>The applicants should have experience in the following area(s): Computer Engineering, Embedded System, IT Support Service, IT System Management, Mobile apps developer, Software Development</li>\r\n	<li>The applicants should have experience in the following business area(s): Computer Hardware/Network Companies, Engineering Firms, IT Enabled Service, Technical Infrastructure</li>\r\n	<li>Freshers are also encouraged to apply.</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Compensation &amp; Other Benefits</p>\r\n\r\n<ul>\r\n	<li>T/A, Mobile bill, Tour allowance</li>\r\n	<li>Lunch Facilities: Partially Subsidize</li>\r\n	<li>Festival Bonus: 2</li>\r\n</ul>', 'public/career/career-1634363542.png', 1, '2021-10-31', 1, 1, '2021-09-20 04:36:14', '2021-10-16 21:25:35');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `joint_ventures`
--

CREATE TABLE `joint_ventures` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `joint_ventures`
--

INSERT INTO `joint_ventures` (`id`, `title`, `link`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'Nipro Corporation, Japan', 'https://www.nipro.co.jp/en/index.html', 1, NULL, '2021-10-17 03:49:47'),
(2, 'SUNKUR-Turkey', 'http://en.sankur.com.tr/', 1, NULL, '2021-10-17 03:50:21'),
(3, 'sonlu', NULL, 1, NULL, '2021-10-17 03:50:34'),
(4, 'Star Syringe Ltd UK', 'https://www.starsyringe.com/', 1, NULL, '2021-10-17 03:50:48');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_02_19_040502_create_role_permissions_table', 1),
(5, '2020_09_06_064224_create_suppliers_table', 1),
(6, '2020_09_26_061536_create_buyers_table', 2),
(7, '2020_09_27_043948_create_items_table', 3),
(8, '2020_09_27_044228_create_grades_table', 3),
(9, '2020_09_29_051344_create_bank_accounts_table', 4),
(10, '2020_09_30_091528_create_modules_table', 5),
(11, '2020_09_30_100024_create_role_permissions_table', 6),
(12, '2021_01_12_061551_create_students_table', 7),
(13, '2021_01_12_090510_create_payments_table', 8),
(14, '2021_06_05_143012_create_pages_table', 9),
(15, '2021_06_10_043041_create_lavels_table', 10),
(16, '2021_06_10_045710_create_subjects_table', 11),
(17, '2021_06_10_055651_create_schools_table', 12),
(18, '2021_09_20_084751_create_notices_table', 13),
(19, '2021_09_20_102308_create_careers_table', 14),
(20, '2021_09_21_050101_create_enrollments_table', 15),
(21, '2021_09_21_054735_create_age_structures_table', 16),
(22, '2021_09_21_083331_create_fees_structures_table', 17),
(23, '2021_09_21_085032_create_year_structures_table', 18),
(24, '2021_09_21_092722_create_payment_schedules_table', 19),
(25, '2021_09_22_045652_create_curricula_table', 20),
(26, '2021_09_22_062501_create_clas_table', 21),
(27, '2021_09_22_062532_create_class_images_table', 21),
(28, '2021_09_22_094459_create_welcome_messages_table', 22),
(29, '2021_09_22_100414_create_our_missions_table', 23),
(30, '2021_09_22_105855_create_pto_committees_table', 24),
(31, '2021_09_23_043307_create_teaching_staff_table', 25),
(32, '2021_09_23_043455_create_staff_table', 25),
(33, '2021_09_26_042504_create_videos_table', 26),
(34, '2021_10_13_061011_create_website_pdfs_table', 27),
(35, '2021_10_17_074904_create_businesses_table', 28),
(36, '2021_10_17_094042_create_joint_ventures_table', 29),
(37, '2021_10_18_045704_create_website_infos_table', 30);

-- --------------------------------------------------------

--
-- Table structure for table `notices`
--

CREATE TABLE `notices` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `section_id` int(3) NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `file` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `notices`
--

INSERT INTO `notices` (`id`, `section_id`, `title`, `description`, `file`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 2, 'Visit of Md. Asadul Islam, Secretary, Health Service, Ministry of Health & Family Welfare, to JMI Industrial Park', '<h1>Visit of Md. Asadul Islam, Secretary, Health Service, Ministry of Health &amp; Family Welfare, to JMI Industrial Park</h1>\r\n\r\n<p>SUNDAY 27TH OF OCTOBER 2019</p>\r\n\r\n<p>JMI Industrial Park; a state-of-the art technology medical devices manufacturing plants in Bangladesh.&nbsp;<strong>Mr. Md. Asadul Islam</strong>, Secretary, Health Service, Ministry of Health &amp; Family Welfare, Government of the People&rsquo;s Republic of Bangladesh, visited JMI Industrial Park on October 27, 2019 with some high officials of Bangladesh Government. Mr. Islam expressed his satisfaction after visiting JMI Industrial Park and he hopes JMI Group will contribute more good products and services in Bangladesh. Some of his encouraging words about JMI Group are given below;<br />\r\n<br />\r\n<em>&ldquo;Fascinating to see the state of the art technology used and the standards being maintained in the industry. It is rewarding to know that this is the first of this kind of industry in the country and serving the country and many other overseas countries within their products which are recognized as the global standard<br />\r\n<br />\r\nThe leader of this initiative is very successful in team building and the dedication he has ushered in the team members for the same goal and targets<br />\r\n<br />\r\nHope that the enterprise will be flourished further to generate more good to the country and to the human society as a whole&rdquo;&nbsp;<strong>-- Md. Asadul Islam</strong></em></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Mr. Md. Abdur Razzaq</strong>, honorable Founder Managing Director of JMI Group was present during the visit of Mr. Secretary and other high officials. Mr. Islam hopes Mr. Razzaq will continue his efforts to build more state-of-the art industries. He also wishes the prosperous future of JMI Group.<br />\r\n<br />\r\nAll employees of JMI Group are delighted to get&nbsp;<strong>Mr. Md. Asadul Islam</strong>, Secretary, Health Service, Ministry of Health &amp; Family Welfare, Government of the People&rsquo;s Republic of Bangladesh at JMI Industrial Park.<br />\r\n&nbsp;</p>', 'public/notice/notice-1636283672.jpg', 1, 1, 1, '2021-09-20 03:37:06', '2021-11-07 17:14:32'),
(2, 2, 'CIP Award 2017', '<p>THURSDAY 21ST OF NOVEMBER 2019</p>\r\n\r\n<p><strong>Mr. Md. Abdur Razzaq</strong>, Founder Managing Director of JMI Group has been recognized as Commercially Important Person (CIP) for his outstanding contribution to the large Industry (Manufacturing) of Bangladesh.</p>\r\n\r\n<p><strong>Mr. Nurul Majid Mahmud Humayun</strong>&nbsp;MP, Honorable Minister, Ministry of Industries, Government of the People&#39;s Republic of Bangladesh has distributed CIP cards 2017 among 48 entrepreneurs in the ceremony at Pan Pacific Sonargaon Dhaka. Prime Minister&rsquo;s Private Industry and Investment Affairs Adviser Salman F Rahman, State Minister for Industries Mr. Kamal Ahmed Majumder MP, and President of the Federation of Bangladesh Chambers of Commerce and Industry (FBCCI) Mr. Sheikh Fazle Fahim were present as special guests.<br />\r\n<br />\r\nMr. Razzaq is one of the leading business personalities and is treated as a man behind the revaluation in the manufacturing of medical devices in Bangladesh. He was born in 1963 in a village Azizpur under Sanbag Upazila of Noakhali District. He accomplished his Masters&#39;s in Economics from Chittagong University. His business is evolving centering on a sole motto - to serve humanity; He wants to build a&nbsp;<strong>Mini Bangladesh</strong>&nbsp;in every country of the world and keeping this dream in mind he has visited more than 90 countries around the world. Mr. Razzaq is very talented and has an innovative personality that made JMI Group one of the most promising business conglomerates. JMI Syringes &amp; Medical Devices Ltd., the flagship company of JMI Group and started its journey in 1999. Mr. Razzaq struggled a lot to achieve name, fame, and honor in his sector. JMI Group has joint ventured business with Japan, South Korea, UK, Thailand, Turkey, China, etc.</p>', 'public/notice/notice-1636283480.jpg', 1, 1, 1, '2021-09-20 03:37:55', '2021-11-07 17:11:20'),
(5, 2, 'JMI Group has provided 03 (three) set 55’’ 4K Android Digital Stand Signage to the Department of Environment, Bangladesh Government.', '<p>JMI Group has provided 03 (three) set 55&rsquo;&rsquo; 4K Android Digital Stand Signage to the Department of Environment, Bangladesh Government for creating social awareness about environmental safety issues.<br />\r\n<br />\r\nOn behalf of JMI Group Managing Director Mr. Md. Abdur Razzaq, CIP handed over 03 (three) set 55&rsquo;&rsquo; 4K Android Digital Stand Signage to honorable Director General (DG) of the Department of Environment Dr. A, K, M Rafique Ahammed at the Department of Environment office, Agargaon in the capital Dhaka on 18 November, 2020 Wednesday.<br />\r\n<br />\r\nAmong others, JMI Group Executive Director (Financial Management) Mr. Md. Mohiuddin Ahmed, Group CFO Mr. Md. Zahangir Alam, Department of Environment Additional Director General- Mr. Humayun Kabir, Director Environmental Clearance- Mr. Syed Nazmul Ahsan, Director (Administration) - Dr. Md. Moksed Ali and others were also present on the occasion.<br />\r\n<br />\r\nMr. Md. Abdur Razzaq, CIP said that as the most promising and fast growing business conglomerate in the country JMI Group established more than 30 companies with remarkable local and foreign investment from Japan, Korea, Turkey, Thailand &amp; China. JMI Group established its green ecofriendly factories on the Dhaka-Chattogram highway in the place of Gazaria-Munshiganj, Chauddagram-Cumilla &amp; Sitakunda-Chattogram.<br />\r\n<br />\r\nDepartment of Environment Director-General Dr. AKM Rafique Ahammed expressed his gratitude to JMI Group Managing Director Mr. Md. Abdur Razzaq, CIP for his contribution to the economy of Bangladesh by ensuring environment friendly industries.&nbsp;</p>', 'public/notice/notice-1636283103.jpg', 1, 1, 1, '2021-09-26 05:08:53', '2021-11-07 17:05:03');

-- --------------------------------------------------------

--
-- Table structure for table `our_missions`
--

CREATE TABLE `our_missions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `section_id` int(3) NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `our_missions`
--

INSERT INTO `our_missions` (`id`, `section_id`, `description`, `created_at`, `updated_at`) VALUES
(1, 1, '<p>The JMI Group is one of the leading and most diversified global conglomerates in Bangladesh. The company was established in April 1999, having offices in all major cities, employing more than 7,000 employees and dedicated to bringing the highest quality products and services to our customers. We have good annual turnover with such diversified interests as Syringes &amp; Medical Devices, Pharmaceuticals, Life-saving Vaccine manufacturing, Hospital Requisite manufacturing, Printing &amp; Packaging Services, Export &amp; Import businesses, the Construction sector, Safe Transportation sector to name a few.<br />\r\n<br />\r\nThe conglomerate has an outstanding record of all-around excellence in growth in different business activities.</p>', '2021-10-17 05:20:46', '2021-11-07 14:57:02'),
(2, 2, '<p><strong>Our Mission:&nbsp;</strong>We view business as a means to increase the material and social well-being of the investors, employees, and society at large, leading to an acceleration of wealth through financial and moral gains as a part of the process of human civilization.<br />\r\n<br />\r\n<strong>Our Vision:</strong>&nbsp;Our Mission is to play a significant role in total business operation also ensuring benefit to the shareholders, stakeholders, and the society at large. We view ourselves as partners with our customers, our employees, and our environment.<br />\r\n<br />\r\n<strong>Our objective:</strong>&nbsp;Our objectives are to conduct transparent business operations based on market mechanisms within the legal &amp; social framework with aims to attain the mission reflected by our vision.</p>\r\n\r\n<p>&nbsp;</p>', '2021-10-17 05:20:48', '2021-11-07 15:04:45');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `type` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT 'custom',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `role_name`, `type`, `created_at`, `updated_at`) VALUES
(1, 'Super Admin', 'system', NULL, '2021-01-04 00:38:36'),
(2, 'Employee', 'system', NULL, '2020-09-27 04:10:53');

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

CREATE TABLE `staff` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `section_id` int(3) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `designation` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `staff`
--

INSERT INTO `staff` (`id`, `section_id`, `name`, `designation`, `file`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 1, 'Md. Abdur Razzaq, CIP', 'Founder Managing Director', 'public/staff/staff-1634450959.png', 1, 1, '2021-09-22 22:56:58', '2021-10-17 00:09:19'),
(2, 1, 'Md. Jabed Iqbal Pathan', 'Chairman', 'public/staff/staff-1634450763.png', 1, 1, '2021-09-22 22:57:36', '2021-10-17 00:06:03'),
(11, 2, 'Md. Abdur Razzaq, CIP', 'Founder Managing Director', 'public/staff/staff-1634451464.png', 1, NULL, '2021-10-17 00:17:44', '2021-10-17 00:17:44'),
(12, 2, 'Md. Jabed Iqbal Pathan', 'Chairman', 'public/staff/staff-1634451492.png', 1, NULL, '2021-10-17 00:18:12', '2021-10-17 00:18:12');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_about`
--

CREATE TABLE `tbl_about` (
  `id` int(11) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_about`
--

INSERT INTO `tbl_about` (`id`, `description`) VALUES
(1, '<p>To ensure a quality educational system comparable to that of schools in Australia, AusIS participates in annual Benchmark Testing which provides data on student educational outcomes. These results are compared to those of similar students in Western Australia. These assessments inform our future planning and shape our whole-school approach to teacher appraisals and educational issues.<br />\r\n<br />\r\n<strong>Elementary School:</strong><br />\r\nOur Early Childhood Education program is an integrated program that is delivered through the Western Australia Curriculum Framework.<br />\r\n<br />\r\n<strong>Year 1- 12:</strong><br />\r\nThe Year 1- 12 curriculum is based on the outcomes and standards framework of the School Curriculum and Standards Authority of Western Australia. &gt;&gt; The school is also fully endorsed with the Secondary &amp; Higher Secondary Education Board to conduct courses &gt;&gt; AusIS is the first overseas school to be affiliated with the prestigious Association of Independent School of Western Australia (AISWA) and is currently working toward full endorsement with AISWA.</p>');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_album`
--

CREATE TABLE `tbl_album` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `photo` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_album`
--

INSERT INTO `tbl_album` (`id`, `title`, `photo`) VALUES
(1, '21st February', 'public/albumImage/folder.png'),
(3, 'myp dp and wace sports day', 'public/albumImage/folder.png'),
(4, 'spring festival', 'public/albumImage/folder.png'),
(5, 'UN Day Photos', 'public/albumImage/folder.png'),
(6, 'Victory Day 2019', 'public/albumImage/folder.png'),
(7, 'WACE batch 2019 graduation', 'public/albumImage/folder.png'),
(8, 'Campus', 'public/albumImage/folder.png');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_application`
--

CREATE TABLE `tbl_application` (
  `id` int(11) NOT NULL,
  `name` varchar(191) NOT NULL,
  `email` varchar(191) NOT NULL,
  `phone` varchar(191) NOT NULL,
  `post` varchar(191) NOT NULL,
  `address` text NOT NULL,
  `file` varchar(191) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_application`
--

INSERT INTO `tbl_application` (`id`, `name`, `email`, `phone`, `post`, `address`, `file`, `created_at`) VALUES
(1, 'solaman badsha', 'shagor@wanitbd.com', '01814000000', 'Junior Engineer (CSE)', 'dhaka', 'public/uploads/cv-1635140779.pdf', '2021-10-25 05:46:19');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_contact`
--

CREATE TABLE `tbl_contact` (
  `id` int(11) NOT NULL,
  `name` varchar(55) NOT NULL,
  `email` varchar(55) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `message` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_contact`
--

INSERT INTO `tbl_contact` (`id`, `name`, `email`, `phone`, `message`) VALUES
(2, 'Solaman Badsha', 'shagor.ahmed374@gmail.com', '01814944730', 'Your message has been saved'),
(3, 'admin', 'shagor.ahmed374@gmail.com', '01814944730', 'asdasdasdasd');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_gallery`
--

CREATE TABLE `tbl_gallery` (
  `id` int(11) NOT NULL,
  `section_id` int(3) NOT NULL,
  `album_id` int(11) DEFAULT 1,
  `title` varchar(100) DEFAULT NULL,
  `photo` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_gallery`
--

INSERT INTO `tbl_gallery` (`id`, `section_id`, `album_id`, `title`, `photo`) VALUES
(1, 1, NULL, NULL, 'public/galleryImage/gallery-1634366093.png'),
(2, 1, NULL, NULL, 'public/galleryImage/gallery-1634454211.png'),
(3, 1, 8, 'Campus', 'public/galleryImage/gallery-03.jpg'),
(4, 1, 8, 'Campus', 'public/galleryImage/gallery-04.jpg'),
(5, 1, 8, 'Campus', 'public/galleryImage/gallery-05.jpg'),
(6, 1, 8, 'Campus', 'public/galleryImage/gallery-06.jpg'),
(7, 1, 8, 'Campus', 'public/galleryImage/gallery-07.jpg'),
(8, 1, 8, 'Campus', 'public/galleryImage/gallery-08.jpg'),
(9, 1, 8, 'Campus', 'public/galleryImage/gallery-09.jpg'),
(10, 1, 8, 'Campus', 'public/galleryImage/gallery-10.jpg'),
(11, 1, 8, 'Campus', 'public/galleryImage/gallery-11.jpg'),
(12, 1, 8, 'Campus', 'public/galleryImage/gallery-12.jpg'),
(14, 1, 1, '21st february', 'public/galleryImage/image1633925939.png'),
(15, 1, 1, '21st February Celebration', 'public/galleryImage/image1633925971.png'),
(16, 1, 1, '21st February Celebration', 'public/galleryImage/image1633925983.png'),
(17, 1, 1, '21st February Celebration', 'public/galleryImage/image1633925989.png'),
(18, 1, 1, '21st February Celebration', 'public/galleryImage/image1633926019.png'),
(19, 1, 1, '21st February Celebration', 'public/galleryImage/image1633926032.png'),
(20, 1, 1, '21st February Celebration', 'public/galleryImage/image1633926045.png'),
(21, 1, 1, '21st February Celebration', 'public/galleryImage/image1633926060.png'),
(22, 1, 1, '21st February Celebration', 'public/galleryImage/image1633926069.png'),
(23, 1, 1, '21st February Celebration', 'public/galleryImage/image1633926077.png'),
(24, 1, 1, '21st February Celebration', 'public/galleryImage/image1633926092.png'),
(26, 2, 1, '21st February Celebration', 'public/galleryImage/image1633926131.png'),
(27, 2, 1, '21st February Celebration', 'public/galleryImage/image1633926138.png'),
(28, 2, 1, '21st February Celebration', 'public/galleryImage/image1633926150.png'),
(29, 2, 1, '21st February Celebration', 'public/galleryImage/image1633926167.png'),
(30, 2, 1, '21st February Celebration', 'public/galleryImage/image1633926177.png'),
(31, 2, 1, '21st February Celebration', 'public/galleryImage/image1633926190.png'),
(32, 2, 1, '21st February Celebration', 'public/galleryImage/image1633926204.png'),
(33, 2, 1, '21st February Celebration', 'public/galleryImage/image1633926214.png'),
(34, 2, 1, '21st February Celebration', 'public/galleryImage/image1633926226.png'),
(35, 2, 1, '21st February Celebration', 'public/galleryImage/image1633926240.png'),
(36, 2, 3, 'MYP DP and wace sports day', 'public/galleryImage/IMG_ (324).JPG'),
(37, 2, 3, 'MYP DP and wace sports day', 'public/galleryImage/IMG_ (325).JPG'),
(38, 2, 3, 'MYP DP and wace sports day', 'public/galleryImage/IMG_ (328).JPG'),
(39, 2, 3, 'MYP DP and wace sports day', 'public/galleryImage/IMG_ (332).JPG'),
(40, 2, 4, 'Spring Festival', 'public/galleryImage/IMG_5180.JPG'),
(41, 2, 4, 'Spring Festival', 'public/galleryImage/IMG_5185.JPG'),
(42, 2, 4, 'Spring Festival', 'public/galleryImage/IMG_5193.JPG'),
(43, 2, 4, 'Spring Festival', 'public/galleryImage/IMG_5194.JPG'),
(44, 2, 4, 'Spring Festival', 'public/galleryImage/IMG_5197.JPG'),
(49, 2, 6, 'Victory Day 2019', 'public/galleryImage/IMG_0978.JPG'),
(50, 2, 6, 'Victory Day 2019', 'public/galleryImage/IMG_1006.JPG'),
(51, 2, 6, 'Victory Day 2019', 'public/galleryImage/IMG_1015.JPG'),
(52, 2, 6, 'Victory Day 2019', 'public/galleryImage/IMG_1021.JPG'),
(53, 2, 6, 'Victory Day 2019', 'public/galleryImage/IMG_1053.JPG'),
(54, 2, 7, 'WACE Graduation', 'public/galleryImage/IMG_1676.JPG'),
(55, 2, 7, 'WACE Graduation', 'public/galleryImage/IMG_1685.JPG'),
(56, 2, 7, 'WACE Graduation', 'public/galleryImage/IMG_1730.JPG'),
(57, 2, 7, 'WACE Graduation', 'public/galleryImage/IMG_1800.JPG'),
(58, 2, 7, 'WACE Graduation', 'public/galleryImage/IMG_1780.JPG'),
(59, 2, 7, 'WACE Graduation', 'public/galleryImage/IMG_1938.JPG'),
(61, 2, NULL, NULL, 'public/galleryImage/gallery-1634636231.png'),
(62, 2, NULL, NULL, 'public/galleryImage/gallery-1634636239.png'),
(63, 2, NULL, NULL, 'public/galleryImage/gallery-1634636247.png'),
(64, 2, NULL, NULL, 'public/galleryImage/gallery-1634636253.png');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_join_us`
--

CREATE TABLE `tbl_join_us` (
  `id` int(9) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_join_us`
--

INSERT INTO `tbl_join_us` (`id`, `description`) VALUES
(1, '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_office_address`
--

CREATE TABLE `tbl_office_address` (
  `id` int(9) NOT NULL,
  `office_address` text NOT NULL,
  `map` text DEFAULT NULL,
  `email` text DEFAULT NULL,
  `phone` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_office_address`
--

INSERT INTO `tbl_office_address` (`id`, `office_address`, `map`, `email`, `phone`) VALUES
(1, '<p>Unique Heights (Level-11), 117, Kazi Nazrul Islam Avenue, Ramna, Dhaka-1217, Bangladesh.</p>', '<iframe width=\"500\" height=\"450\" id=\"gmap_canvas\" src=\"https://maps.google.com/maps?q=JMI%20Group,%20unique%20heights,%20117%20kazi%20nazrul%20islam%20avenue,%20Dhaka&t=&z=13&ie=UTF8&iwloc=&output=embed\" frameborder=\"0\" scrolling=\"no\" marginheight=\"0\" marginwidth=\"0\"></iframe>', '<p><a href=\"mailto:info@jmi.com.bd\" target=\"blank\">info@jmi.com.bd</a></p>', '<p>880-2-55138723, 55138724</p>');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_slider`
--

CREATE TABLE `tbl_slider` (
  `id` int(9) NOT NULL,
  `title` varchar(191) DEFAULT NULL,
  `description` varchar(191) DEFAULT NULL,
  `image` varchar(191) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_slider`
--

INSERT INTO `tbl_slider` (`id`, `title`, `description`, `image`, `created_at`) VALUES
(1, 'JMI Auto Gas & Conversion', 'Located on Diplomatic Zone, Banani Dhaka!', 'public/uploads/slider-1634531728.png', '2021-10-18 04:35:28'),
(3, 'JMI Builders & Construction Ltd.', 'Only 15 Minutes from Hazrat Shahjalal International Airport.', 'public/uploads/slider-1634531792.png', '2021-10-18 04:36:32'),
(6, 'JMI Syringes & Medical Devices Ltd.', 'Airport protocol service for arrival & departing guests', 'public/uploads/slider-1634531842.png', '2021-10-18 04:37:22'),
(7, 'KN 95 Launcing', 'surrounded by Embassies, High Commissions & corporate offices', 'public/uploads/slider-1634532046.png', '2021-10-18 04:40:46'),
(8, 'BCTL Inauguration', 'surrounded by Embassies, High Commissions & corporate offices', 'public/uploads/slider-1634532067.png', '2021-10-18 04:41:07');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) CHARACTER SET utf8 NOT NULL,
  `phone` varchar(191) CHARACTER SET utf8 NOT NULL,
  `role_id` int(11) DEFAULT NULL,
  `email` varchar(191) CHARACTER SET utf8 NOT NULL,
  `password` varchar(191) CHARACTER SET utf8 NOT NULL COMMENT '123456',
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `image` mediumtext CHARACTER SET utf8 DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `phone`, `role_id`, `email`, `password`, `status`, `image`, `created_at`, `updated_at`) VALUES
(1, 'JMI', '01974000000', 1, 'jmi@gmail.com', '$2y$10$R6mGIVc8IAsls7rOdcxFgOzhCnWr.ubrMhPNN1h5c4xdF12gjomY.', 1, 'public/userImage/me2.jpg', '2020-09-27 11:16:35', '2020-09-27 11:16:35'),
(7, 'new user pass 123456', '01814000000', NULL, 'new@example.com', '$2y$10$GfWb25CmGW4tTwcWO280GuBs4Rch5bDtyGU3/EYIWqXRJciHtHmqO', 1, 'public/userImage/DSC_0590.jpg', '2021-10-18 06:40:28', '2021-10-18 06:40:28');

-- --------------------------------------------------------

--
-- Table structure for table `videos`
--

CREATE TABLE `videos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `iframe` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `videos`
--

INSERT INTO `videos` (`id`, `category`, `iframe`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(3, 'video', '<iframe width=\"100%\" height=\"290\" src=\"https://www.youtube.com/embed/Q0kVE7D0qZ8\" title=\"YouTube video player\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>', 1, 1, '2021-09-26 00:35:10', '2021-10-16 00:22:23'),
(4, 'video', '<iframe width=\"360\" height=\"315\" src=\"https://www.youtube.com/embed/v0q0BY3i1eI\" title=\"YouTube video player\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>', 1, 1, '2021-09-26 00:35:29', '2021-10-16 00:23:59'),
(6, 'tvc_ovc', '<iframe width=\"100%\" height=\"315\" src=\"https://www.youtube.com/embed/ss7EJ-PW2Uk\" title=\"YouTube video player\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>', 1, 1, '2021-11-09 00:08:22', '2021-11-09 00:18:32');

-- --------------------------------------------------------

--
-- Table structure for table `website_infos`
--

CREATE TABLE `website_infos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `facebook` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `twitter` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `instragam` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `linkedin` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `youtube` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `website_infos`
--

INSERT INTO `website_infos` (`id`, `facebook`, `twitter`, `instragam`, `linkedin`, `youtube`, `logo`, `created_at`, `updated_at`) VALUES
(1, 'https://www.facebook.com/jmigroup', 'https://twitter.com/', 'https://www.instagram.com/jmigroupbd/', 'https://www.linkedin.com/company/jmigroup/', 'https://www.youtube.com/channel/UCEFgdV_trg_ANNlHbX4Tw0Q', 'public/uploads/logo-1634637968.png', NULL, '2021-10-19 04:06:08');

-- --------------------------------------------------------

--
-- Table structure for table `welcome_messages`
--

CREATE TABLE `welcome_messages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `person_id` int(3) NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `welcome_messages`
--

INSERT INTO `welcome_messages` (`id`, `person_id`, `description`, `file`, `created_at`, `updated_at`) VALUES
(1, 1, '<p>Md. Jabed Iqbal Pathan completed his B.Sc. Engineering degree from Bangladesh University of Engineering and Technology (BUET). After completion of study from BUET he engaged him in business with himself different organizations at different capacity. He has experience of more than 19 (Ninteen) years as a businessman. He is a member of Board of Directors and Chairman of Board of Directors of JMI Syringes &amp; Medical Devices Ltd. apart from his Directorship with JMI Syringes &amp; Medical Devices Ltd. he is also the founder and Directors of many businesses namely Nipro JMI Pharma Ltd., JMI Marketing Ltd., E- Medicare Ltd., Mazzak Inter Trade Ltd., VIP Traders, Tracking &amp; Survey Solution Ltd. DNA Solution Ltd. etc. Mr. Pathan is a well-known entrepreneur in Bangladesh.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;\r\n<h2>Md. Jabed Iqbal Pathan</h2>\r\n</p>\r\n\r\n<p>Chairman, JMI Group</p>', 'public/uploads/message-1634446336.png', '2021-10-17 04:30:44', '2021-10-16 22:52:16'),
(2, 2, '<p>The company started its operation in 1999 with JMI Syringes &amp; Medical Devices Ltd (formerly JMI Bangla co. Ltd.) with the collaboration of partners from The Republic of South Korea; my dream was to serve the nation by not only offering innovative products but also to make these products affordable to the average income earners within Bangladesh.<br />\r\n<br />\r\nBy focusing on this inspiration, The JMI Group has always dreamed of the diversification and expansion of its current business operations. Due to this motivation, JMI Group now has 11 different businesses operating since its inception. In order to expand to evermore sophisticated products within JMI&rsquo;s lineup, I am happy to inform you that we have already started a new joint venture with Nipro Corporation, Japan. In the context of the agreement, JMI will work together with Nipro Corporation in Bangladesh for improving people&rsquo;s quality of life.<br />\r\n<br />\r\nAs the founder Managing Director of JMI Group, I would like to express my gratitude to all stakeholders, customers, and employees of this emerging group for all working together since its founding period. If we continue our collaborative and innovative effort, I believe we will soon become one of the best business groups within Bangladesh.<br />\r\nI do believe that JMI Group has much to offer the Bangladesh health sector as well as the world beyond. We have always promised to give our best support, services, and products for healthcare in the future as JMI has done in the past through Auto Disable (AD) syringes and other medical devices. By continuing the innovative services by the JMI team, I have every trust in our young generation for their increasing positive attitude and enthusiasm. Our young generation can work hard and take the lead to take Bangladesh to its desired position in the global market sector; they can hold their heads high.</p>\r\n\r\n<h2>Md. Abdur Razzaq, CIP</h2>\r\n\r\n<p>Founder &amp; Managing Director, JMI Group</p>', 'public/uploads/message-1634446378.png', '2021-10-17 04:30:47', '2021-11-07 15:01:33');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `businesses`
--
ALTER TABLE `businesses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `careers`
--
ALTER TABLE `careers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `joint_ventures`
--
ALTER TABLE `joint_ventures`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notices`
--
ALTER TABLE `notices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `our_missions`
--
ALTER TABLE `our_missions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `staff`
--
ALTER TABLE `staff`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_about`
--
ALTER TABLE `tbl_about`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_album`
--
ALTER TABLE `tbl_album`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_application`
--
ALTER TABLE `tbl_application`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_contact`
--
ALTER TABLE `tbl_contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_gallery`
--
ALTER TABLE `tbl_gallery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_join_us`
--
ALTER TABLE `tbl_join_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_office_address`
--
ALTER TABLE `tbl_office_address`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_slider`
--
ALTER TABLE `tbl_slider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `videos`
--
ALTER TABLE `videos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `website_infos`
--
ALTER TABLE `website_infos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `welcome_messages`
--
ALTER TABLE `welcome_messages`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `businesses`
--
ALTER TABLE `businesses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `careers`
--
ALTER TABLE `careers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `joint_ventures`
--
ALTER TABLE `joint_ventures`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `notices`
--
ALTER TABLE `notices`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `our_missions`
--
ALTER TABLE `our_missions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `staff`
--
ALTER TABLE `staff`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `tbl_about`
--
ALTER TABLE `tbl_about`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_album`
--
ALTER TABLE `tbl_album`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tbl_application`
--
ALTER TABLE `tbl_application`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_contact`
--
ALTER TABLE `tbl_contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_gallery`
--
ALTER TABLE `tbl_gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT for table `tbl_join_us`
--
ALTER TABLE `tbl_join_us`
  MODIFY `id` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_office_address`
--
ALTER TABLE `tbl_office_address`
  MODIFY `id` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tbl_slider`
--
ALTER TABLE `tbl_slider`
  MODIFY `id` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `videos`
--
ALTER TABLE `videos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `website_infos`
--
ALTER TABLE `website_infos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `welcome_messages`
--
ALTER TABLE `welcome_messages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
