@extends('website.layout.default')
@section('title')
    JMI | Mission, Vision & Objective
@endsection
@section('content')

<section class="advanced-area">
    <div class="container">
        <h1>Mission, Vision & Objective</h1>
        <div class="col-md-6 col-sm-12 col-xs-12">
            <div class="adv-detail cor">
                <p style="text-align:left">
                    {!! $mission->description !!}
                </p><br><br>
            </div>
        </div>
        <div class="col-md-6 col-sm-12 col-xs-12">
            <div class="adv-detail cor">
                <img src="{{ asset('public/web') }}/img/interface/mission.jpg"/>
            </div>
        </div>
    </div>
</section>

@endsection