<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Session;

class AboutController extends Controller
{
    public function index(){

    	$data['about'] = DB::table('tbl_about')->get();
    	return view('admin.about.manage_about',$data);
    }

    public function edit($id){

    	$single = DB::table('tbl_about')->find($id);
    	return view('admin.about.edit',['single'=>$single]);
    }

    public function update(Request $request, $id){

        $result = DB::table('tbl_about')
            ->where('id',$id)
            ->update([
                'description'=>$request->description
            ]);

        if($result){
            setMessage("message","success","Successful");
            return back();
        }else{
            setMessage("message","danger","Failed");
            return back();
        }
        
    }//update


}//AboutController
