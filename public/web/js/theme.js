var skillsDiv = jQuery('#skills');

jQuery(window).on('scroll', function(){
  var winT = jQuery(window).scrollTop(),
    winH = jQuery(window).height(),
    skillsT = skillsDiv.offset().top;
  if(winT + winH  > skillsT){
    jQuery('.skillbar').each(function(){
      jQuery(this).find('.skillbar-bar').animate({
        width:jQuery(this).attr('data-percent')
      },4000);
    });
  }
});

jQuery(document).ready(function () {
    jQuery('#datepicker').datepicker({
        format: 'dd-mm-yyyy',
        startDate: '+1d'
    });
});

$(function() {
    var $searchlink = $('#searchlink');

     $searchlink.on('click', function(e){
    var target = e ? e.target : window.event.srcElement;

    if($(target).attr('id') == 'searchlink') {
      if($(this).hasClass('open')) {
          $(this).removeClass('open');
      } else {
          $(this).addClass('open');
      }
    }
  });
});

$(document).ready(function() {
var url = window.location.href;
url = url.substring(0, (url.indexOf("#") == -1) ? url.length : url.indexOf("#"));
url = url.substring(0, (url.indexOf("?") == -1) ? url.length : url.indexOf("?"));
url = url.substr(url.lastIndexOf("/") + 1);
if(url == ''){
url = 'index.php';
}
$('.navigation li').each(function(){
var href = $(this).find('a').attr('href');
if(url == href){
$(this).addClass('active');
}
});
});

$(document).ready(function(){
    $(function(){
        $(document).on( 'scroll', function(){
            if ($(window).scrollTop() > 100) {
                $('.scroll-top-wrapper').addClass('show');
            } else {
                $('.scroll-top-wrapper').removeClass('show');
            }
        });
        $('.scroll-top-wrapper').on('click', scrollToTop);
    });
    function scrollToTop() {
        verticalOffset = typeof(verticalOffset) != 'undefined' ? verticalOffset : 0;
        element = $('body');
        offset = element.offset();
        offsetTop = offset.top;
        $('html, body').animate({scrollTop: offsetTop}, 500, 'linear');
    }
});

