@extends('admin.layout.default')
@section('title')
  Manage Notice
@endsection
@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Manage Notice</h3>
          <div class="box-tools pull-right"></div>
        </div>
        <div class="box-body color-black">
              <table id="members_list_table" class="table table-bordered table-striped">
                <thead>
                  <tr>
                      <th>SN</th>
					            <th>Section</th>
                      <th>Title</th>
                      <th>Date</th>
                      <th>File</th>
                      <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php if(!empty($get_all)){ 
                  	foreach ($get_all as $key => $value) { 
                  ?>
                    <tr>
                      	<td>{{ ++$key }}</td>
                        <td>
                          <?php
                            if($value->section_id==1){
                              echo 'Notice';
                            }else if($value->section_id==2){
                              echo 'News';
                            }else{
                              echo 'Event';
                            }
                          ?>
                        </td>
                        <td>{{ $value->title }}</td>
                        <td>{{ date("d-m-Y",strtotime($value->created_at)) }}</td>
                        <td>
                          <a href="{{ asset($value->file) }}" target="_blank">
                            View Notice
                          </a>
                        </td>
                    	<td>
                          <a href="{{ route('notices.edit',$value->id) }}">Edit&nbsp;<i class="fa fa-edit"></i></a> | 
                          <a href="{{ route('notices.destroy',$value->id) }}" style="color: red;" title="Delete" onclick="return confirm('Are you sure to delete this ?')" >Delete<i class="fa fa-trash-o fa-lg" style="color: red;"></i></a>
                        </td>
                    </tr>
                  <?php } } ?>
                </tbody>
              </table>        
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </section><!-- /.content -->
  </div><!-- /.content-wrapper -->

@endsection